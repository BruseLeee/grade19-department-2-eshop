using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Electronic.emporium.Api.Repository;
using Electronic.emporium.Api.Entity;
using Electronic.emporium.Api.Utils;
using System.Linq;
using Electronic.emporium.Api.Params;
using Microsoft.AspNetCore.Authorization;
using Electronic.emporium.Api.Database;
using System;
using System.Collections;

namespace Electronic.emporium.Api.Controllers
{
    /// <summary>
    /// 管理类型/属性/属性值控制器
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class BackTypeController : ControllerBase
    {
        /// <summary>
        /// 创建供应商存储仓库
        /// </summary>
        private IRepository<Electronic.emporium.Api.Entity.Type> _TypeRepository;
        private IConfiguration _configuration;
        /// <summary>
        /// 所有数据库
        /// </summary>
        /// <returns></returns>
        ElectronicDb _Context = new ElectronicDb();

        public BackTypeController(IConfiguration configuration, IRepository<Electronic.emporium.Api.Entity.Type> TypeRepository)
        {
            _TypeRepository = TypeRepository;
            _configuration = configuration;
        }

        /// <summary>
        /// 获取类型
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("gettype")]
        public dynamic gettype()
        {
            var type = _TypeRepository.Table.ToList();
            return DataStatus.DataSuccess(1000, type, "获取类型成功！");
        }

        /// <summary>
        /// 添加类型
        /// </summary>
        /// <param name="AddBrand">传入前端数据实体</param>
        /// <returns></returns>
        [HttpPost]
        [Route("Addstpe")]
        public dynamic Addstpe(AddorUpType AddType)
        {
            var name = AddType.Name;
            if (string.IsNullOrEmpty(name))
            {
                return DataStatus.DataError(1111, "请检查必填字段是否都填写规范！");
            }

            var isname = _TypeRepository.Table.Where(x => x.Name == name).SingleOrDefault();
            if (isname != null)
            {
                return DataStatus.DataError(1111, "类型已存在，请勿重复使用！");
            }

            var type = new Electronic.emporium.Api.Entity.Type
            {
                Name = name
            };

            _TypeRepository.Insert(type);
            return DataStatus.DataSuccess(1000, type, "类型添加功！");
        }

        /// <summary>
        /// 获取属性
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getAttribute/{typeId}")]
        public dynamic getAttribute(int typeId)
        {
            var type = _Context.Attribute.Where(x => x.TypeId == typeId).ToList();
            return DataStatus.DataSuccess(1000, type, "获取属性成功！");
        }

        /// <summary>
        /// 添加属性
        /// </summary>
        /// <param name="typeId">类型id</param>
        /// <returns></returns>
        [HttpPost]
        [Route("AddAttribute/{typeId}")]
        public dynamic AddAttribute(int typeId, AddorUpAttribute AddAttribute)
        {
            var istype = _TypeRepository.GetId(typeId);
            if (istype == null)
            {
                return DataStatus.DataError(1111, "类型不存在！");
            }

            var name = AddAttribute.Name;
            if (string.IsNullOrEmpty(name))
            {
                return DataStatus.DataError(1111, "请检查必填字段是否都填写规范！");
            }

            var isname = _Context.Attribute.Where(x => x.Name == name && x.TypeId == typeId).SingleOrDefault();
            if (isname != null)
            {
                return DataStatus.DataError(1111, "属性已存在，请勿重复使用！");
            }

            var Attribute = new Electronic.emporium.Api.Entity.Attribute
            {
                Name = name,
                TypeId = typeId
            };

            _Context.Attribute.Add(Attribute);
            _Context.SaveChanges();
            return DataStatus.DataSuccess(1000, Attribute, "属性添加功！");
        }

        /// <summary>
        /// 获取属性值
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getAttributevalue/{attributeId}")]
        public dynamic getAttributevalue(int attributeId)
        {
            var type = _Context.AttributeValue.Where(x => x.AttributeId == attributeId).ToList();
            return DataStatus.DataSuccess(1000, type, "获取属性成功！");
        }

        /// <summary>
        /// /// 添加属性值
        /// </summary>
        /// <param name="AddBrand">传入前端数据实体</param>
        /// <returns></returns>
        [HttpPost]
        [Route("AddAttributevalue/{attributeId}")]
        public dynamic AddAttributevalue(int attributeId, AddorUpAttributevalue AddAttributevalue)
        {
            var isAttribute = _Context.Attribute.Where(x => x.Id == attributeId).SingleOrDefault();
            if (isAttribute == null)
            {
                return DataStatus.DataError(1111, "属性不存在！");
            }

            var name = AddAttributevalue.Name;
            if (string.IsNullOrEmpty(name))
            {
                return DataStatus.DataError(1111, "请检查必填字段是否都填写规范！");
            }

            var isname = _Context.AttributeValue.Where(x => x.Name == name && x.AttributeId == attributeId).SingleOrDefault();
            if (isname != null)
            {
                return DataStatus.DataError(1111, "属性值已存在，请勿重复使用！");
            }

            var addValue = new Electronic.emporium.Api.Entity.AttributeValue
            {
                Name = name,
                AttributeId = attributeId,
            };

            _Context.AttributeValue.Add(addValue);
            _Context.SaveChanges();
            return DataStatus.DataSuccess(1000, new { name = addValue.Name, Id = addValue.Id }, "属性值添加功！");
        }

        /// <summary>
        /// 修改类型
        /// </summary>
        /// <param name="UpSupplier">传入前端数据实体</param>
        /// <returns></returns>
        [HttpPut]
        [Route("upstpe/{typeId}")]
        public dynamic upstpe(int typeId, AddorUpType upType)
        {
            var istype = _TypeRepository.GetId(typeId);
            if (istype == null)
            {
                return DataStatus.DataError(1111, "类型不存在！");
            }

            var name = upType.Name;
            if (string.IsNullOrEmpty(name))
            {
                return DataStatus.DataError(1111, "请检查必填字段是否都填写规范！");
            }

            var isname = _TypeRepository.Table.Where(x => x.Name == name && x.Id != typeId).SingleOrDefault();
            if (isname != null)
            {
                return DataStatus.DataError(1111, "类型已存在，请勿重复使用！");
            }

            istype.Name = name;

            _TypeRepository.Update(istype);

            return DataStatus.DataSuccess(1000, istype, "修改类型成功！");
        }

        /// <summary>
        /// 修改属性
        /// </summary>
        /// <param name="upAttribute">传入前端数据实体</param>
        /// <returns></returns>
        [HttpPut]
        [Route("upAttribute/{AttributeId}")]
        public dynamic upAttribute(int AttributeId, AddorUpAttributevalue upAttribute)
        {
            var isAttribute = _Context.Attribute.Where(x => x.Id == AttributeId).SingleOrDefault();
            if (isAttribute == null)
            {
                return DataStatus.DataError(1111, "属性不存在！");
            }

            var name = upAttribute.Name;
            if (string.IsNullOrEmpty(name))
            {
                return DataStatus.DataError(1111, "请检查必填字段是否都填写规范！");
            }

            var isname = _Context.Attribute.Where(x => x.Name == name && x.Id != AttributeId).SingleOrDefault();
            if (isname != null)
            {
                return DataStatus.DataError(1111, "属性已存在，请勿重复使用！");
            }

            isAttribute.Name = name;

            _Context.Attribute.Update(isAttribute);
            _Context.SaveChanges();

            return DataStatus.DataSuccess(1000, isAttribute, "修改属性成功！");
        }

        /// <summary>
        /// 修改属性值
        /// </summary>
        /// <param name="upAttribute">传入前端数据实体</param>
        /// <returns></returns>
        [HttpPut]
        [Route("upAttributevalue/{AttributevalueId}")]
        public dynamic upAttributevalue(int AttributevalueId, AddorUpAttributevalue UpAttributevalue)
        {
            var isAttributevalue = _Context.AttributeValue.Where(x => x.Id == AttributevalueId).SingleOrDefault();
            if (isAttributevalue == null)
            {
                return DataStatus.DataError(1111, "属性值不存在！");
            }

            var name = UpAttributevalue.Name;
            if (string.IsNullOrEmpty(name))
            {
                return DataStatus.DataError(1111, "请检查必填字段是否都填写规范！");
            }

            var isname = _Context.AttributeValue.Where(x => x.Name == name && x.Id != AttributevalueId).SingleOrDefault();
            if (isname != null)
            {
                return DataStatus.DataError(1111, "属性值已存在，请勿重复使用！");
            }

            isAttributevalue.Name = name;

            _Context.AttributeValue.Update(isAttributevalue);
            _Context.SaveChanges();

            return DataStatus.DataSuccess(1000, isAttributevalue, "修改属性成功！");
        }


        /// <summary>
        /// 删除类型
        /// </summary>
        /// <param name="typeId"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("delstpe/{typeId}")]
        public dynamic delstpe(int typeId)
        {
            var istype = _TypeRepository.GetId(typeId);
            if (istype == null)
            {
                return DataStatus.DataError(1111, "类型不存在！");
            }

            var Attribute = _Context.Attribute.Where(x => x.TypeId == typeId).ToList();

            foreach (var item in Attribute)
            {
                var AttributeValue = _Context.AttributeValue.Where(x => x.AttributeId == item.Id).ToList();
                _Context.RemoveRange(AttributeValue);
                _Context.SaveChanges();
            }

            _Context.RemoveRange(Attribute);
            _Context.SaveChanges();
            _TypeRepository.Delete(typeId);


            return DataStatus.DataSuccess(1000, typeId, "删除类型及其所有成功！");
        }

        /// <summary>
        /// 删除属性
        /// </summary>
        /// <param name="typeId"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("delAttribute/{AttributeId}")]
        public dynamic delAttribute(int AttributeId)
        {
            var isAttribute = _Context.Attribute.Where(x => x.Id == AttributeId).SingleOrDefault();
            if (isAttribute == null)
            {
                return DataStatus.DataError(1111, "属性不存在！");
            }

            var AttributeValue = _Context.AttributeValue.Where(x => x.AttributeId == isAttribute.Id).ToList();
            _Context.RemoveRange(AttributeValue);

            _Context.RemoveRange(isAttribute);
            _Context.SaveChanges();

            return DataStatus.DataSuccess(1000, AttributeId, "删除属性及其所有属性值成功！");
        }

        /// <summary>
        /// 删除属性值
        /// </summary>
        /// <param name="typeId"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("delAttributevalue/{AttributevalueId}")]
        public dynamic delAttributevalue(int AttributevalueId)
        {
            var isAttributevalue = _Context.AttributeValue.Where(x => x.Id == AttributevalueId).SingleOrDefault();
            if (isAttributevalue == null)
            {
                return DataStatus.DataError(1111, "属性值不存在！");
            }

            _Context.RemoveRange(isAttributevalue);
            _Context.SaveChanges();

            return DataStatus.DataSuccess(1000, isAttributevalue, "删除属性值成功！");
        }

        /// <summary>
        /// 获取属性参数
        /// </summary>
        /// <param name="typeId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("gettypevalue/{typeId}")]
        public dynamic gettypevalue(int typeId)
        {
            var istype=_TypeRepository.GetId(typeId);
            if(istype==null){
                return DataStatus.DataError(1111, "类型不存在！");
            }

            var value =_Context.Attribute.Where(x=>x.TypeId==typeId).Join(_Context.Type,Pet =>Pet.TypeId, per => per.Id, (pet, per) => new{
                    KeyId=pet.Id,
                    KeyValue=pet.Name,
                    Value=_Context.AttributeValue.Where(y=>y.AttributeId==pet.Id).ToList(),
                    SelectValue=new ArrayList()
            });

            return DataStatus.DataSuccess(1000,value , "求求了！");
        }
    }
}