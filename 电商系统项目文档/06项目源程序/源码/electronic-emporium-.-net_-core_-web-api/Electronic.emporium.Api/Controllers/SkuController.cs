using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Electronic.emporium.Api.Repository;
using Electronic.emporium.Api.Entity;
using Electronic.emporium.Api.Utils;
using System.Linq;
using Electronic.emporium.Api.Params;
using Microsoft.AspNetCore.Authorization;
using Electronic.emporium.Api.Database;
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Electronic.emporium.Api.Controllers
{
    /// <summary>
    /// 管理sku控制器
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class SkuController : ControllerBase
    {
        /// <summary>
        /// 创建sku存储仓库
        /// </summary>
        private IRepository<Sku> _SkuRepository;
        private IConfiguration _configuration;
        /// <summary>
        /// 所有数据库
        /// </summary>
        /// <returns></returns>
        ElectronicDb _Context = new ElectronicDb();

        public SkuController(IConfiguration configuration, IRepository<Sku> SkuRepository)
        {
            _SkuRepository = SkuRepository;
            _configuration = configuration;
        }

        /// <summary>
        /// 添加sku
        /// </summary>
        /// <param name="AddBrand">传入前端数据实体</param>
        /// <returns></returns>
        [HttpPost]
        [Route("Addsku/{ProductId}")]
        public dynamic Addsku(int ProductId, AddorUpSku Addsku)
        {
            var isProduct = _Context.ProductInfo.Where(x => x.Id == ProductId).SingleOrDefault();
            if (isProduct == null)
            {
                return DataStatus.DataError(1111, "所添加的商品不存在！");
            }

            var KeyValueJson = Addsku.KeyValueJson;
            var sku = Addsku.SKU;
            var price=Addsku.Price;
            if (string.IsNullOrEmpty(KeyValueJson) || string.IsNullOrEmpty(sku) || price==0)
            {
                return DataStatus.DataError(1111, "请检查必填字段是否都填写规范！");
            }

            var isAttributeValue = _SkuRepository.Table.Where(x => x.KeyValueJson == KeyValueJson && x.ProductId == ProductId).SingleOrDefault();
            if (isAttributeValue != null)
            {
                return DataStatus.DataError(1111, "所添加的属性值重复！");
            }

            var addsku = new Sku
            {
                ProductId = ProductId,
                SKU = sku,
                KeyValueJson = KeyValueJson,
                Price=price
            };            
            //查找各个json对应的中文值
            // var jsonvalue=KeyValue.

            _SkuRepository.Insert(addsku);
            var json=JsonConvert.DeserializeObject(addsku.KeyValueJson);
            return DataStatus.DataSuccess(1000, json, "添加sku成功！");
        }

        [HttpDelete]
        [Route("delsku/{Sku}")]
        public dynamic delsku(string Sku)
        {
            var isSku=_SkuRepository.Table.Where(x=>x.SKU==Sku).SingleOrDefault();
            if (isSku == null)
            {
                return DataStatus.DataError(1111, "Sku不存在！");
            }

            _SkuRepository.Delete(isSku.Id);
            return DataStatus.DataSuccess(1000, isSku.Id, "删除成功！");
        }

        /// <summary>
        /// 获取sku组合
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getsku/{ProductId}")]
        public dynamic getSupplierInfo(int ProductId)
        {
            var sku = _SkuRepository.Table.Where(x=>x.ProductId==ProductId).Select(x=>new{
                SKU=x.SKU,
                KeyValueJson = JsonConvert.DeserializeObject(x.KeyValueJson),
                Price=x.Price
            });
            return DataStatus.DataSuccess(1000, sku, "获取sku成功！");
        }
    }
}