//这里是管理员控制器

using System.Linq;
using Electronic.emporium.Api.Database;
using Electronic.emporium.Api.Entity;
using Electronic.emporium.Api.Params;
using Electronic.emporium.Api.Repository;
using Electronic.emporium.Api.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace Electronic.emporium.Api.Controllers
{
    /// <summary>
    /// 管理员控制器
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class BackUserAdminController : ControllerBase
    {
        /// <summary>
        /// 创建管理员储存仓库
        /// </summary>
        private IRepository<AdminUser> _adminUSerReposigory;
        private IConfiguration _configuration;
        private TokenParameter _tokenParams;

        ElectronicDb _Context = new ElectronicDb();

        public BackUserAdminController(
            IRepository<AdminUser> adminUserRepository,
            IConfiguration configuration
        )
        {
            _adminUSerReposigory = adminUserRepository;
            _configuration = configuration;
            _tokenParams =
                _configuration
                    .GetSection("TokenParameter")
                    .Get<TokenParameter>();

        }

        /// <summary>
        /// 管理员登录
        /// </summary>
        /// <param name="adminUserParams"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        [Route("adminlogin")]
        public dynamic AdminLogin(AdminUserParams adminLogin)
        {
            var username = adminLogin.Username;
            var password = adminLogin.Password;

            var adminUser = _adminUSerReposigory.Table.Where(x => x.Username == username).SingleOrDefault();

            if (adminUser == null)
            {
                return DataStatus.DataError(1111, "用户不存在，请重新输入用户名和密码");
            }

            if (string.IsNullOrEmpty(username) && string.IsNullOrEmpty(password))
            {
                DataStatus.DataError(1112, "请检查必填字段是否都填写规范！");
            }



            var token = TokenHelper.GenerateToken(_tokenParams, adminUser.Username);
            var refreshToken = "112358";

            return DataStatus.DataSuccess(1000, new { adminUser, token = token, refreshToken = refreshToken }, "登录成功");
        }

        /// <summary>
        /// 注册管理员用户
        /// </summary>
        /// <param name="newAdmin"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("adminregister")]
        public dynamic AdminRegister(AdminUserParams newAdmin)
        {
            var username = newAdmin.Username;
            var password = newAdmin.Password;
            var nickname = newAdmin.Nickname;
            var phone = newAdmin.Phone;
            var gender = newAdmin.Gender;

            var adminuser = _adminUSerReposigory.Table.Where(x => x.Username == username).SingleOrDefault();

            if (adminuser != null)
            {
                return DataStatus.DataError(1112, "您注册的用户已存在，请重新输入用户名");
            }

            if (string.IsNullOrEmpty(username) && string.IsNullOrEmpty(password) && string.IsNullOrEmpty(nickname) && string.IsNullOrEmpty(phone))
            {
                return DataStatus.DataError(1111, "请检查必填字段是否都填写规范！");
            }

            var newAdminUser = new AdminUser
            {
                Username = username,
                Password = password,
                NickName = nickname,
                Phone = phone,
                Gender = gender
            };

            _adminUSerReposigory.Insert(newAdminUser);

            return DataStatus.DataSuccess(1000, newAdminUser, "注册成功");
        }

        /// <summary>
        /// 查询管理员用户列表控制器
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getadminuser")]
        public dynamic getAdminUser()
        {
            var adminuser = _adminUSerReposigory.Table.ToList();
            return DataStatus.DataSuccess(1000, adminuser, "查询管理员列表成功");
        }

        /// <summary>
        /// 查询指定管理员用户列表控制器
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getupadminuser/{id}")]
        public dynamic getUpAdminUser(int id)
        {
            var adminuser = _adminUSerReposigory.GetId(id);
            return DataStatus.DataSuccess(1000, adminuser, "查询管理员列表成功");
        }

        /// <summary>
        /// 修改个人信息
        /// </summary>
        /// <param name="upAdminUser"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("upadminuser/{id}")]
        public dynamic upAdminUser(AdminUserParams upAdminUser, int id)
        {
            var adminuser = _adminUSerReposigory.GetId(id);

            var nickname = upAdminUser.Nickname;
            var phone = upAdminUser.Phone;
            var gender = upAdminUser.Gender;

            if (adminuser == null)
            {
                return DataStatus.DataError(1111, "修改的管理员并不能为空，请重新选择");
            }

            if (string.IsNullOrEmpty(nickname) || string.IsNullOrEmpty(phone))
            {
                return DataStatus.DataError(1112, "请检查必填字段是否都填写规范！");
            }

            adminuser.NickName = nickname;
            adminuser.Phone = phone;
            adminuser.Gender = gender;

            _adminUSerReposigory.Update(adminuser);

            return DataStatus.DataSuccess(1000, adminuser, "修改个人信息成功");
        }


        /// <summary>
        /// 删除管理员控制器
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("deleteadminuser/{id}")]
        public dynamic deleteAdminRole(int id)
        {
            var adminrole = _adminUSerReposigory.GetId(id);
            if (adminrole == null)
            {
                return DataStatus.DataError(1111, "该管理员不存在，删除失败!");
            }
            _adminUSerReposigory.Delete(id);
            return DataStatus.DataSuccess(1000, id, "删除管理员用户成功");
        }

        /// <summary>
        /// token验证
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost, Route("token")]
        public dynamic GetToken(AdminUserParams model)
        {
            var username = model.Username.Trim();
            var password = model.Password.Trim();

            var user = _adminUSerReposigory.Table.Where(x => x.Username == username && x.Password == password).FirstOrDefault();

            if (user == null)
            {
                return new
                {
                    Code = 1001,
                    Data = user,
                    Msg = "用户名或密码不正确，请核对后重试"
                };
            }

            var token = TokenHelper.GenerateToken(_tokenParams, user.Username);
            var refreshToken = "112358";

            var res = new
            {
                Code = 1000,
                Data = new { Token = token, RefreshToken = refreshToken },
                Msg = "用户登录成功，获取token成功"
            };
            return res;
        }

        /// <summary>
        /// 刷新token
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost, Route("refreshToken")]
        public dynamic RefreshToken(RefreshTokenDTO model)
        {
            var username = TokenHelper.ValidateToken(_tokenParams, model);

            if (string.IsNullOrEmpty(username))
            {
                return new { Code = 1002, Data = "", Msg = "token验证失败" };
            }

            var token = TokenHelper.GenerateToken(_tokenParams, username);
            var refreshToken = "112358";

            return new
            {
                Code = 1000,
                Data = new { Token = token, refreshToken = refreshToken },
                Msg = "刷新token成功^_^"
            };
        }
    }
}
