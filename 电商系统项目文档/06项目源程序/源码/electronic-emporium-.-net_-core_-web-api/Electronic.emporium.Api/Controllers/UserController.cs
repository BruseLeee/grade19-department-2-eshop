﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Electronic.emporium.Api.Repository;
using Electronic.emporium.Api.Entity;
using Electronic.emporium.Api.Utils;
using System.Linq;
using Electronic.emporium.Api.Params;
using Microsoft.AspNetCore.Authorization;
using Electronic.emporium.Api.Database;
using System;
namespace Electronic.emporium.Api.Controllers
{
    /// <summary>
    /// 用户控制器
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class UserController : ControllerBase
    {
        /// <summary>
        /// 创建用户存储仓库
        /// </summary>
        /// <typeparam name="Users">用户实体</typeparam>
        /// <returns></returns>
        private IRepository<User> _userRepository;

        private IConfiguration _configuration;
        private TokenParameter _tokenParams;

        /// <summary>
        /// 所有数据库
        /// </summary>
        ElectronicDb _Context = new ElectronicDb();

        public UserController(IConfiguration configuration, IRepository<User> userRepository)
        {
            _userRepository = userRepository;
            _configuration = configuration;
            _tokenParams =
                _configuration
                    .GetSection("TokenParameter")
                    .Get<TokenParameter>();
        }

        /// <summary>
        /// 注册
        /// </summary>
        /// <param name="AddUser">接收前端的数据</param>
        /// <returns></returns>
        [HttpPost]
        [Route("closing")]
        public dynamic Closing(AddorUpUser AddUser)
        {
            var userName = AddUser.Username;
            var userPassword = AddUser.Password;
            var reUserPassword = AddUser.rePassword;
            var problemId = AddUser.ProblemId;
            var answer = AddUser.Answer;

            if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(userPassword) || string.IsNullOrEmpty(reUserPassword) || problemId == 0 || string.IsNullOrEmpty(answer))
            {
                return DataStatus.DataError(1111, "请检查必填字段是否都填写规范！");
            }

            var isUser = _userRepository.Table.Where(x => x.Username == userName).SingleOrDefault();
            if (isUser != null)
            {
                return DataStatus.DataError(1111, "该用户已注册，请直接登录！");
            }

            if (!userPassword.Equals(reUserPassword))
            {
                return DataStatus.DataError(1111, "两次输入的密码不一致！");
            }

            var newUser = new User
            {
                Username = userName,
                Password = reUserPassword,
                ProblemId = problemId,
                Answer = answer
            };

            _userRepository.Insert(newUser);
            return DataStatus.DataSuccess(1000, new { userId = newUser.Id }, "注册成功！");

        }

        /// <summary>
        /// 添加用户信息
        /// </summary>
        /// <param name="userId">用户Id</param>
        /// <param name="AddUserInfo">接收前端的数据</param>
        /// <returns></returns>
        [HttpPost]
        [Route("newUserInfo/{userId}")]
        public dynamic NewUserInfo(int userId, AddorUpUserInfo AddUserInfo)
        {
            var IsUser = _userRepository.GetId(userId);
            if (IsUser == null)
            {
                return DataStatus.DataError(1111, "用户不存在，请先注册！");
            }

            var IsUserInfo = _Context.UserInfo.Where(x => x.UserId == userId).SingleOrDefault();
            if (IsUserInfo != null)
            {
                return DataStatus.DataError(1111, "注册失败该用户Id已经存在信息，请联系管理员处理！");
            }

            var nickname = AddUserInfo.Nickname;
            var gender = AddUserInfo.Gender;

            if (string.IsNullOrEmpty(nickname) || string.IsNullOrEmpty(gender))
            {
                return DataStatus.DataError(1111, "请检查必填字段是否都填写规范！");
            }

            var newUserInfo = new UserInfo
            {
                UserId = userId,
                Nickname = nickname,
                Gender = gender,
                IconUrl = "/UserImg/DefaultImg.png"
            };

            _Context.UserInfo.Add(newUserInfo);
            _Context.SaveChanges();
            return DataStatus.DataSuccess(1000, new { }, "注册用户信息成功！");
        }

        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="loginData">接收前端的数据</param>
        /// <returns></returns>
        [HttpPost]
        [Route("login")]
        public dynamic Login(AddorUpUser loginData)
        {
            var userName = loginData.Username;
            var password = loginData.Password;

            var user = _userRepository.Table.ToList();

            var Isuser = user.Where(x => x.Username.Equals(userName) && x.Password.Equals(password) && x.IsDeleted == false).SingleOrDefault();

            if (Isuser == null)
            {
                return DataStatus.DataError(1111, "账号或密码不正确！");
            }

            if (!Isuser.IsActived)
            {
                return DataStatus.DataError(1111, "账号未启用，请联系管理员！");
            }

            var userInfo = _Context.UserInfo.Where(x => x.UserId == Isuser.Id).SingleOrDefault();
            var token = TokenHelper.GenerateToken(_tokenParams, Isuser.Username);
            var refreshToken = "112358";


            return DataStatus.DataSuccess(1000, new { UserId = Isuser.Id, Nickname = userInfo.Nickname, token = token, refreshToken = refreshToken }, "登录成功！");
        }

        /// <summary>
        /// 获取用户Id
        /// </summary>
        /// <param name="userName">账号</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getuserid/{userName}")]
        public dynamic getuserid(string userName)
        {
            var IsUser = _userRepository.Table.Where(x => x.Username.Equals(userName)).SingleOrDefault();
            if (IsUser == null)
            {
                return DataStatus.DataError(1111, "用户不存在！");
            }
            return DataStatus.DataSuccess(1000, new { userId = IsUser.Id }, "获取用户Id成功！");
        }

        /// <summary>
        /// 以用户Id获取密保
        /// </summary>
        /// <param name="userId">用户id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getuserproblem/{userId}")]
        public dynamic GetUserProblem(int userId)
        {
            var IsUser = _userRepository.GetId(userId);
            if (IsUser == null)
            {
                return DataStatus.DataError(1111, "用户不存在！");
            }

            var userProblemId = IsUser.ProblemId;
            var problem = _Context.Problem.Where(x => x.Id == userProblemId && x.IsActived == true && x.IsDeleted == false).ToList();
            return DataStatus.DataSuccess(1000, problem, "获取密保问题成功！");
        }

        /// <summary>
        /// 验证用户密保答案
        /// </summary>
        /// <param name="userId">用户id</param>
        /// <param name="validanswer">接收前端的数据</param>
        /// <returns></returns>
        [HttpPut]
        [Route("validanswer/{userId}")]
        public dynamic validanswer(int userId, AddorUpUser validanswer)
        {
            var IsUser = _userRepository.GetId(userId);
            if (IsUser == null)
            {
                return DataStatus.DataError(1111, "用户不存在！");
            }

            var answer = validanswer.Answer;
            if (string.IsNullOrEmpty(answer))
            {
                return DataStatus.DataError(1111, "请检查必填字段是否都填写规范！");
            }

            if (!IsUser.Answer.Equals(answer))
            {
                return DataStatus.DataError(1111, "答案不正确！");
            }

            return DataStatus.DataSuccess(1000, new { }, "密保验证成功！");
        }


        /// <summary>
        /// 修改密码
        /// </summary>
        /// <param name="userId">用户Id</param>
        /// <param name="upPassword">接收前端的数据</param>
        /// <returns></returns>
        [HttpPut]
        [Route("uppassword/{userId}")]
        public dynamic upPassword(int userId, AddorUpUser upPassword)
        {
            var IsUser = _userRepository.GetId(userId);
            if (IsUser == null)
            {
                return DataStatus.DataError(1111, "用户不存在！");
            }

            var password = upPassword.Password;
            var rePassword = upPassword.rePassword;

            if (string.IsNullOrEmpty(password) || string.IsNullOrEmpty(rePassword))
            {
                return DataStatus.DataError(1111, "请检查必填字段是否都填写规范！");
            }

            if (IsUser.Password.Equals(password))
            {
                return DataStatus.DataError(1111, "新密码与原密码相同！");
            }

            if (!password.Equals(rePassword))
            {
                return DataStatus.DataError(1111, "两次输入的密码不一致！");
            }

            IsUser.Password = rePassword;
            _userRepository.Update(IsUser);

            return DataStatus.DataSuccess(1000, new { }, "密码修改成功！");
        }

        [HttpGet]
        [Route("getuserinof/{userId}")]
        public dynamic GetUserInof(int userId)
        {
            var IsUser = _userRepository.GetId(userId);
            if (IsUser == null)
            {
                return DataStatus.DataError(1111, "用户不存在！");
            }

            var IsUserInfo = _Context.UserInfo.Where(x => x.UserId == userId).SingleOrDefault();
            if (IsUserInfo == null)
            {
                return DataStatus.DataError(1111, "账号异常，请联系管理员！");
            }

            var UserInfoView = new
            {
                Username = IsUser.Username,
                Nickname = IsUserInfo.Nickname,
                Gender = IsUserInfo.Gender,
            };

            return DataStatus.DataSuccess(1000, UserInfoView, "获取个人信息成功！");

        }

        [HttpPut]
        [Route("upuserinof/{userId}")]
        public dynamic upuserinof(int userId,AddorUpUserInfo upUserInfo)
        {
            var IsUser = _userRepository.GetId(userId);
            if (IsUser == null)
            {
                return DataStatus.DataError(1111, "用户不存在！");
            }

            var IsUserInfo = _Context.UserInfo.Where(x => x.UserId == userId).SingleOrDefault();
            if (IsUserInfo == null)
            {
                return DataStatus.DataError(1111, "账号异常，请联系管理员！");
            }

            var nickname = upUserInfo.Nickname;
            var gender = upUserInfo.Gender;

            if (string.IsNullOrEmpty(nickname) || string.IsNullOrEmpty(gender))
            {
                return DataStatus.DataError(1111, "请检查必填字段是否都填写规范！");
            }

            IsUserInfo.Nickname=nickname;
            IsUserInfo.Gender=gender;
            _Context.UserInfo.Update(IsUserInfo);
            _Context.SaveChanges();

            return DataStatus.DataSuccess(1000, IsUserInfo, "获取个人信息成功！");

        }


    }
}