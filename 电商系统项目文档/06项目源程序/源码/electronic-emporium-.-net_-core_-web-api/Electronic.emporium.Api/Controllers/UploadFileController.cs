using System;
using System.IO;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Electronic.emporium.Api.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;
using Electronic.emporium.Api.Repository;
using Microsoft.Net.Http.Headers;
using Electronic.emporium.Api.Entity;
using System.Linq;
using Electronic.emporium.Api.Database;

namespace Electronic.emporium.Api.Controllers
{
    // [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class UploadFileController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        ElectronicDb _Context = new ElectronicDb();

        public UploadFileController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        /// <summary>
        /// logo上传
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpLogo/{BrandId}")]
        public string UpLogo(int BrandId, IFormCollection model)
        {
            var isBrand=_Context.BrandInfo.Where(x=>x.Id==BrandId).SingleOrDefault();
            if (isBrand == null)
            {
                return DataStatus.DataError(1111, "该品牌不已存在，上传失败！");
            }

            // 获得当前应用所在的完整路径（绝对地址）
            var filePath = Directory.GetCurrentDirectory();

            // 通过配置文件获得存放文件的相对路径头像
            string path = _configuration["UpLogo"];

            // 最终存放文件的完整路径
            var preFullPath = Path.Combine(filePath, path);
            // 如果路径不存在，则创建
            if (!Directory.Exists(preFullPath))
            {
                Directory.CreateDirectory(preFullPath);
            }

            var resultPath = new List<string>();
            foreach (IFormFile file in model.Files)
            {
                if (file.Length > 0)
                {
                    var fileName = file.FileName;
                    var extName = fileName.Substring(fileName.LastIndexOf("."));//extName包含了“.”
                    var rndName = Guid.NewGuid().ToString("N") + extName;
                    var tempPath = Path.Combine(path, rndName).Replace("\\", "/");
                    using (var stream = new FileStream(Path.Combine(filePath, tempPath), FileMode.CreateNew))//Path.Combine(_env.WebRootPath, fileName)
                    {
                        file.CopyTo(stream);
                    }
                    // 此处地址可能带有两个反斜杠，虽然也能用，比较奇怪，统一转换成斜杠，这样在任何平台都有一样的表现
                    resultPath.Add(tempPath.Replace("\\", "/"));
                    isBrand.LogoUrl=tempPath;
                }
            }
            _Context.BrandInfo.Update(isBrand);
            _Context.SaveChanges();
            return DataStatus.DataSuccess(1000, isBrand, "上传Logo成功！");
        }

        /// <summary>
        /// 商品相册上传
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpProductimg/{ProductId}")]
        public string UpProductimg(int ProductId, IFormCollection model)
        {
            var isArticle = _Context.ProductInfo.Where(x => x.Id == ProductId).SingleOrDefault();

            if (isArticle == null)
            {
                return DataStatus.DataError(1221, "该商品不存在！");
            }
            // 获得当前应用所在的完整路径（绝对地址）
            var filePath = Directory.GetCurrentDirectory();

            // 通过配置文件获得存放文件的相对路径
            string path = _configuration["UpProductimg"];

            // 最终存放文件的完整路径
            var preFullPath = Path.Combine(filePath, path);
            // 如果路径不存在，则创建
            if (!Directory.Exists(preFullPath))
            {
                Directory.CreateDirectory(preFullPath);
            }

            var resultPath = new List<string>();

            foreach (IFormFile file in model.Files)
            {
                if (file.Length > 0)
                {
                    var fileName = file.FileName;
                    var extName = fileName.Substring(fileName.LastIndexOf("."));//extName包含了“.”
                    var rndName = Guid.NewGuid().ToString("N") + extName;
                    var tempPath = Path.Combine(path, rndName).Replace("\\", "/");
                    using (var stream = new FileStream(Path.Combine(filePath, tempPath), FileMode.CreateNew))//Path.Combine(_env.WebRootPath, fileName)
                    {
                        file.CopyTo(stream);
                    }
                    // 此处地址可能带有两个反斜杠，虽然也能用，比较奇怪，统一转换成斜杠，这样在任何平台都有一样的表现
                    resultPath.Add(tempPath.Replace("\\", "/"));
                    isArticle.PictureUrl=tempPath;
                }
            }
            _Context.ProductInfo.Update(isArticle);
            _Context.SaveChanges();
            return DataStatus.DataSuccess(1000, isArticle, "插入商品图片成功！");
        }

        /// <summary>
        /// 富文本图上传
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpFulltextimg")]
        public string UpFulltextimg(IFormCollection model)
        {
            // 获得当前应用所在的完整路径（绝对地址）
            var filePath = Directory.GetCurrentDirectory();

            // 通过配置文件获得存放文件的相对路径
            string path = _configuration["UpFulltextimg"];

            // 最终存放文件的完整路径
            var preFullPath = Path.Combine(filePath, path);
            // 如果路径不存在，则创建
            if (!Directory.Exists(preFullPath))
            {
                Directory.CreateDirectory(preFullPath);
            }

            var resultPath = new List<string>();

            foreach (IFormFile file in model.Files)
            {
                if (file.Length > 0)
                {
                    var fileName = file.FileName;
                    var extName = fileName.Substring(fileName.LastIndexOf("."));//extName包含了“.”
                    var rndName = Guid.NewGuid().ToString("N") + extName;
                    var tempPath = Path.Combine(path, rndName).Replace("\\", "/");
                    using (var stream = new FileStream(Path.Combine(filePath, tempPath), FileMode.CreateNew))//Path.Combine(_env.WebRootPath, fileName)
                    {
                        file.CopyTo(stream);
                    }

                    // 此处地址可能带有两个反斜杠，虽然也能用，比较奇怪，统一转换成斜杠，这样在任何平台都有一样的表现
                    resultPath.Add(tempPath.Replace("\\", "/"));
                }
            }
            return DataStatus.DataSuccess(1000, resultPath, "插入图片成功！");
        }

        // /// <summary>
        // /// 文章图片上传
        // /// </summary>
        // /// <param name="model"></param>
        // /// <returns></returns>
        // [HttpPost("{uId}")]
        // public string UploadFile(int uId, IFormCollection model)
        // {
        //     var userId = uId;
        //     var isUsers = _Context.Users.Where(x => x.Id == userId).SingleOrDefault();

        //     if (isUsers == null)
        //     {
        //         return DataStatus.DataError(1221, "该用户不存在！");
        //     }

        //     // 获得当前应用所在的完整路径（绝对地址）
        //     var filePath = Directory.GetCurrentDirectory();

        //     // 通过配置文件获得存放文件的相对路径
        //     string path = _configuration["UploadFilesPath"];

        //     // 最终存放文件的完整路径
        //     var preFullPath = Path.Combine(filePath, path);
        //     // 如果路径不存在，则创建
        //     if (!Directory.Exists(preFullPath))
        //     {
        //         Directory.CreateDirectory(preFullPath);
        //     }

        //     var resultPath = new List<string>();

        //     foreach (IFormFile file in model.Files)
        //     {
        //         if (file.Length > 0)
        //         {
        //             var fileName = file.FileName;
        //             var extName = fileName.Substring(fileName.LastIndexOf("."));//extName包含了“.”
        //             var rndName = Guid.NewGuid().ToString("N") + extName;
        //             var tempPath = Path.Combine(path, rndName).Replace("\\", "/");
        //             using (var stream = new FileStream(Path.Combine(filePath, tempPath), FileMode.CreateNew))//Path.Combine(_env.WebRootPath, fileName)
        //             {
        //                 file.CopyTo(stream);
        //             }
        //             var tmpFile = new ImagesUrl
        //             {
        //                 OriginName = fileName,
        //                 CurrentName = rndName,
        //                 ATImageUrl = tempPath,
        //                 UserId = userId
        //             };

        //             _fileInfoRepository.Insert(tmpFile);
        //             // 此处地址可能带有两个反斜杠，虽然也能用，比较奇怪，统一转换成斜杠，这样在任何平台都有一样的表现
        //             resultPath.Add(tempPath.Replace("\\", "/"));
        //         }
        //     }

        //     return DataStatus.DataSuccess(1000, resultPath, "插入图片成功！");
        // }


        // // /// <summary>
        // // /// 站点二维码上传
        // // /// </summary>
        // // /// <param name="model"></param>
        // // /// <returns></returns>
        // [HttpPost]
        // [Route("UpQRCode/{wId}")]
        // public string UpQRCode(int wId, IFormCollection model)
        // {
        //     var webId = wId;
        //     var isweb = _Context.WebSide.Where(x => x.Id == webId).SingleOrDefault();

        //     if (isweb == null)
        //     {
        //         return DataStatus.DataError(1221, "该站点不存在！");
        //     }

        //     // 获得当前应用所在的完整路径（绝对地址）
        //     var filePath = Directory.GetCurrentDirectory();

        //     // 通过配置文件获得存放文件的相对路径
        //     string path = _configuration["UpQRCode"];

        //     // 最终存放文件的完整路径
        //     var preFullPath = Path.Combine(filePath, path);
        //     // 如果路径不存在，则创建
        //     if (!Directory.Exists(preFullPath))
        //     {
        //         Directory.CreateDirectory(preFullPath);
        //     }

        //     var resultPath = new List<string>();

        //     foreach (IFormFile file in model.Files)
        //     {
        //         if (file.Length > 0)
        //         {
        //             var fileName = file.FileName;
        //             var extName = fileName.Substring(fileName.LastIndexOf("."));//extName包含了“.”
        //             var rndName = Guid.NewGuid().ToString("N") + extName;
        //             var tempPath = Path.Combine(path, rndName).Replace("\\", "/");
        //             using (var stream = new FileStream(Path.Combine(filePath, tempPath), FileMode.CreateNew))//Path.Combine(_env.WebRootPath, fileName)
        //             {
        //                 file.CopyTo(stream);
        //             }
        //             var addQRCode = new QRCode
        //             {
        //                 WebSideId = webId,
        //                 QRUrl = tempPath
        //             };

        //             _Context.QRCode.Add(addQRCode);
        //             _Context.SaveChanges();
        //             // 此处地址可能带有两个反斜杠，虽然也能用，比较奇怪，统一转换成斜杠，这样在任何平台都有一样的表现
        //             resultPath.Add(tempPath.Replace("\\", "/"));
        //         }
        //     }

        //     return DataStatus.DataSuccess(1000, resultPath, "插入图片成功！");
        // }


    }
}