using System.Linq;
using Electronic.emporium.Api.Database;
using Electronic.emporium.Api.Entity;
using Electronic.emporium.Api.Params;
using Electronic.emporium.Api.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace Electronic.emporium.Api.Controllers
{
    /// <summary>
    /// 管理密保问题控制器
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class BackProblemController : ControllerBase
    {
        /// <summary>
        /// 创建密保问题储存仓库
        /// </summary>
        private IRepository<Problem> _problemRepository;

        private IConfiguration _configuration;

        ElectronicDb _Context = new ElectronicDb();

        public BackProblemController(IConfiguration configuration, IRepository<Problem> problemRepository)
        {
            _problemRepository = problemRepository;
            _configuration = configuration;
        }

        /// <summary>
        /// 获取指定密保
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getupproblem/{id}")]
        public dynamic GetUpProblem(int id)
        {
            var problem = _problemRepository.GetId(id);
            return DataStatus.DataSuccess(1000, problem, "获取密保问题成功！");
        }

        /// <summary>
        /// 获取密保
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getproblem")]
        public dynamic GetProblem()
        {
            var problem = _problemRepository.Table.ToList();
            return DataStatus.DataSuccess(1000, problem, "获取密保问题成功！");
        }

        /// <summary>
        /// 添加密保问题
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="problemRepository"></param>
        [HttpPost]
        [Route("addproblem")]
        public dynamic addProblem(BackProblemParams addProblem){
            var name = addProblem.Name;
            if(string.IsNullOrEmpty(name))
            {
                return DataStatus.DataError(1111,"请检查必填字段是否规范");
            }

            var isProblem = _problemRepository.Table.Where(x=> x.Name == name).SingleOrDefault();

            if(isProblem != null)
            {
                return DataStatus.DataError(1112,"该问题已存在，请重新输入问题");
            }
            
            var problem = new Problem
            {
                Name=name
            };

            _problemRepository.Insert(problem);

            return DataStatus.DataSuccess(1000, problem, "添加问题成功");
        }

        /// <summary>
        /// 修改密保问题
        /// </summary>
        /// <param name="upProblem">接收前端数据</param>
        /// <param name="id">用户Id</param>
        /// <returns></returns>
        [HttpPut]
        [Route("upproblem/{id}")]
        public dynamic upProblem(BackProblemParams upProblem, int id)
        {
            var userProblem = _problemRepository.GetId(id);

            var name = upProblem.Name;
            if (string.IsNullOrEmpty(name))
            {
                return DataStatus.DataError(1111, "请确认密保问题是否填写规范");
            }

            var isProblem = _problemRepository.Table.Where(x => x.Name == name).SingleOrDefault();

            if (isProblem != null)
            {
                return DataStatus.DataError(1112, "该问题已存在，请重新输入问题");
            }

            userProblem.Name = name;
            _problemRepository.Update(userProblem);
            return DataStatus.DataSuccess(1000, userProblem, "成功更新问题");
        }

        /// <summary>
                /// 删除密保问题
                /// </summary>
                /// <param name="id"></param>
                /// <returns></returns>
        [HttpDelete]
        [Route("deleteproblem/{id}")]
        public dynamic deleteProblem(int id)
        {
            var userProblem = _Context.User.Where(x => x.ProblemId == id).Count();
            if (userProblem != 0)
            {
                return DataStatus.DataError(1111, "删除失败，该问题正在使用中！");
            }

            _problemRepository.Delete(id);
            return DataStatus.DataSuccess(1000, id, "问题删除成功");
        }
    }


}
