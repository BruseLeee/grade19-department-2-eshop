using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Electronic.emporium.Api.Repository;
using Electronic.emporium.Api.Entity;
using Electronic.emporium.Api.Utils;
using System.Linq;
using Electronic.emporium.Api.Params;
using Microsoft.AspNetCore.Authorization;
using Electronic.emporium.Api.Database;
using System;
namespace Electronic.emporium.Api.Controllers
{
    /// <summary>
    /// 通用控制器
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class OrderCartController : ControllerBase
    {
        /// <summary>
        /// 创建购物车存储仓库
        /// </summary>
        private IRepository<OrderCart> _OrderCartRepository;
        private IConfiguration _configuration;
        /// <summary>
        /// 所有数据库
        /// </summary>
        /// <returns></returns>
        ElectronicDb _Context = new ElectronicDb();

        public OrderCartController(IConfiguration configuration, IRepository<OrderCart> OrderCartRepository)
        {
            _OrderCartRepository = OrderCartRepository;
            _configuration = configuration;
        }

        /// <summary>
        /// 获取用户购物车
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getUserOrderCart/{UserId}")]
        public dynamic getUserOrderCart(int UserId)
        {
            var isuser = _Context.User.Where(x => x.Id == UserId).SingleOrDefault();
            if (isuser == null)
            {
                return DataStatus.DataError(1111, "用户不存在！");
            }

            var OrderCar = _OrderCartRepository.Table.Where(x => x.UserId == UserId).ToList();

            var sku=OrderCar.Join(_Context.Skus, Pet => Pet.Sku, per => per.SKU, (pet, per) => new
            {
                Id = pet.Id,
                ProductId = per.ProductId,
                Sku = pet.Sku,
                Number = pet.Number,
                Price=per.Price
            }).ToList();

            var OrderCarSku=sku.Join(_Context.ProductInfo,Pet => Pet.ProductId, per => per.Id, (pet, per) => new{
                Id=pet.Id,
                Productname=per.Name,
                ProductURL=per.PictureUrl,
                Sku=pet.Sku,
                Number=pet.Number,
                Price=pet.Price,
                AllPrise=pet.Price*pet.Number
            }).ToList();
            
            return DataStatus.DataSuccess(1000, OrderCarSku, "获取用户购物车成功！");
        }

        /// <summary>
        /// 加入用户购物车
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("addUserOrderCart/{UserId}")]
        public dynamic addUserOrderCart(int UserId, AddorUpOrderCart addOrderCart)
        {
            var isuser = _Context.User.Where(x => x.Id == UserId).SingleOrDefault();
            if (isuser == null)
            {
                return DataStatus.DataError(1111, "用户不存在！");
            }

            var sku = addOrderCart.Sku;
            var issku = _Context.Skus.Where(x => x.SKU == sku).SingleOrDefault();
            if (issku == null)
            {
                return DataStatus.DataError(1111, "商品sku不存在！");
            }

            var Number = addOrderCart.Number;
            if (Number == 0)
            {
                return DataStatus.DataError(1111, "购买数量不能低于一件！");
            }

            var isOrderCartsku = _OrderCartRepository.Table.Where(x => x.Sku == sku && x.UserId==UserId).SingleOrDefault();
            if (isOrderCartsku != null)
            {
                isOrderCartsku.Number = isOrderCartsku.Number + Number;
                _OrderCartRepository.Update(isOrderCartsku);
                return DataStatus.DataSuccess(1000, isOrderCartsku, "加入购物车成功！");
            }

            var newOrderCart = new OrderCart
            {
                UserId = UserId,
                Sku = sku,
                Number = Number
            };

            _OrderCartRepository.Insert(newOrderCart);
            return DataStatus.DataSuccess(1000, newOrderCart, "加入购物车成功！");
        }

        /// <summary>
        /// 修改用户购物车
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        [Route("upUserOrderCart/{Id}")]
        public dynamic upUserOrderCart(int Id, AddorUpOrderCart upOrderCart)
        {
            var isOrderCart = _OrderCartRepository.Table.Where(x => x.Id == Id).SingleOrDefault();
            if (isOrderCart == null)
            {
                return DataStatus.DataError(1111, "购物车不存在该商品！");
            }

            var Number = upOrderCart.Number;
            if (Number == 0)
            {
                return DataStatus.DataError(1111, "购买数量不能低于一件！");
            }

            isOrderCart.Number = Number;
            _OrderCartRepository.Update(isOrderCart);
            return DataStatus.DataSuccess(1000, isOrderCart, "修改购物车数量成功！");
        }

        /// <summary>
        /// 获取商品详细信息
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("delUserOrderCart/{Id}")]
        public dynamic getProduct(int Id)
        {
            var isOrderCart = _OrderCartRepository.Table.Where(x => x.Id == Id).SingleOrDefault();
            if (isOrderCart == null)
            {
                return DataStatus.DataError(1111, "购物车不存在该商品！");
            }
            _OrderCartRepository.Delete(Id);
            return DataStatus.DataSuccess(1000, Id, "清除购物车商品成功！");
        }

    }
}