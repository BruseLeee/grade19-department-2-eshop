using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Electronic.emporium.Api.Repository;
using Electronic.emporium.Api.Entity;
using Electronic.emporium.Api.Utils;
using System.Linq;
using Electronic.emporium.Api.Params;
using Microsoft.AspNetCore.Authorization;
using Electronic.emporium.Api.Database;
using System;
namespace Electronic.emporium.Api.Controllers
{
    /// <summary>
    /// 通用控制器
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class GeneralController : ControllerBase
    {
        private IConfiguration _configuration;

        /// <summary>
        /// 所有数据库
        /// </summary>
        ElectronicDb _Context = new ElectronicDb();

        public GeneralController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        /// <summary>
        /// 获取密保
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("problem")]
        public dynamic GetProblem()
        {
            var problem = _Context.Problem.Where(x => x.IsActived == true).ToList();
            return DataStatus.DataSuccess(1000, problem, "获取密保问题成功！");
        }

        /// <summary>
        /// 获取指定用户的收货地址
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getuseraddress/{userId}")]
        public dynamic getuseraddress(int userId)
        {
            var UserAddress = _Context.ReceiveAddress.Where(x => x.UserId == userId).ToList();
            return DataStatus.DataSuccess(1000, UserAddress, "获取用户地址成功！");
        }

        /// <summary>
        /// 获取父类
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getParentCategory")]
        public dynamic getParentCategory()
        {
            var ParentCategory = _Context.ParentCategory.Where(x => x.IsActived == true).ToList();
            return DataStatus.DataSuccess(1000, ParentCategory, "获取父类成功！");
        }

        /// <summary>
        /// 获取子类
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getsubCategory/{ParentId}")]
        public dynamic getsubCategory(int ParentId)
        {
            var isParent = _Context.ParentCategory.Where(x => x.Id == ParentId && x.IsActived == true).SingleOrDefault();
            if (isParent == null)
            {
                return DataStatus.DataError(1111, "父类不存在！");
            }
            var subCategory = _Context.SubCateGory.Where(x => x.ParentId == ParentId).ToList();
            return DataStatus.DataSuccess(1000, subCategory, "获取子类成功！");
        }

        /// <summary>
        /// 获取品牌
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getBrandInfo")]
        public dynamic getBrandInfo()
        {
            var BrandInfo = _Context.BrandInfo.Where(x=>x.IsActived == true).ToList();
            return DataStatus.DataSuccess(1000, BrandInfo, "获取品牌成功！");
        }

        /// <summary>
        /// 获取供应商
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getSupplierInfo")]
        public dynamic getSupplierInfo()
        {
            var SupplierInfo = _Context.SupplierInfo.Where(x=>x.IsActived == true).ToList();
            return DataStatus.DataSuccess(1000, SupplierInfo, "获取供应商成功！");
        }

        //获取商品详细信息
        [HttpGet]
        [Route("getProduct/{ProductId}")]
        public dynamic getProduct(int ProductId)
        {
            var Product = _Context.ProductInfo.Where(x=>x.Id == ProductId).Select(y=>new{
                Id=y.Id,
                Name=y.Name,
                PictureUrl=y.PictureUrl,
                Descript=y.Descript,
                Values=_Context.Skus.Where(x=>x.ProductId==ProductId).ToList()
            }).SingleOrDefault();
            return DataStatus.DataSuccess(1000, Product, "获取商品详细信息成功！");
        }

        /// <summary>
        /// 获取指定用户的收货地址
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getaddress/{userId}")]
        public dynamic getaddress(int userId)
        {
            var UserAddress = _Context.ReceiveAddress.Where(x => x.UserId == userId).Select(y=>new {
                id=y.Id,
                Address=y.Name+"  "+y.Phone+"  "+y.Province+y.City+y.Region+y.DetailAddress
            }).ToList();
            return DataStatus.DataSuccess(1000, UserAddress, "获取用户地址成功！");
        }        

    }
}