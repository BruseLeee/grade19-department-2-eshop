using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Electronic.emporium.Api.Repository;
using Electronic.emporium.Api.Entity;
using Electronic.emporium.Api.Utils;
using System.Linq;
using Electronic.emporium.Api.Params;
using Microsoft.AspNetCore.Authorization;
using Electronic.emporium.Api.Database;
using System;

namespace Electronic.emporium.Api.Controllers
{
    /// <summary>
    /// 品牌控制器
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class BrandInfoController : ControllerBase
    {
        /// <summary>
        /// 创建品牌存储仓库
        /// </summary>
        private IRepository<BrandInfo> _BrandInfoRepository;
        private IConfiguration _configuration;
        /// <summary>
        /// 所有数据库
        /// </summary>
        /// <returns></returns>
        ElectronicDb _Context = new ElectronicDb();

        public BrandInfoController(IConfiguration configuration, IRepository<BrandInfo> BrandInfoRepository)
        {
            _BrandInfoRepository = BrandInfoRepository;
            _configuration = configuration;
        }

        /// <summary>
        /// 获取商品
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getProduct/{Id}")]
        public dynamic getProduct(int Id)
        {
            var Product = _Context.ProductInfo
            .Where(x => x.IsActived == true && x.BrandId==Id)
            .ToList();
            return DataStatus.DataSuccess(1000, Product, "获取商品成功！");
        }



    }
}