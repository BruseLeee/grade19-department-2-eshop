using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Electronic.emporium.Api.Repository;
using Electronic.emporium.Api.Entity;
using Electronic.emporium.Api.Utils;
using System.Linq;
using Electronic.emporium.Api.Params;
using Microsoft.AspNetCore.Authorization;
using Electronic.emporium.Api.Database;
using System;

namespace Electronic.emporium.Api.Controllers
{
    /// <summary>
    /// 管理分类控制器
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class BackCategoryController : ControllerBase
    {
        private IConfiguration _configuration;
        /// <summary>
        /// 所有数据库
        /// </summary>
        /// <returns></returns>
        ElectronicDb _Context = new ElectronicDb();

        public BackCategoryController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        /// <summary>
        /// 获取父类
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getParentCategory")]
        public dynamic getParentCategory()
        {
            var ParentCategory = _Context.ParentCategory.ToList();
            return DataStatus.DataSuccess(1000, ParentCategory, "获取父类成功！");
        }

        /// <summary>
        /// 获取子类
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getsubCategory/{ParentId}")]
        public dynamic getsubCategory(int ParentId)
        {
            var isParent = _Context.ParentCategory.Where(x => x.Id == ParentId).SingleOrDefault();
            if (isParent == null)
            {
                return DataStatus.DataError(1111, "父类不存在！");
            }
            var subCategory = _Context.SubCateGory.Where(x => x.ParentId == ParentId).ToList();
            return DataStatus.DataSuccess(1000, subCategory, "获取子类成功！");
        }

        /// <summary>
        /// 添加父分类
        /// </summary>
        /// <param name="AddBackUserInfo"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("addParentCategory")]
        public dynamic addParentCategory(AddorUpBackCategory addParentCategory)
        {
            var name = addParentCategory.Name;

            if (string.IsNullOrEmpty(name))
            {
                return DataStatus.DataError(1111, "请检查必填字段是否都填写规范！");
            }

            var isName = _Context.ParentCategory.Where(x => x.Name.Equals(name)).SingleOrDefault();
            if (isName != null)
            {
                return DataStatus.DataError(1111, "该分类已存在，请勿重复添加！");
            }

            var AddParentCategory = new ParentCategory
            {
                Name = name,
                IsActived=true
            };
            _Context.ParentCategory.Add(AddParentCategory);
            _Context.SaveChanges();

            return DataStatus.DataSuccess(1000, AddParentCategory, "添加分类成功！");
        }

        /// <summary>
        /// 修改父分类
        /// </summary>
        /// <param name="UpBackUserInfo"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpParentCategory/{Id}")]
        public dynamic UpParentCategory(int Id, AddorUpBackCategory UpParentCategory)
        {
            var IsParentCategory = _Context.ParentCategory.Where(x => x.Id == Id).SingleOrDefault();
            if (IsParentCategory == null)
            {
                return DataStatus.DataError(1111, "分类不存在！");
            }
            var name = UpParentCategory.Name;

            if (string.IsNullOrEmpty(name))
            {
                return DataStatus.DataError(1111, "请检查必填字段是否都填写规范！");
            }

            var isName = _Context.ParentCategory.Where(x => x.Name.Equals(name) && x.Id != Id).SingleOrDefault();
            if (isName != null)
            {
                return DataStatus.DataError(1111, "该分类已存在，请勿重复！");
            }

            IsParentCategory.Name = name;

            _Context.ParentCategory.Update(IsParentCategory);
            _Context.SaveChanges();

            return DataStatus.DataSuccess(1000, IsParentCategory, "分类修改成功！");
        }

        /// <summary>
        /// 删除父类包含了子类一起删除
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("delParentCategory/{ParentId}")]
        public dynamic delParentCategory(int ParentId)
        {
            var IsParentCategory = _Context.ParentCategory.Where(x => x.Id == ParentId).SingleOrDefault();
            if (IsParentCategory == null)
            {
                return DataStatus.DataError(1111, "分类不存在！");
            }

            var isdel = _Context.ProductInfo.Where(x => x.ParentId == ParentId).ToList();
            if (isdel.Count() != 0)
            {
                return DataStatus.DataError(1111, "分类使用中无法删除！");
            }

            var IsSubCategory = _Context.SubCateGory.Where(x => x.ParentId == ParentId).ToList();

            _Context.Remove(IsParentCategory);
            _Context.RemoveRange(IsSubCategory);
            _Context.SaveChanges();
            return DataStatus.DataSuccess(1000, IsParentCategory, "分类删除成功！");
        }

        /// <summary>
        /// 添加子分类
        /// </summary>
        /// <param name="addsubCategory"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("addsubCategory/{ParentId}")]
        public dynamic addsubCategory(int ParentId, AddorUpBackCategory addsubCategory)
        {
            var isParent = _Context.ParentCategory.Where(x => x.Id == ParentId && x.IsActived==true).SingleOrDefault();
            if (isParent == null)
            {
                return DataStatus.DataError(1111, "父类不存在,或未启用！");
            }

            var name = addsubCategory.Name;

            if (string.IsNullOrEmpty(name))
            {
                return DataStatus.DataError(1111, "请检查必填字段是否都填写规范！");
            }

            var isName = _Context.SubCateGory.Where(x => x.Name.Equals(name)).SingleOrDefault();
            if (isName != null)
            {
                return DataStatus.DataError(1111, "该分类已存在，请勿重复添加！");
            }

            var AddSubCategory = new SubCategory
            {
                Name = name,
                ParentId = ParentId,
                IsActived=true
            };
            _Context.SubCateGory.Add(AddSubCategory);
            _Context.SaveChanges();

            return DataStatus.DataSuccess(1000, AddSubCategory, "添加分类成功！");
        }

        /// <summary>
        /// 修改子分类
        /// </summary>
        /// <param name="UpBackUserInfo"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("upsubCategory/{subId}")]
        public dynamic upsubCategory(int subId, AddorUpBackCategory upsubCategory)
        {
            var IssubCategory = _Context.SubCateGory.Where(x => x.Id == subId).SingleOrDefault();
            if (IssubCategory == null)
            {
                return DataStatus.DataError(1111, "分类不存在！");
            }
            var name = upsubCategory.Name;

            if (string.IsNullOrEmpty(name))
            {
                return DataStatus.DataError(1111, "请检查必填字段是否都填写规范！");
            }

            var isName = _Context.SubCateGory.Where(x => x.Name.Equals(name) && x.Id != subId).SingleOrDefault();
            if (isName != null)
            {
                return DataStatus.DataError(1111, "该分类已存在，请勿重复！");
            }

            IssubCategory.Name = name;

            _Context.SubCateGory.Update(IssubCategory);
            _Context.SaveChanges();

            return DataStatus.DataSuccess(1000, IssubCategory, "分类修改成功！");
        }

        /// <summary>
        /// 删除子类
        /// </summary>
        /// <param name="subId"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("delsubCategory/{subId}")]
        public dynamic delsubCategory(int subId)
        {
            var IssubCategory = _Context.SubCateGory.Where(x => x.Id == subId).SingleOrDefault();
            if (IssubCategory == null)
            {
                return DataStatus.DataError(1111, "分类不存在！");
            }

            var isdel = _Context.ProductInfo.Where(x => x.SubId == subId).ToList();
            if (isdel.Count() != 0)
            {
                return DataStatus.DataError(1111, "分类使用中无法删除！");
            }

            _Context.Remove(IssubCategory);
            _Context.SaveChanges();
            return DataStatus.DataSuccess(1000, IssubCategory, "分类删除成功！");
        }

        /// <summary>
        /// 是否启用父级
        /// </summary>
        /// <param name="ParentId">父级Id</param>
        /// <returns></returns>
        [HttpPut]
        [Route("Parentisactived/{ParentId}")]
        public dynamic Parentisactived(int ParentId)
        {
            var IsParent = _Context.ParentCategory.Where(x => x.Id == ParentId).SingleOrDefault();
            if (IsParent == null)
            {
                return DataStatus.DataError(1111, "不存在！");
            }

            if (IsParent.IsActived)
            {
                IsParent.IsActived = false;
            }
            else
            {
                IsParent.IsActived = true;
            }

            _Context.ParentCategory.Update(IsParent);
            _Context.SaveChanges();


            return DataStatus.DataSuccess(1000, IsParent, "修改是否启用成功！");
        }

        /// <summary>
        /// 是否启用子级
        /// </summary>
        /// <param name="subId">子级Id</param>
        /// <returns></returns>
        [HttpPut]
        [Route("subisactived/{subId}")]
        public dynamic subisactived(int subId)
        {
            var Issub = _Context.SubCateGory.Where(x => x.Id == subId).SingleOrDefault();
            if (Issub == null)
            {
                return DataStatus.DataError(1111, "父类不存在！");
            }

            if (Issub.IsActived)
            {
                Issub.IsActived = false;
            }
            else
            {
                Issub.IsActived = true;
            }

            _Context.SubCateGory.Update(Issub);
            _Context.SaveChanges();


            return DataStatus.DataSuccess(1000, Issub, "修改是否启用成功！");
        }


    }
}
