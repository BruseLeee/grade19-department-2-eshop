using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Electronic.emporium.Api.Repository;
using Electronic.emporium.Api.Entity;
using Electronic.emporium.Api.Utils;
using System.Linq;
using Electronic.emporium.Api.Params;
using Microsoft.AspNetCore.Authorization;
using Electronic.emporium.Api.Database;
using System;

namespace Electronic.emporium.Api.Controllers
{
    /// <summary>
    /// 管理品牌控制器
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class BackBrandInfoController : ControllerBase
    {
        /// <summary>
        /// 创建品牌存储仓库
        /// </summary>
        private IRepository<BrandInfo> _BrandInfoRepository;
        private IConfiguration _configuration;
        /// <summary>
        /// 所有数据库
        /// </summary>
        /// <returns></returns>
        ElectronicDb _Context = new ElectronicDb();

        public BackBrandInfoController(IConfiguration configuration, IRepository<BrandInfo> BrandInfoRepository)
        {
            _BrandInfoRepository = BrandInfoRepository;
            _configuration = configuration;
        }

        /// <summary>
        /// 获取品牌
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getBrandInfo")]
        public dynamic getBrandInfo()
        {
            var BrandInfo = _Context.BrandInfo.ToList();
            return DataStatus.DataSuccess(1000, BrandInfo, "获取品牌成功！");
        }

        /// <summary>
        /// 添加品牌信息
        /// </summary>
        /// <param name="AddBrand">传入前端数据实体</param>
        /// <returns></returns>
        [HttpPost]
        [Route("AddBrand")]
        public dynamic AddBrand(AddorUpBrand AddBrand)
        {
            var name = AddBrand.Name;
            var desc = AddBrand.Desc;
            var isRecommend = AddBrand.IsRecommend == false ? false : true;

            if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(desc))
            {
                return DataStatus.DataError(1111, "请检查必填字段是否都填写规范！");
            }

            var IsBrand = _BrandInfoRepository.Table.Where(x => x.Name == name).SingleOrDefault();
            if (IsBrand != null)
            {
                return DataStatus.DataError(1111, "该品牌已存在，请勿重复使用！");
            }

            var newBrand = new BrandInfo
            {
                Name = name,
                Desc = desc,
                IsRecommend = isRecommend
            };

            _BrandInfoRepository.Insert(newBrand);
            return DataStatus.DataSuccess(1000, newBrand, "添加品牌信息成功！");
        }

        /// <summary>
        /// 修改品牌信息
        /// </summary>
        /// <param name="UpBrand">传入前端数据实体</param>
        /// <returns></returns>
        [HttpPut]
        [Route("UpBrand/{brandId}")]
        public dynamic UpBrand(int brandId, AddorUpBrand UpBrand)
        {
            var isBrand = _BrandInfoRepository.GetId(brandId);
            if (isBrand == null)
            {
                return DataStatus.DataError(1111, "品牌不存在！");
            }

            var name = UpBrand.Name;
            var desc = UpBrand.Desc;
            var isRecommend = UpBrand.IsRecommend == false ? false : true;

            if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(desc))
            {
                return DataStatus.DataError(1111, "请检查必填字段是否都填写规范！");
            }

            var Isname = _BrandInfoRepository.Table.Where(x => x.Name == name && x.Id != brandId).SingleOrDefault();
            if (Isname != null)
            {
                return DataStatus.DataError(1111, "该品牌已存在，请勿重复使用！");
            }

            isBrand.Name = name;
            isBrand.Desc = desc;
            isBrand.IsRecommend = isRecommend;


            _BrandInfoRepository.Update(isBrand);
            return DataStatus.DataSuccess(1000, isBrand, "修改品牌信息成功！");
        }

        
        /// <summary>
        /// 删除品牌
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("delBrand/{BrandId}")]
        public dynamic delBrand(int BrandId)
        {
            var IsBrand = _BrandInfoRepository.Table.Where(x => x.Id == BrandId).SingleOrDefault();
            if (IsBrand == null)
            {
                return DataStatus.DataError(1111, "品牌不存在！");
            }

            var isdel = _Context.ProductInfo.Where(x => x.BrandId == BrandId).ToList();
            if (isdel.Count() != 0)
            {
                return DataStatus.DataError(1111, "品牌使用中无法删除！");
            }

            _BrandInfoRepository.Delete(BrandId);

            return DataStatus.DataSuccess(1000, BrandId, "品牌删除成功！");
        }


    }
}