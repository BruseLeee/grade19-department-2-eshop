using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Electronic.emporium.Api.Repository;
using Electronic.emporium.Api.Entity;
using Electronic.emporium.Api.Utils;
using System.Linq;
using Electronic.emporium.Api.Params;
using Microsoft.AspNetCore.Authorization;
using Electronic.emporium.Api.Database;
using System;

namespace Electronic.emporium.Api.Controllers
{
    /// <summary>
    /// 分类控制器
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class CategoryController : ControllerBase
    {
        private IConfiguration _configuration;
        /// <summary>
        /// 所有数据库
        /// </summary>
        /// <returns></returns>
        ElectronicDb _Context = new ElectronicDb();

        public CategoryController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        /// <summary>
        /// 获取父类
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getCategory")]
        public dynamic getCategory()
        {
            var Category = _Context.ParentCategory
            .Where(x => x.IsActived == true)
            .Select(z => new
            {
                PId = z.Id,
                PName = z.Name,
                SubCateGory = _Context.SubCateGory.Where(y => y.ParentId == z.Id && y.IsActived == true).Select(j => new
                {
                    SId = j.Id,
                    SName = j.Name
                }).ToList()
            }).ToList();

            return DataStatus.DataSuccess(1000, Category, "获取分类成功！");
        }

        /// <summary>
        /// 获取商品
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getProduct/{Id}")]
        public dynamic getProduct(int Id)
        {
            var Product = _Context.ProductInfo
            .Where(x => x.IsActived == true && x.SubId==Id)
            .ToList();
            return DataStatus.DataSuccess(1000, Product, "获取商品成功！");
        }


    }
}
