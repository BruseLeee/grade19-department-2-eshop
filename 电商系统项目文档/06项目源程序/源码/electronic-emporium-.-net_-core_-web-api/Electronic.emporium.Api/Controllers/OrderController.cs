using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Electronic.emporium.Api.Repository;
using Electronic.emporium.Api.Entity;
using Electronic.emporium.Api.Utils;
using System.Linq;
using Electronic.emporium.Api.Params;
using Microsoft.AspNetCore.Authorization;
using Electronic.emporium.Api.Database;
using System;
namespace Electronic.emporium.Api.Controllers
{
    /// <summary>
    /// 订单控制器
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class OrderController : ControllerBase
    {
        /// <summary>
        /// 创建订单存储仓库
        /// </summary>
        private IRepository<Order> _OrderRepository;
        /// <summary>
        /// 创建订单详细存储仓库
        /// </summary>
        private IRepository<OrderInfo> _OrderInfoRepository;
        private IConfiguration _configuration;
        /// <summary>
        /// 所有数据库
        /// </summary>
        /// <returns></returns>
        ElectronicDb _Context = new ElectronicDb();

        public OrderController(IConfiguration configuration, IRepository<Order> OrderRepository, IRepository<OrderInfo> OrderInfoRepository)
        {
            _OrderRepository = OrderRepository;
            _OrderInfoRepository = OrderInfoRepository;
            _configuration = configuration;
        }

        /// <summary>
        /// 提交订单
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("addorder/{UserId}")]
        public dynamic addorder(int UserId, AddorUpOrder addOrder)
        {
            var isuser = _Context.User.Where(x => x.Id == UserId).SingleOrDefault();
            if (isuser == null)
            {
                return DataStatus.DataError(1111, "用户不存在！");
            }

            var addressId = addOrder.ReceiveAddressId;
            var isaddress = _Context.ReceiveAddress.Where(x => x.UserId == UserId && x.Id == addressId).SingleOrDefault();
            if (isaddress == null)
            {
                return DataStatus.DataError(1111, "用户地址不存在！");
            }

            var money = addOrder.OrderMoney;

            var newOrder = new Order
            {
                UserId = UserId,
                ReceiveAddressId = addressId,
                OrderMoney = money
            };

            _OrderRepository.Insert(newOrder);

            var OrderCart = _Context.OrderCart.Where(x => x.UserId == UserId).ToList();
            foreach (var x in OrderCart)
            {
                var newOrderinfo = new OrderInfo
                {
                    OrderId = newOrder.Id,
                    Sku = x.Sku,
                    Number = x.Number
                };
                _OrderInfoRepository.Insert(newOrderinfo);
            }
            _Context.OrderCart.RemoveRange(OrderCart);
            _Context.SaveChanges();

            return DataStatus.DataSuccess(1000, newOrder, "订单提交成功！");
        }

        /// <summary>
        /// 获取用户订单
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getuserOrder/{UserId}")]
        public dynamic getuserOrder(int UserId)
        {
            var isuser = _Context.User.Where(x => x.Id == UserId).SingleOrDefault();
            if (isuser == null)
            {
                return DataStatus.DataError(1111, "用户不存在！");
            }

            var Order = _OrderRepository.Table.Where(x=>x.UserId==UserId).ToList();
            return DataStatus.DataSuccess(1000, Order, "获取订单成功！");
        }

        /// <summary>
        /// 获取用户订单详情
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getuserOrderinfo/{OrderId}")]
        public dynamic getuserOrderinfo(int OrderId)
        {
            var isOrderId = _OrderRepository.Table.Where(x => x.Id == OrderId).SingleOrDefault();
            if (isOrderId == null)
            {
                return DataStatus.DataError(1111, "订单不存在！");
            }

            var Orderdb=_OrderInfoRepository.Table.Where(x=>x.OrderId==OrderId).ToList();

            var Order =Orderdb.Join(_Context.Skus,Pet => Pet.Sku, per => per.SKU, (pet, per) => new{
                Id=pet.Id,
                ProductId=per.ProductId,
                typevalue=per.KeyValueJson,
                Number=pet.Number
            }).ToList();

            var Orderinfo=Order.Join(_Context.ProductInfo,Pet => Pet.ProductId, per => per.Id, (pet, per) => new{
                Id=pet.Id,
                ProductName=per.Name,
                typevalue=pet.typevalue,
                Number=pet.Number
            }).ToList();

            return DataStatus.DataSuccess(1000, Orderinfo, "获取订单详情成功！");
        }

        /// <summary>
        /// 获取所有订单
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getOrder")]
        public dynamic getOrder([FromQuery] PagerParams query)
        {
            var pageIndex = query.PageIndex;//当前页码
            var pageSize = query.PageSize;//每页条数

            var dbOrder = _OrderRepository.Table.ToList();
            var Order=dbOrder.Join(_Context.ReceiveAddress,Pet => Pet.ReceiveAddressId, per => per.Id, (pet, per) => new{
                Id=pet.Id,
                ReceiveAddress=per.Name+" "+per.Phone+" "+per.Province+per.City+per.Region+per.DetailAddress,
                OrderMoney=pet.OrderMoney
            }).ToList();

            var order = Order.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();

            return DataStatus.DataSuccess(1000, new {
                data=order,
                pager = new {pageIndex,pageSize,rowsTotal=Order.Count()}
            }, "获取订单成功！");
        }

    }
}