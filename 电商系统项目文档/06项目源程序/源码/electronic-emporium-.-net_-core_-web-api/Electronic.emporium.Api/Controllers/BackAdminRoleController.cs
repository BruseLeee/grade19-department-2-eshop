//这是角色控制器管理员角色分配也放这里

using System.Linq;
using Electronic.emporium.Api.Database;
using Electronic.emporium.Api.Entity;
using Electronic.emporium.Api.Params;
using Electronic.emporium.Api.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace Electronic.emporium.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class BackAdminRoleController : ControllerBase
    {
        ElectronicDb _Context = new ElectronicDb();
        private IRepository<AdminRole> _adminRoleRepository;
        private IConfiguration _configuration;
        public BackAdminRoleController(
            IConfiguration configuration,
            IRepository<AdminRole> adminRoleRepository
        )
        {
            _adminRoleRepository = adminRoleRepository;
            _configuration = configuration;
        }

        /// <summary>
        /// 管理管理员权限
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("addadminrole")]
        public dynamic AdminRole(AdminRoleParams newAdminRole)
        {
            var adminname = newAdminRole.AdminName;
            var rolename = newAdminRole.RoleName;

            var admin = _Context.AdminUser.Where(x => x.Username == adminname).SingleOrDefault();
            var role = _Context.Role.Where(x => x.Name == rolename).SingleOrDefault();

            if (string.IsNullOrEmpty(adminname) && string.IsNullOrEmpty(rolename))
            {
                return DataStatus.DataError(1111, "传入的权限和管理员名称不能为空");
            }

            if (admin == null || role == null)
            {
                return DataStatus.DataError(11112, "管理员或权限不存在，请重新输入！");
            }

            var adminId = admin.Id;
            var roleId = role.Id;

            var isAdminRole = _adminRoleRepository.Table.Where(x => x.AdminId == adminId && x.RoleId == roleId).SingleOrDefault();

            if (isAdminRole != null)
            {
                return DataStatus.DataError(1113, "该管理员已有权限，请重新设置！");
            }

            var adminrole = new AdminRole
            {
                AdminId = adminId,
                RoleId = roleId
            };

            _adminRoleRepository.Insert(adminrole);

            return DataStatus.DataSuccess(1000, new { AdminName = adminname, RoleName = rolename }, "查询成功");
        }

        /// <summary>
        /// 查询管理员角色
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getadminrole")]
        public dynamic getAdminRole()
        {
            var adminrole = _adminRoleRepository.Table.ToList();
            var adminname = _Context.AdminRole.Join(_Context.AdminUser, ar => ar.AdminId, au => au.Id, (ar, au) => new AdminRoleViewParams
            {
                Id=ar.Id,
                AdminName = au.Username,
                AdminId = ar.AdminId,
                RoleId = ar.RoleId
            });
            var rolename = _Context.AdminRole.Join(_Context.Role, ar => ar.AdminId, au => au.Id, (ar, au) => new AdminRoleViewParams
            {
                Id=ar.Id,
                RoleName = au.Name,
                AdminId = ar.AdminId,
                RoleId = ar.RoleId
            });

            var adminroleInfo = adminname.Join(rolename, an => an.AdminId,rn=>rn.AdminId,(an,rn)=>new AdminRoleViewParams{
                Id=an.Id,
                AdminName=an.AdminName,
                RoleName=rn.RoleName,
                AdminId=an.AdminId,
                RoleId=rn.RoleId
            });
            return DataStatus.DataSuccess(1000, adminroleInfo, "查询管理员角色表成功");
        }

        /// <summary>
        /// 查询指定管理员角色
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getupadminrole/{id}")]
        public dynamic getUpAdminRole(int id)
        {
            var adminrole = _adminRoleRepository.GetId(id);

            var adminname =_Context.AdminUser.Where(x =>x.Id==adminrole.AdminId).SingleOrDefault().Username;
            var rolename =_Context.Role.Where(x =>x.Id==adminrole.RoleId).SingleOrDefault().Name;
            
            return DataStatus.DataSuccess(1000, new { AdminName = adminname, RoleName = rolename }, "查询管理员角色表成功");
        }


        /// <summary>
        /// 修改管理员权限
        /// </summary>
        /// <param name="upAdminRole"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("upadminrole/{id}")]
        public dynamic upAdminRole(AdminRoleParams upAdminRole, int id)
        {
            var adminrole = _adminRoleRepository.GetId(id);

            var adminname = upAdminRole.AdminName;
            var rolename = upAdminRole.RoleName;

            var admin = _Context.AdminUser.Where(x => x.Username == adminname).SingleOrDefault();
            var role = _Context.Role.Where(x => x.Name == rolename).SingleOrDefault();

            if (adminname == null && rolename == null)
            {
                return DataStatus.DataError(1111, "传入的权限和管理员名称不能为空");
            }

            if (admin == null || role == null)
            {
                return DataStatus.DataError(1112, "管理员或权限不存在，请重新输入！");
            }

            var adminId = admin.Id;
            var roleId = role.Id;

            var isAdminRole = _adminRoleRepository.Table.Where(x => x.AdminId == adminId && x.RoleId == roleId).SingleOrDefault();

            if (isAdminRole != null)
            {
                return DataStatus.DataError(1113, "该管理员已有权限，请重新设置！");
            }

            adminrole.AdminId = adminId;
            adminrole.RoleId = roleId;

            _adminRoleRepository.Update(adminrole);

            return DataStatus.DataSuccess(1000, new { AdminName = adminname, RoleName = rolename }, "权限修改成功");
        }

        [HttpDelete]
        [Route("deleteadminrole/{id}")]
        public dynamic deleteAdminrole(int id)
        {
            var adminrole = _adminRoleRepository.GetId(id);
            _adminRoleRepository.Delete(id);
            return DataStatus.DataSuccess(1000, id, "删除成功");
        }
    }
}