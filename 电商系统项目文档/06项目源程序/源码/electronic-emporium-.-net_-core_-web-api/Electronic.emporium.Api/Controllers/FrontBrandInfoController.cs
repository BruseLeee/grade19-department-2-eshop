using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Electronic.emporium.Api.Repository;
using Electronic.emporium.Api.Entity;
using Electronic.emporium.Api.Utils;
using System.Linq;
using Electronic.emporium.Api.Params;
using Microsoft.AspNetCore.Authorization;
using Electronic.emporium.Api.Database;
using System;

namespace Electronic.emporium.Api.Controllers
{
    /// <summary>
    /// 管理品牌控制器
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class FrontBrandInfoController : ControllerBase
    {
        /// <summary>
        /// 创建品牌存储仓库
        /// </summary>
        private IRepository<BrandInfo> _BrandInfoRepository;
        private IConfiguration _configuration;
        /// <summary>
        /// 所有数据库
        /// </summary>
        /// <returns></returns>
        ElectronicDb _Context = new ElectronicDb();

        public FrontBrandInfoController(IConfiguration configuration, IRepository<BrandInfo> BrandInfoRepository)
        {
            _BrandInfoRepository = BrandInfoRepository;
            _configuration = configuration;
        }

        /// <summary>
        /// 根据品牌ID获取品牌信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getBrandInfo/{brandId}")]
        public dynamic getBrandInfo(int brandId)
        {
            var BrandId = _Context.BrandInfo.Where(x => x.Id == brandId).SingleOrDefault();

            if (BrandId == null)
            {
                return DataStatus.DataError(1111, "该品牌不存在!");
            }

            var BrandInfo = _BrandInfoRepository.GetId(brandId);

            return DataStatus.DataSuccess(1000, BrandInfo, "获取品牌成功！");
        }



    }
}