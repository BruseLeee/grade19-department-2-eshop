using System.Linq;
using Electronic.emporium.Api.Database;
using Electronic.emporium.Api.Entity;
using Electronic.emporium.Api.Params;
using Electronic.emporium.Api.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace Electronic.emporium.Api.Controllers
{
    /// <summary>
    /// 角色控制器
    /// </summary>    
    [ApiController]
    [Route("[controller]")]
    public class BackRoleController : ControllerBase
    {
        /// <summary>
        /// 创建角色存储仓库
        /// </summary>
        public IRepository<Role> _roleRepository;
        public IConfiguration _configuration;
        ElectronicDb _Context = new ElectronicDb();

        public BackRoleController(
            IRepository<Role> roleRepository,
            IConfiguration configuration
        ){
            _roleRepository=roleRepository;
            _configuration=configuration;
        }

        /// <summary>
        /// 添加角色
        /// </summary>
        /// <param name="addRole"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("addrole")]
        public dynamic addRole(BackRoleParams addRole)
        {
            var name=addRole.Name;

            if(string.IsNullOrEmpty(name))
            {
                return DataStatus.DataError(1111,"注册的角色名称不能为空，请重新输入角色名称!");
            }

            var role=_roleRepository.Table.Where(x=>x.Name==name).SingleOrDefault();

            if(role != null)
            {
                return DataStatus.DataError(1112,"该角色已存在，请重新输入！");
            }

            var newRole=new Role
            {
                Name=name
            };

            _roleRepository.Insert(newRole);

            return DataStatus.DataSuccess(1000,newRole,"注册角色成功");
        }

        /// <summary>
        /// 查询角色控制器
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getrole")]
        public dynamic getRole()
        {
            var role = _roleRepository.Table.ToList();
            return DataStatus.DataSuccess(1000,role,"查询角色列表成功");
        }

        /// <summary>
        /// 查询指定角色控制器
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getuprole/{id}")]
        public dynamic getUpRole(int id)
        {
            var role = _roleRepository.GetId(id);
            return DataStatus.DataSuccess(1000,role,"查询角色列表成功");
        }

        /// <summary>
        /// 删除角色控制器
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("deleterole/{id}")]
        public dynamic deleteRole(int id)
        {
            var role=_roleRepository.GetId(id);
            if(role == null)
            {
                return DataStatus.DataError(1111,"该角色不存在，删除失败");
            }
            _roleRepository.Delete(id);
            return DataStatus.DataSuccess(1000,id,"删除成功");
        }

        /// <summary>
        /// 修改角色控制器
        /// </summary>
        /// <param name="upRole"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("uprole/{id}")]
        public dynamic upRole(BackRoleParams upRole,int id)
        {
            var role=_roleRepository.GetId(id);
            var name = upRole.Name;

            if(role == null)
            {
                return DataStatus.DataError(1111,"修改的角色不存在，请重新输入！");
            }

            if(string.IsNullOrEmpty(name))
            {
                return DataStatus.DataError(1112,"请规范填写必填字段！");
            }

            var isRole = _roleRepository.Table.Where(x=>x.Name==name).SingleOrDefault();
            if(isRole != null)
            {
                return DataStatus.DataError(1113,"该角色一存在，请重新输入！");
            }

            role.Name=name;
            _roleRepository.Update(role);

            return DataStatus.DataSuccess(1000,role,"修改角色成功");
        }
    }
}