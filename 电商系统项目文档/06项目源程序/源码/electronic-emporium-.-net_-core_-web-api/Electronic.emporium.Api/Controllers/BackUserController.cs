using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Electronic.emporium.Api.Repository;
using Electronic.emporium.Api.Entity;
using Electronic.emporium.Api.Utils;
using System.Linq;
using Electronic.emporium.Api.Params;
using Microsoft.AspNetCore.Authorization;
using Electronic.emporium.Api.Database;
using System;

namespace Electronic.emporium.Api.Controllers
{
    /// <summary>
    /// 管理用户控制器
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class BackUserController : ControllerBase
    {
        /// <summary>
        /// 创建用户存储仓库
        /// </summary>
        private IRepository<User> _userRepository;
        private IConfiguration _configuration;
        /// <summary>
        /// 所有数据库
        /// </summary>
        /// <returns></returns>
        ElectronicDb _Context = new ElectronicDb();

        public BackUserController(IConfiguration configuration, IRepository<User> userRepository)
        {
            _userRepository = userRepository;
            _configuration = configuration;
        }

        /// <summary>
        /// 获取所有用户数据请求
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getusersinfoView")]
        public dynamic GetUsersInfoView([FromQuery] PagerParams query)
        {
            var pageIndex = query.PageIndex;//当前页码
            var pageSize = query.PageSize;//每页条数

            var UserInfoView = _Context.UserInfo.Join(_Context.User.Where(x=>x.IsDeleted==false), Pet => Pet.UserId, per => per.Id, (pet, per) => new
            {
                Id = per.Id,
                Username = per.Username,
                Nickname = pet.Nickname,
                Gender = pet.Gender,
                IsActived = per.IsActived,
                CreatedTime = per.CreatedTime,
            });

            var userInfo = UserInfoView.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();

            return DataStatus.DataSuccess(1000, new {
                data=userInfo,
                pager = new {pageIndex,pageSize,rowsTotal = UserInfoView.Count()}
            }, "获取用户模块成功");
        }

        /// <summary>
        /// 获取用户详细信息
        /// </summary>
        /// <param name="userId">用户Id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getusersinfo/{userId}")]
        public dynamic GetUsersInfo(int userId)
        {
            var IsUser = _userRepository.GetId(userId);
            if (IsUser == null)
            {
                return DataStatus.DataError(1111, "用户不存在！");
            }

            var UserInfo = _Context.UserInfo.Where(x => x.UserId == IsUser.Id).Join(_Context.User, Pet => Pet.UserId, per => per.Id, (pet, per) => new
            {
                Id = per.Id,
                Username = per.Username,
                ProblemId = per.ProblemId,
                Answer = per.Answer,
                Password = per.Password,
                Nickname = pet.Nickname,
                Gender = pet.Gender,
            });

            return DataStatus.DataSuccess(1000, UserInfo, "获取用户详细信息显示成功");
        }

        /// <summary>
        /// 修改用户信息
        /// </summary>
        /// <param name="userId">用户Id</param>
        /// <param name="UpBackUserInfo">传入前端数据实体</param>
        /// <returns></returns>
        [HttpPut]
        [Route("upusersinfo/{userId}")]
        public dynamic upusersinfo(int userId, AddorUpBackUserInfo UpBackUserInfo)
        {
            var IsUser = _userRepository.GetId(userId);
            if (IsUser == null)
            {
                return DataStatus.DataError(1111, "用户不存在！");
            }

            var userName = UpBackUserInfo.Username;
            var userPassword = UpBackUserInfo.Password==""?IsUser.Password:UpBackUserInfo.Password;
            var problemId = UpBackUserInfo.ProblemId;
            var answer = UpBackUserInfo.Answer;
            var nickname = UpBackUserInfo.Nickname;
            var gender = UpBackUserInfo.Gender;

            if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(userPassword) || problemId == 0 || string.IsNullOrEmpty(nickname) || string.IsNullOrEmpty(gender))
            {
                return DataStatus.DataError(1111, "请检查必填字段是否都填写规范！");
            }

            var IsUserInfo = _Context.UserInfo.Where(x => x.UserId == userId).SingleOrDefault();
            if (IsUserInfo == null)
            {
                return DataStatus.DataError(1111, "没有找到该用户信息！");
            }

            var IsUserName = _userRepository.Table.Where(x => x.Username == userName && x.Id != userId).SingleOrDefault();
            if (IsUserName != null)
            {
                return DataStatus.DataError(1111, "该用户已存在，请勿重复使用！");
            }

            IsUser.Username = userName;
            IsUser.Password = userPassword;
            IsUser.ProblemId = problemId;
            IsUser.Answer = answer;
            IsUserInfo.Nickname = nickname;
            IsUserInfo.Gender = gender;

            _userRepository.Update(IsUser);
            _Context.UserInfo.Update(IsUserInfo);
            _Context.SaveChanges();

            var UpUserInfo = new
            {
                Id = IsUser.Id,
                Username = IsUser.Username,
                Nickname = IsUserInfo.Nickname,
                Gender = IsUserInfo.Gender,
                IsActived = IsUser.IsActived,
                CreatedTime = IsUser.CreatedTime,
            };

            return DataStatus.DataSuccess(1000, UpUserInfo, "修改用户信息成功");
        }

        /// <summary>
        /// 添加用户信息
        /// </summary>
        /// <param name="newUser">传入前端数据实体</param>
        /// <returns></returns>
        [HttpPost]
        [Route("adduserInfo")]
        public dynamic adduserInfo(AddorUpBackUserInfo AddBackUserInfo)
        {

            var userName = AddBackUserInfo.Username;
            var userPassword = AddBackUserInfo.Password;
            var problemId = AddBackUserInfo.ProblemId;
            var answer = AddBackUserInfo.Answer;
            var nickname = AddBackUserInfo.Nickname;
            var gender = AddBackUserInfo.Gender;

            if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(userPassword) || problemId == 0 || string.IsNullOrEmpty(answer) || string.IsNullOrEmpty(nickname) || string.IsNullOrEmpty(gender))
            {
                return DataStatus.DataError(1111, "请检查必填字段是否都填写规范！");
            }

            var IsUserName = _userRepository.Table.Where(x => x.Username == userName).SingleOrDefault();
            if (IsUserName != null)
            {
                return DataStatus.DataError(1111, "该用户已存在，请勿重复使用！");
            }

            var newUser = new User
            {
                Username = userName,
                Password = userPassword,
                ProblemId = problemId,
                Answer = answer
            };

            var IsUserInfo = _Context.UserInfo.Where(x => x.UserId == newUser.Id).SingleOrDefault();
            if (IsUserInfo != null)
            {
                return DataStatus.DataError(1111, "添加失败该用户Id已经存在信息，请先处理！");
            }

            _userRepository.Insert(newUser);
            var newUserInfo = new UserInfo
            {
                UserId = newUser.Id,
                Nickname = nickname,
                Gender = gender,
                IconUrl = "/UserImg/DefaultImg.png"

            };

            _Context.UserInfo.Add(newUserInfo);
            _Context.SaveChanges();

            var AddUserInfo = new
            {
                Id = newUser.Id,
                Username = newUser.Username,
                Nickname = newUserInfo.Nickname,
                Gender = newUserInfo.Gender,
                IsActived = newUser.IsActived,
                CreatedTime = newUser.CreatedTime,
            };
            return DataStatus.DataSuccess(1000, AddUserInfo, "添加用户信息成功");
        }

        /// <summary>
        /// 是否启用
        /// </summary>
        /// <param name="userId">用户Id</param>
        /// <returns></returns>
        [HttpPut]
        [Route("isactived/{userId}")]
        public dynamic isactived(int userId)
        {
            var IsUser = _userRepository.GetId(userId);
            if (IsUser == null)
            {
                return DataStatus.DataError(1111, "用户不存在！");
            }
            var IsUserInfo = _Context.UserInfo.Where(x => x.UserId == userId).SingleOrDefault();
            if (IsUserInfo == null)
            {
                return DataStatus.DataError(1111, "没有找到该用户信息！");
            }

            if (IsUser.IsActived)
            {
                IsUser.IsActived = false;
            }
            else
            {
                IsUser.IsActived = true;
            }

            _userRepository.Update(IsUser);

            var isactived = new
            {
                Id = IsUser.Id,
                Username = IsUser.Username,
                Nickname = IsUserInfo.Nickname,
                Gender = IsUserInfo.Gender,
                IsActived = IsUser.IsActived,
                CreatedTime = IsUser.CreatedTime,
            };

            return DataStatus.DataSuccess(1000, isactived, "修改是否启用成功！");
        }

        /// <summary>
        /// 是否删除
        /// </summary>
        /// <param name="userId">用户Id</param>
        /// <returns></returns>
        [HttpPut]
        [Route("isdeleted/{userId}")]
        public dynamic isdeleted(int userId)
        {
            var IsUser = _userRepository.GetId(userId);
            if (IsUser == null)
            {
                return DataStatus.DataError(1111, "用户不存在！");
            }
            var IsUserInfo = _Context.UserInfo.Where(x => x.UserId == userId).SingleOrDefault();
            if (IsUserInfo == null)
            {
                return DataStatus.DataError(1111, "没有找到该用户信息！");
            }

            if (!IsUser.IsDeleted)
            {
                IsUser.IsDeleted = true;
            }

            _userRepository.Update(IsUser);

            var isactived = new
            {
                Id = IsUser.Id,
                Username = IsUser.Username,
                Nickname = IsUserInfo.Nickname,
                Gender = IsUserInfo.Gender,
                IsActived = IsUser.IsActived,
                CreatedTime = IsUser.CreatedTime,
            };

            return DataStatus.DataSuccess(1000, isactived, "删除用户信息成功!");
        }
    }
}