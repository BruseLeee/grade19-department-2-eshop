using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Electronic.emporium.Api.Repository;
using Electronic.emporium.Api.Entity;
using Electronic.emporium.Api.Utils;
using System.Linq;
using Electronic.emporium.Api.Params;
using Microsoft.AspNetCore.Authorization;
using Electronic.emporium.Api.Database;
using System;
using Newtonsoft.Json;
/// <summary>
/// 未完成！！
/// </summary>

namespace Electronic.emporium.Api.Controllers
{
    /// <summary>
    /// 管理用户地址控制器
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class BackUserAddressController : ControllerBase
    {
        /// <summary>
        /// 创建用户地址存储仓库
        /// </summary>
        private IRepository<ReceiveAddress> _ReceiveAddressRepository;
        private IRepository<User> _userRepository;
        private IConfiguration _configuration;
        /// <summary>
        /// 所有数据库
        /// </summary>
        /// <returns></returns>
        ElectronicDb _Context = new ElectronicDb();

        public BackUserAddressController(IConfiguration configuration, IRepository<ReceiveAddress> ReceiveAddressRepository, IRepository<User> userRepository)
        {
            _userRepository = userRepository;
            _ReceiveAddressRepository = ReceiveAddressRepository;
            _configuration = configuration;
        }

        /// <summary>
        /// 用户地址添加
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("addUserAddress/{userId}")]
        public dynamic addUserAddress(int userId, AddoorUpAddress addUserAddress)
        {
            var IsUser = _userRepository.GetId(userId);

            if (IsUser == null)
            {
                return DataStatus.DataError(1111, "该用户不存在！");
            }
            var Username = addUserAddress.Name;
            var Phone = addUserAddress.Phone;
            var Province = addUserAddress.Province;
            var City = addUserAddress.City;
            var Region = addUserAddress.Region;
            var DetailAddress = addUserAddress.DetailAddress;

            if (string.IsNullOrEmpty(Username) || string.IsNullOrEmpty(Phone) || string.IsNullOrEmpty(Province) || string.IsNullOrEmpty(City) || string.IsNullOrEmpty(Region) || string.IsNullOrEmpty(DetailAddress))
            {
                return DataStatus.DataError(1111, "请检查必填字段是否都填写规范！");
            }

            var userAddress = new ReceiveAddress
            {
                UserId = userId,
                Name = Username,
                Phone = Phone,
                Province = Province,
                City = City,
                Region = Region,
                DetailAddress = DetailAddress
            };

            _ReceiveAddressRepository.Insert(userAddress);
            var newuserAddress = new
            {
                Id = userAddress.Id,
                UserId = userId,
                Name = Username,
                Phone = Phone,
                Province = Province,
                City = City,
                Region = Region,
                DetailAddress = DetailAddress
            };
            return DataStatus.DataSuccess(1000, newuserAddress, "添加地址信息成功");
        }

        /// <summary>
        /// 获取用户的指定地址
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getUserAddress/{userId}")]
        public dynamic getBrandInfo(int userId)
        {
            var IsUser = _userRepository.GetId(userId);

            if (IsUser == null)
            {
                return DataStatus.DataError(1111, "该用户不存在！");
            }
            var address = _ReceiveAddressRepository.Table.Where(y => y.UserId == userId).Select(x => new ShowAddress
            {
                Id = x.Id,
                UserId = x.UserId,
                Name = x.Name,
                Phone = x.Phone,
                Province = x.Province,
                City = x.City,
                Region = x.Region,
                DetailAddress = x.DetailAddress
            }).ToList();

            return DataStatus.DataSuccess(1000, address, "获取地址信息成功");
        }

        /// <summary>
        /// 用户地址修改
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        [Route("upUserAddress/{addressId}")]
        public dynamic upUserAddress(int addressId, AddoorUpAddress upUserAddress)
        {
            var Isaddress = _ReceiveAddressRepository.GetId(addressId);
            if (Isaddress == null)
            {
                return DataStatus.DataError(1111, "该地址不存在！");
            }
            var Username = upUserAddress.Name;
            var Phone = upUserAddress.Phone;
            var Province = upUserAddress.Province;
            var City = upUserAddress.City;
            var Region = upUserAddress.Region;
            var DetailAddress = upUserAddress.DetailAddress;

            if (string.IsNullOrEmpty(Username) || string.IsNullOrEmpty(Phone) || string.IsNullOrEmpty(Province) || string.IsNullOrEmpty(City) || string.IsNullOrEmpty(Region) || string.IsNullOrEmpty(DetailAddress))
            {
                return DataStatus.DataError(1111, "请检查必填字段是否都填写规范！");
            }
            Isaddress.Name = Username;
            Isaddress.Phone = Phone;
            Isaddress.Province = Province;
            Isaddress.City = City;
            Isaddress.Region = Region;
            Isaddress.DetailAddress = DetailAddress;

            _ReceiveAddressRepository.Update(Isaddress);
            var upuserAddress = new
            {
                Id = Isaddress.Id,
                UserId = Isaddress.UserId,
                Name = Isaddress.Name,
                Phone = Isaddress.Phone,
                Province = Isaddress.Province,
                City = Isaddress.City,
                Region = Isaddress.Region,
                DetailAddress = Isaddress.DetailAddress
            };
            return DataStatus.DataSuccess(1000, upuserAddress, "修改地址信息成功");
        }

        /// <summary>
        /// 删除地址
        /// </summary>
        /// <param name="typeId"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("delUserAddress/{addressId}")]
        public dynamic delUserAddress(int addressId)
        {
            var Isaddress = _ReceiveAddressRepository.GetId(addressId);
            if (Isaddress == null)
            {
                return DataStatus.DataError(1111, "该地址不存在！");
            }

            _ReceiveAddressRepository.Delete(addressId);

            return DataStatus.DataSuccess(1000, addressId, "删除地址成功！");
        }
    }
}