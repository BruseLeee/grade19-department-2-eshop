using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Electronic.emporium.Api.Repository;
using Electronic.emporium.Api.Entity;
using Electronic.emporium.Api.Utils;
using System.Linq;
using Electronic.emporium.Api.Params;
using Microsoft.AspNetCore.Authorization;
using Electronic.emporium.Api.Database;
using System;

namespace Electronic.emporium.Api.Controllers
{
    /// <summary>
    /// 管理商品控制器
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class BackProductInfoController : ControllerBase
    {
        /// <summary>
        /// 创建商品存储仓库
        /// </summary>
        private IRepository<ProductInfo> _ProductInfoRepository;
        private IConfiguration _configuration;
        /// <summary>
        /// 所有数据库
        /// </summary>
        /// <returns></returns>
        ElectronicDb _Context = new ElectronicDb();

        public BackProductInfoController(IConfiguration configuration, IRepository<ProductInfo> ProductInfoRepository)
        {
            _ProductInfoRepository = ProductInfoRepository;
            _configuration = configuration;
        }

        /// <summary>
        /// 获取商品
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getProduct")]
        public dynamic getProduct([FromQuery] PagerParams query)
        {
            var pageIndex = query.PageIndex;//当前页码
            var pageSize = query.PageSize;//每页条数

            var problem = _Context.ProductInfo.Select(x=>new
            {
                Id = x.Id,
                Name = x.Name,
                CostPrice = x.CostPrice,
                PictureUrl = x.PictureUrl,
                IsRecommend = x.IsRecommend,
                IsNew = x.IsNew,
                IsActived = x.IsActived

            });

            var product = problem.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();

            return DataStatus.DataSuccess(1000, new{
                data = product,
                pager = new { pageIndex,pageSize,rowsTotal = problem.Count() }
            }, "获取商品成功！");
        }

        /// <summary>
        /// 添加商品信息
        /// </summary>
        /// <param name="AddProduct">传入前端数据实体</param>
        /// <returns></returns>
        [HttpPost]
        [Route("AddProduct")]
        public dynamic AddProduct(AddorUpProduct AddProduct)
        {
            var parentId = AddProduct.ParentId;
            var subId = AddProduct.SubId;
            var name = AddProduct.Name;
            var brandId = AddProduct.BrandId;
            var supplierId = AddProduct.SupplierId;
            var descript = AddProduct.Descript;
            var costPrice = AddProduct.CostPrice;
            var unit = AddProduct.Unit;
            var isRecommend = AddProduct.IsRecommend;
            var isNew = AddProduct.IsNew;

            var isParent = _Context.ParentCategory.Where(x => x.Id == parentId).SingleOrDefault();
            if (isParent == null)
            {
                return DataStatus.DataError(1111, "请检查必填字段是否都填写规范！");
            }

            var issub = _Context.SubCateGory.Where(x => x.Id == subId).SingleOrDefault();
            if (issub == null)
            {
                return DataStatus.DataError(1111, "请检查必填字段是否都填写规范！");
            }

            var isbrand = _Context.BrandInfo.Where(x => x.Id == brandId).SingleOrDefault();
            if (isbrand == null)
            {
                return DataStatus.DataError(1111, "请检查必填字段是否都填写规范！");
            }

            var issupplier = _Context.SubCateGory.Where(x => x.Id == supplierId).SingleOrDefault();
            if (issupplier == null)
            {
                return DataStatus.DataError(1111, "请检查必填字段是否都填写规范！");
            }

            if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(descript) || costPrice < 0)
            {
                return DataStatus.DataError(1111, "请检查必填字段是否都填写规范！");
            }

            var newProduct = new ProductInfo
            {
                ParentId = parentId,
                SubId = subId,
                Name = name,
                BrandId = brandId,
                SupplierId = supplierId,
                Descript = descript,
                CostPrice = costPrice,
                Unit = unit,
                IsRecommend = isRecommend,
                IsNew = isNew
            };

            _ProductInfoRepository.Insert(newProduct);
            return DataStatus.DataSuccess(1000, newProduct, "添加商品信息成功！");
        }

        [HttpDelete]
        [Route("delProduct/{Product}")]
        public dynamic delProduct(int Product)
        {
            var isProduct =_ProductInfoRepository.GetId(Product);
            if (isProduct == null)
            {
                return DataStatus.DataError(1111, "商品不存在！");
            }

            var sku=_Context.Skus.Where(x=>x.ProductId==Product).ToList();
            _Context.Skus.RemoveRange(sku);

            _ProductInfoRepository.Delete(Product); 
            _Context.SaveChanges();
            return DataStatus.DataSuccess(1000, Product, "删除成功！");
        }
    }
}