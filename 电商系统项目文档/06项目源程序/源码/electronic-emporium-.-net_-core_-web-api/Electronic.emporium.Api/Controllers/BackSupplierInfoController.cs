using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Electronic.emporium.Api.Repository;
using Electronic.emporium.Api.Entity;
using Electronic.emporium.Api.Utils;
using System.Linq;
using Electronic.emporium.Api.Params;
using Microsoft.AspNetCore.Authorization;
using Electronic.emporium.Api.Database;
using System;

namespace Electronic.emporium.Api.Controllers
{
    /// <summary>
    /// 管理供应商控制器
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class BackSupplierInfoController : ControllerBase
    {
        /// <summary>
        /// 创建供应商存储仓库
        /// </summary>
        private IRepository<SupplierInfo> _SupplierInfoRepository;
        private IConfiguration _configuration;
        /// <summary>
        /// 所有数据库
        /// </summary>
        /// <returns></returns>
        ElectronicDb _Context = new ElectronicDb();

        public BackSupplierInfoController(IConfiguration configuration, IRepository<SupplierInfo> SupplierInfoRepository)
        {
            _SupplierInfoRepository = SupplierInfoRepository;
            _configuration = configuration;
        }

        /// <summary>
        /// 获取供应商
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getSupplierInfo")]
        public dynamic getSupplierInfo()
        {
            var SupplierInfo = _Context.SupplierInfo.ToList();
            return DataStatus.DataSuccess(1000, SupplierInfo, "获取供应商成功！");
        }

        /// <summary>
        /// 添加供应商信息
        /// </summary>
        /// <param name="AddBrand">传入前端数据实体</param>
        /// <returns></returns>
        [HttpPost]
        [Route("AddSupplierInfo")]
        public dynamic AddSupplierInfo(AddorUpSupplier AddSupplier)
        {
            var name = AddSupplier.Name;
            var phone = AddSupplier.Phone;
            var linkMan = AddSupplier.LinkMan;

            if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(phone) || string.IsNullOrEmpty(linkMan))
            {
                return DataStatus.DataError(1111, "请检查必填字段是否都填写规范！");
            }

            var IsSupplier = _SupplierInfoRepository.Table.Where(x => x.Name == name).SingleOrDefault();
            if (IsSupplier != null)
            {
                return DataStatus.DataError(1111, "该供应商已存在，请勿重复使用！");
            }

            var newSupplier = new SupplierInfo
            {
                Name = name,
                Phone = phone,
                LinkMan = linkMan
            };

            _SupplierInfoRepository.Insert(newSupplier);
            return DataStatus.DataSuccess(1000, newSupplier, "添加供应商信息成功！");
        }

        /// <summary>
        /// 修改供应商信息
        /// </summary>
        /// <param name="UpSupplier">传入前端数据实体</param>
        /// <returns></returns>
        [HttpPut]
        [Route("UpSupplierInfo/{supplierId}")]
        public dynamic UpSupplierInfo(int supplierId, AddorUpSupplier UpSupplier)
        {
            var isSupplier = _SupplierInfoRepository.GetId(supplierId);
            if (isSupplier == null)
            {
                return DataStatus.DataError(1111, "供应商不存在！");
            }

            var name = UpSupplier.Name;
            var phone = UpSupplier.Phone;
            var linkMan = UpSupplier.LinkMan;

            if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(phone) || string.IsNullOrEmpty(linkMan))
            {
                return DataStatus.DataError(1111, "请检查必填字段是否都填写规范！");
            }

            var Isname = _SupplierInfoRepository.Table.Where(x => x.Name == name && x.Id != supplierId).SingleOrDefault();
            if (Isname != null)
            {
                return DataStatus.DataError(1111, "该供应商已存在，请勿重复使用！");
            }

            isSupplier.Name = name;
            isSupplier.Phone = phone;
            isSupplier.LinkMan = linkMan;


            _SupplierInfoRepository.Update(isSupplier);
            return DataStatus.DataSuccess(1000, isSupplier, "修改供应商信息成功！");
        }

        /// <summary>
        /// 删除供应商
        /// </summary>
        /// <param name="supplierId"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("delsupplierInfo/{supplierId}")]
        public dynamic delsupplierInfo(int supplierId)
        {
            var isSupplier = _SupplierInfoRepository.GetId(supplierId);
            if (isSupplier == null)
            {
                return DataStatus.DataError(1111, "供应商不存在！");
            }

            var isdel = _Context.ProductInfo.Where(x => x.SupplierId == supplierId).ToList();
            if (isdel.Count() != 0)
            {
                return DataStatus.DataError(1111, "供应商使用中无法删除！");
            }

            _SupplierInfoRepository.Delete(supplierId);

            return DataStatus.DataSuccess(1000, supplierId, "供应商删除成功！");
        }

        /// <summary>
        /// 是否启用供应商
        /// </summary>
        /// <param name="supplierId">供应商Id</param>
        /// <returns></returns>
        [HttpPut]
        [Route("supplierisactived/{supplierId}")]
        public dynamic supplierisactived(int supplierId)
        {
            var isSupplier = _SupplierInfoRepository.GetId(supplierId);
            if (isSupplier == null)
            {
                return DataStatus.DataError(1111, "供应商不存在！");
            }

            if (isSupplier.IsActived)
            {
                isSupplier.IsActived = false;
            }
            else
            {
                isSupplier.IsActived = true;
            }

            _SupplierInfoRepository.Update(isSupplier);

            return DataStatus.DataSuccess(1000, isSupplier, "修改是否启用成功！");
        }

    }
}