namespace Electronic.emporium.Api.Params
{
    /// <summary>
    /// Sku添加修改
    /// </summary>
    public class AddorUpSku
    {

        /// <summary>
        /// 属性属性值json
        /// </summary>
        /// <value></value>
        public string KeyValueJson { get; set; }

        /// <summary>
        /// Sku
        /// </summary>
        /// <value></value>
        public string SKU { get; set; }
        /// <summary>
        /// 价格
        /// </summary>
        /// <value></value>
        public decimal Price { get; set; }

    }
}