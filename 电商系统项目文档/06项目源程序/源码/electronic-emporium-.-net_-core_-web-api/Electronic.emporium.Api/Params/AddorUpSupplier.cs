namespace Electronic.emporium.Api.Params
{
    /// <summary>
    /// 后台添加以及修改供应商使用
    /// </summary>
    public class AddorUpSupplier
    {
        /// <summary>
        /// 名称
        /// </summary>
        /// <value></value>
        public string Name { get; set; }

        /// <summary>
        /// 联系人
        /// </summary>
        /// <value></value>
        public string LinkMan { get; set; }

        /// <summary>
        /// 联系人电话
        /// </summary>
        /// <value></value>
        public string Phone { get; set; }
    }
}