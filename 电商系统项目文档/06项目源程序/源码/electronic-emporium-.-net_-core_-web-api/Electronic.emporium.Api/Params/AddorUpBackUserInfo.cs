namespace Electronic.emporium.Api.Params
{
    /// <summary>
    /// 后台添加以及修改用户使用
    /// </summary>
    public class AddorUpBackUserInfo
    {
        /// <summary>
        /// 用户名
        /// </summary>
        /// <value></value>
        public string Username { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        /// <value></value>
        public string Password { get; set; }

        /// <summary>
        /// 问题Id
        /// </summary>
        /// <value></value>
        public int ProblemId { get; set; }

        /// <summary>
        /// 答案
        /// </summary>
        /// <value></value>
        public string Answer { get; set; }

        /// <summary>
        /// 昵称
        /// </summary>
        /// <value></value>
        public string Nickname { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        /// <value></value>
        public string Gender { get; set; }

    }
}