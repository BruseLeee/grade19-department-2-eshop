namespace Electronic.emporium.Api.Params
{
    /// <summary>
    /// 添加以及修改用户信息使用
    /// </summary>
    public class AddorUpUserInfo
    {
        /// <summary>
        /// 昵称
        /// </summary>
        /// <value></value>
        public string Nickname { get; set; }

        /// <summary>
        /// 用户头像
        /// </summary>
        /// <value></value>
        public string IconUrl { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        /// <value></value>
        public string Gender { get; set; }

    }
}