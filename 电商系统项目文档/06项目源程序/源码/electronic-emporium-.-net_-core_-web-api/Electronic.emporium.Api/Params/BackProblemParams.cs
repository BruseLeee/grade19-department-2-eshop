namespace Electronic.emporium.Api.Params
{
    /// <summary>
    /// 密保问题使用
    /// </summary>
    public class BackProblemParams
    {
        /// <summary>
        /// 问题
        /// </summary>
        /// <value></value>
        public string Name{get;set;}
    }
}