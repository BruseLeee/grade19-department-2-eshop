namespace Electronic.emporium.Api.Params
{
    /// <summary>
    /// 刷新tokem
    /// </summary>
    public class RefreshTokenDTO
    {
        public string Token { get; set; } 
        public string refreshToken { get; set; }
    }
}