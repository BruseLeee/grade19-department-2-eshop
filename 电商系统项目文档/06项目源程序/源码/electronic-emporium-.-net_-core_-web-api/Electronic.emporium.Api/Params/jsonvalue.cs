using Newtonsoft.Json;

namespace Electronic.emporium.Api.Params
{
    /// <summary>
    /// json转换
    /// </summary>
    [JsonObject(IsReference = true)]

    public class jsonvalue
    {
        public int Key { get; set; }
        public int value { get; set; }
    }
}