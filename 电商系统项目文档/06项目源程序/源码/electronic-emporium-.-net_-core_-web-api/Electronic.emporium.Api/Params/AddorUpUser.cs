namespace Electronic.emporium.Api.Params
{
    /// <summary>
    /// 添加以及修改用户使用
    /// </summary>
    public class AddorUpUser
    {
        /// <summary>
        /// 用户名
        /// </summary>
        /// <value></value>
        public string Username { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        /// <value></value>
        public string Password { get; set; }

        /// <summary>
        /// 确认密码
        /// </summary>
        /// <value></value>
        public string rePassword { get; set; }

        /// <summary>
        /// 问题Id
        /// </summary>
        /// <value></value>
        public int ProblemId { get; set; }

        /// <summary>
        /// 答案
        /// </summary>
        /// <value></value>
        public string Answer { get; set; }

    }
}