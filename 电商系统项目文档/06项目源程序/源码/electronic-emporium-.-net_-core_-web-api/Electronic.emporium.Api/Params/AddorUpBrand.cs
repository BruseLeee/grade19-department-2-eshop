namespace Electronic.emporium.Api.Params
{
    /// <summary>
    /// 后台添加以及修改品牌使用
    /// </summary>
    public class AddorUpBrand
    {
        /// <summary>
        /// 名称
        /// </summary>
        /// <value></value>
        public string Name{get;set;}

        /// <summary>
        /// 描述
        /// </summary>
        /// <value></value>
        public string Desc{get;set;}

        /// <summary>
        /// 是否推荐
        /// </summary>
        /// <value></value>
        public bool IsRecommend {get;set;}
    }
}