namespace Electronic.emporium.Api.Params
{
    /// <summary>
    /// 管理员用户实体
    /// </summary>
    public class AdminUserParams
    {
        /// <summary>
        /// 用户名
        /// </summary>
        /// <value></value>
        public string Username{get;set;}

        /// <summary>
        /// 密码
        /// </summary>
        /// <value></value>
        public string Password {get;set;}

        /// <summary>
        /// 昵称
        /// </summary>
        /// <value></value>
        public string Nickname {get;set;}

        /// <summary>
        /// 手机号
        /// </summary>
        /// <value></value>
        public string Phone {get;set;}

        /// <summary>
        /// 性别
        /// </summary>
        /// <value></value>
        public string Gender{get;set;}
    }
}