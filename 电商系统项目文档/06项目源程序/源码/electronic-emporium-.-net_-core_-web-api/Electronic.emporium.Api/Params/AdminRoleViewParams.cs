namespace Electronic.emporium.Api.Params
{
    public class AdminRoleViewParams 
    {
        public int Id{get;set;}
        public int AdminId{get;set;} 

        public int RoleId{get;set;}
        public string RoleName{get;set;}

        public string AdminName {get;set;}
    }
}