namespace Electronic.emporium.Api.Params
{
    /// <summary>
    /// 管理员权限实体
    /// </summary>
    public class AdminRoleParams
    {
        /// <summary>
        /// 管理员名称
        /// </summary>
        /// <value></value>
        public string AdminName { get; set; }
        /// <summary>
        /// 权限名称
        /// </summary>
        /// <value></value>
        public string RoleName { get; set; }
    }
}