namespace Electronic.emporium.Api.Params
{
    /// <summary>
    /// 购物车表
    /// </summary>
    public class AddorUpOrderCart
    {
        /// <summary>
        /// 商品Sku
        /// </summary>
        /// <value></value>
        public string Sku {get;set;}

        /// <summary>
        /// 数量
        /// </summary>
        /// <value></value>
        public int Number{get;set;}

    }
}