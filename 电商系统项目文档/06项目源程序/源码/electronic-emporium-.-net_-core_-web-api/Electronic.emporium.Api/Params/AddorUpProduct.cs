namespace Electronic.emporium.Api.Params
{
    /// <summary>
    /// 后台添加修改商品表
    /// </summary>
    public class AddorUpProduct
    {
        /// <summary>
        /// 父类Id
        /// </summary>
        /// <value></value>
        public int ParentId {get;set;}

        /// <summary>
        /// 子类Id
        /// </summary>
        /// <value></value>
        public int SubId {get;set;}

        /// <summary>
        /// 名称
        /// </summary>
        /// <value></value>
        public string Name {get;set;}

        /// <summary>
        /// 品牌Id
        /// </summary>
        /// <value></value>
        public int BrandId {get;set;}

        /// <summary>
        /// 供应商Id
        /// </summary>
        /// <value></value>
        public  int SupplierId {get;set;}

        /// <summary>
        /// 商品描述
        /// </summary>
        /// <value></value>
        public string Descript {get;set;}

        /// <summary>
        /// 供应价格
        /// </summary>
        /// <value></value>
        public decimal CostPrice {get;set;}

        /// <summary>
        /// 单位
        /// </summary>
        /// <value></value>
        public string Unit {get;set;}

        /// <summary>
        /// 是否推荐
        /// </summary>
        /// <value></value>
        public bool IsRecommend {get;set;}

        /// <summary>
        /// 是否新品
        /// </summary>
        /// <value></value>
        public bool IsNew {get;set;}
    }
    
}