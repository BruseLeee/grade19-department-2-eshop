namespace Electronic.emporium.Api.Params
{
    /// <summary>
    /// 后台添加以及修改类型使用
    /// </summary>
    public class AddorUpType
    {
        /// <summary>
        /// 名称
        /// </summary>
        /// <value></value>
        public string Name { get; set; }

    }
}