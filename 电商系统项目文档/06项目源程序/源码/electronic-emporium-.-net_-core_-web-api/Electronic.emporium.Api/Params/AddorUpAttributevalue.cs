namespace Electronic.emporium.Api.Params
{
    /// <summary>
    /// 后台添加以及修改属值性使用
    /// </summary>
    public class AddorUpAttributevalue
    {
        /// <summary>
        /// 名称
        /// </summary>
        /// <value></value>
        public string Name { get; set; }

    }
}