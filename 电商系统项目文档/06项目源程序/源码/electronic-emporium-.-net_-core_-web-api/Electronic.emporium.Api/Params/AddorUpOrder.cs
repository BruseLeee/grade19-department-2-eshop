namespace Electronic.emporium.Api.Params
{
    /// <summary>
    /// 订单表
    /// </summary>
    public class AddorUpOrder
    {
        /// <summary>
        /// 地址Id
        /// </summary>
        /// <value></value>
        public int ReceiveAddressId { get; set; }
        /// <summary>
        /// 订单金额
        /// </summary>
        /// <value></value>
        public decimal OrderMoney { get; set; }
    }
}