using Microsoft.EntityFrameworkCore; //引入EF
using Electronic.emporium.Api.Entity; //引入数据表类
using System;

namespace Electronic.emporium.Api.Database
{
    public class ElectronicDb : DbContext
    {
        public ElectronicDb()
        {

        }
        public ElectronicDb(DbContextOptions options) : base(options)
        {

        }

        //用户表
        public DbSet<User> User{get;set;}
        //问题表
        public DbSet<Problem> Problem{get;set;}
        //用户信息
        public DbSet<UserInfo> UserInfo{get;set;}
        //收货地址
        public DbSet<ReceiveAddress> ReceiveAddress {get;set;}
        //管理员用户
        public DbSet<AdminUser> AdminUser{get;set;}
        //余额
        public DbSet<BalanceLog> BalanceLog {get;set;}
        //用户角色表
        public DbSet<AdminRole> AdminRole {get;set;}
        //角色表
        public DbSet<Role> Role{get;set;}
        //供应商信息表表
        public DbSet<SupplierInfo> SupplierInfo {get;set;}
        //子类表
        public DbSet<SubCategory> SubCateGory {get;set;}
        //品牌信息表
        public DbSet<BrandInfo> BrandInfo {get;set;}
        //父类表
        public DbSet<ParentCategory> ParentCategory {get;set;}
        //商品表
        public DbSet<ProductInfo> ProductInfo {get;set;}
        //类型表
        public DbSet<Electronic.emporium.Api.Entity.Type> Type {get;set;}
        //属性表
        public DbSet<Electronic.emporium.Api.Entity.Attribute> Attribute {get;set;}
        //属性值表
        public DbSet<AttributeValue> AttributeValue {get;set;}
        //Sku表
        public DbSet<Sku> Skus{get;set;}
        //商品评价表
        public DbSet<Appraise> Appraise{get;set;}
        //商家回复评价表
        public DbSet<MerchantReplayAppraise> MerchantReplayAppraise{get;set;}
        //库存信息表
        public DbSet<InventoryInfo> InventoryInfo{get;set;}
        //订单表
        public DbSet<Order> Order{get;set;}
        //订单详情表
        public DbSet<OrderInfo> OrderInfo {get;set;}
        //购物车表
        public DbSet<OrderCart> OrderCart{get;set;}

        protected override void OnConfiguring(DbContextOptionsBuilder options)  //重写这个方法并且连上我们的数据库
        {
            // options.UseNpgsql(@"host=47.112.101.212;database=ElectronicDb;uid=postgres;pwd=cultural0725.");
            options.UseNpgsql(@"host=47.112.101.212;database=ElectronicDb;uid=postgres;pwd=cultural0725.");
            // options.UseSqlServer(@"server=.;database=ElectronicDb;uid=sa;pwd=123456");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder){
            modelBuilder.Entity<Problem>().HasData(
                new Problem()
                {
                    Id = 1,
                    Name = "你最喜欢的东西",
                    IsActived = true,
                    IsDeleted = false,
                    CreatedTime = DateTime.Now,
                    UpdatedTime=DateTime.Now
                },
                new Problem()
                {
                    Id=2,
                    Name = "你最讨厌的东西",
                    IsActived = true,
                    IsDeleted = false,
                    CreatedTime = DateTime.Now,
                    UpdatedTime=DateTime.Now
                }
            );
            modelBuilder.Entity<AdminUser>().HasData(
                new AdminUser()
                {
                    Id = 1,
                    Username = "admin",
                    Password="113",
                    NickName = "nike",
                    Phone="11223555",
                    Gender = "男"
                }                
            );
        }
        

    }
}