using Newtonsoft.Json;

namespace Electronic.emporium.Api.Entity
{
    /// <summary>
    /// 收货地址
    /// </summary>
    [JsonObject(IsReference = true)]
    public class ReceiveAddress : BaseEntity
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        /// <value></value>
        public int UserId { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        /// <value></value>
        public string Name { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        /// <value></value>
        public string Phone { get; set; }

        /// <summary>
        /// 省份
        /// </summary>
        /// <value></value>
        public string Province { get; set; }

        /// <summary>
        /// 城市
        /// </summary>
        /// <value></value>
        public string City { get; set; }

        /// <summary>
        /// 区
        /// </summary>
        /// <value></value>
        public string Region { get; set; }

        /// <summary>
        /// 详细地址
        /// </summary>
        /// <value></value>
        public string DetailAddress { get; set; }

    }
}