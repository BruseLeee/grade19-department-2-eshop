namespace Electronic.emporium.Api.Entity
{
    /// <summary>
    /// 类型表
    /// </summary>
    public class Type : BaseEntity
    {
        /// <summary>
        /// 名称
        /// </summary>
        /// <value></value>
        public string Name { get; set; }
    }
}


