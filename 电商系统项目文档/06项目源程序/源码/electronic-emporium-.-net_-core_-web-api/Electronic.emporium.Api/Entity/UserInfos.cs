
namespace Electronic.emporium.Api.Entity
{
    /// <summary>
    /// 用户信息表
    /// </summary>
    public class UserInfo : BaseEntity
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        /// <value></value>
        public int UserId { get; set; }

        /// <summary>
        /// 昵称
        /// </summary>
        /// <value></value>
        public string Nickname { get; set; }

        /// <summary>
        /// 用户头像
        /// </summary>
        /// <value></value>
        public string IconUrl { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        /// <value></value>
        public string Gender { get; set; }

        /// <summary>
        /// 余额
        /// </summary>
        /// <value></value>
        public decimal Money { get; set; }

    }
}