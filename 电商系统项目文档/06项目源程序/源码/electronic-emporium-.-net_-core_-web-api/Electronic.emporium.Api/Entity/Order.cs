using System;
namespace Electronic.emporium.Api.Entity
{
    /// <summary>
    /// 订单表
    /// </summary>
    public class Order : BaseEntity
    {
        /// <summary>
        /// 用户id
        /// </summary>
        /// <value></value>
        public int UserId { get; set; }
        /// <summary>
        /// 地址Id
        /// </summary>
        /// <value></value>
        public int ReceiveAddressId { get; set; }
        /// <summary>
        /// 订单金额
        /// </summary>
        /// <value></value>
        public decimal OrderMoney { get; set; }
    }
}