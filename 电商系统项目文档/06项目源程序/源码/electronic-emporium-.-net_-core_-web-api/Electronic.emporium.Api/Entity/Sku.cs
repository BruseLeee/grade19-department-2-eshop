namespace Electronic.emporium.Api.Entity
{
    /// <summary>
    /// Sku表
    /// </summary>
    public class Sku : BaseEntity
    {
        /// <summary>
        /// 商品Id
        /// </summary>
        /// <value></value>
        public int ProductId { get; set; }
        /// <summary>
        /// Sku
        /// </summary>
        /// <value></value>
        public string SKU { get; set; }
        /// <summary>
        /// 属性属性值json
        /// </summary>
        /// <value></value>
        public string KeyValueJson { get; set; }
        /// <summary>
        /// 价格
        /// </summary>
        /// <value></value>
        public decimal Price {get;set;}

    }
}