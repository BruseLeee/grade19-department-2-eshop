
namespace Electronic.emporium.Api.Entity
{
    /// <summary>
    /// 管理员用户表
    /// </summary>
    public class AdminUser : BaseEntity
    {   
        /// <summary>
        /// 用户名
        /// </summary>
        /// <value></value>
        public string Username {get;set;}

        /// <summary>
        /// 密码
        /// </summary>
        /// <value></value>
        public string Password{get;set;}

        /// <summary>
        /// 昵称
        /// </summary>
        /// <value></value>
        public string NickName{get;set;}

        /// <summary>
        /// 手机号
        /// </summary>
        /// <value></value>
        public string Phone{get;set;}

        /// <summary>
        /// 性别
        /// </summary>
        /// <value></value>
        public string Gender{get;set;}

    }
}