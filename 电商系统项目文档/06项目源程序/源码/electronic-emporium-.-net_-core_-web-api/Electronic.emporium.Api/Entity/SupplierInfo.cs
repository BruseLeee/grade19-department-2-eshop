namespace Electronic.emporium.Api.Entity
{
    /// <summary>
    /// 供应商信息表
    /// </summary>
    public class SupplierInfo : BaseEntity
    {
        /// <summary>
        /// 名称
        /// </summary>
        /// <value></value>
        public string Name {get;set;}


        /// <summary>
        /// 联系人
        /// </summary>
        /// <value></value>
        public string LinkMan {get;set;}

        /// <summary>
        /// 联系人电话
        /// </summary>
        /// <value></value>
        public string Phone {get;set;}
    }
}