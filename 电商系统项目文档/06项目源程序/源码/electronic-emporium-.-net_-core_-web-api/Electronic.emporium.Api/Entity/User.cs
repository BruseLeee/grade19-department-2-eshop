
namespace Electronic.emporium.Api.Entity
{
    /// <summary>
    /// 用户表
    /// </summary>
    public class User : BaseEntity
    {
        /// <summary>
        /// 用户名
        /// </summary>
        /// <value></value>
        public string Username {get;set;}

        /// <summary>
        /// 密码
        /// </summary>
        /// <value></value>
        public string Password {get;set;}

        /// <summary>
        /// 问题Id
        /// </summary>
        /// <value></value>
        public int ProblemId {get;set;}

        /// <summary>
        /// 答案
        /// </summary>
        /// <value></value>
        public string Answer {get;set;}

    }
}