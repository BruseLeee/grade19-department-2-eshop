namespace Electronic.emporium.Api.Entity
{
    /// <summary>
    /// 商家回复评论表
    /// </summary>
    public class MerchantReplayAppraise : BaseEntity
    {
        /// <summary>
        /// 评价id
        /// </summary>
        /// <value></value>
        public int AppraiseId { get; set; }
        /// <summary>
        /// 内容
        /// </summary>
        /// <value></value>
        public string Content { get; set; }
        /// <summary>
        /// 评价
        /// </summary>
        /// <value></value>
        public int AppraiseValue { get; set; }

    }
}