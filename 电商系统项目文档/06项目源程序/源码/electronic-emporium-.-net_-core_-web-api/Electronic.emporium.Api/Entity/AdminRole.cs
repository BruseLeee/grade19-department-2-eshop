

namespace Electronic.emporium.Api.Entity
{
    /// <summary>
    /// 管理员角色表
    /// </summary>
    public class AdminRole : BaseEntity
    {
        /// <summary>
        /// 角色id
        /// </summary>
        /// <value></value>
        public int RoleId {get;set;}

        /// <summary>
        /// 管理员d
        /// </summary>
        /// <value></value>
        public int AdminId {get;set;}
    }
}