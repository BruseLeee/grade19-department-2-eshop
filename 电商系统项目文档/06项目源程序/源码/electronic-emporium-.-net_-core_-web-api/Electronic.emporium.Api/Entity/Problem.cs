namespace Electronic.emporium.Api.Entity
{
    /// <summary>
    /// 问题表
    /// </summary>
    public class Problem : BaseEntity
    {
        /// <summary>
        /// 问题名称
        /// </summary>
        /// <value></value>
        public string Name{get;set;}

    }
}