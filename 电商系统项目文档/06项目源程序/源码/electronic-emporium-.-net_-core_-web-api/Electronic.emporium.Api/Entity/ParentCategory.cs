using Newtonsoft.Json;

namespace Electronic.emporium.Api.Entity
{
    /// <summary>
    /// 父类表
    /// </summary>
    
    [JsonObject(IsReference = true)]
    public class ParentCategory : BaseEntity
    {
        /// <summary>
        /// 名称
        /// </summary>
        /// <value></value>
        public string Name{get;set;}

    }
}