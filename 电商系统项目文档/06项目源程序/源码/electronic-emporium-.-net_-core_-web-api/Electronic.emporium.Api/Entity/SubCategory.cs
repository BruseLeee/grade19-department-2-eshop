using Newtonsoft.Json;

namespace Electronic.emporium.Api.Entity
{
    /// <summary>
    /// 子类表
    /// </summary>
    [JsonObject(IsReference = true)]
    public class SubCategory : BaseEntity
    {
        /// <summary>
        /// 父类Id
        /// </summary>
        /// <value></value>
        public int ParentId { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        /// <value></value>
        public string Name { get; set; }

    }
}