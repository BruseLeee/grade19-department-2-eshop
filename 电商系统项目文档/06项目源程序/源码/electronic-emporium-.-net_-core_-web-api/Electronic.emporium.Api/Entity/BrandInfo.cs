
namespace Electronic.emporium.Api.Entity
{
    /// <summary>
    /// 品牌信息表
    /// </summary>
    public class BrandInfo : BaseEntity
    {
        /// <summary>
        /// 名称
        /// </summary>
        /// <value></value>
        public string Name{get;set;}

        /// <summary>
        /// Logo
        /// </summary>
        /// <value></value>
        public string LogoUrl {get;set;}

        /// <summary>
        /// 描述
        /// </summary>
        /// <value></value>
        public string Desc{get;set;}

        /// <summary>
        /// 是否推荐
        /// </summary>
        /// <value></value>
        public bool IsRecommend {get;set;}

    }
}