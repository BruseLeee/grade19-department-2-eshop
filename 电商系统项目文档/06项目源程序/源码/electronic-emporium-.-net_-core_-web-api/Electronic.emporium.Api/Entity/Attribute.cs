namespace Electronic.emporium.Api.Entity
{
    /// <summary>
    /// 属性表
    /// </summary>
    public class Attribute : BaseEntity
    {
        /// <summary>
        /// 类型Id
        /// </summary>
        /// <value></value>
        public int TypeId{get;set;}
        /// <summary>
        /// 名称
        /// </summary>
        /// <value></value>
        public string Name{get;set;}

    }
}