namespace Electronic.emporium.Api.Entity
{
    /// <summary>
    /// 库存信息表
    /// </summary>
    public class InventoryInfo : BaseEntity
    {
        /// <summary>
        /// Sku
        /// </summary>
        /// <value></value>
        public int Sku { get; set; }
        /// <summary>
        /// 销售价
        /// </summary>
        /// <value></value>
        public decimal SellPrice { get; set; }
        /// <summary>
        /// 库存量
        /// </summary>
        /// <value></value>
        public int InventoryCount { get; set; }
    }
}