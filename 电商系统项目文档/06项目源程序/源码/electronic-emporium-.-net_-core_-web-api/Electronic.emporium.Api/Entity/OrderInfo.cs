namespace Electronic.emporium.Api.Entity
{
    /// <summary>
    /// 订单详情表
    /// </summary>
    public class OrderInfo : BaseEntity
    {
        /// <summary>
        /// 订单id
        /// </summary>
        /// <value></value>
        public int OrderId { get; set; }
        /// <summary>
        /// Sku
        /// </summary>
        /// <value></value>
        public string Sku { get; set; }
        /// <summary>
        /// 数量
        /// </summary>
        /// <value></value>
        public int Number { get; set; }
    }
}