
namespace Electronic.emporium.Api.Entity
{
    /// <summary>
    /// 角色表
    /// </summary>
    public class Role : BaseEntity
    {
        /// <summary>
        /// 名称
        /// </summary>
        /// <value></value>
        public string Name {get;set;}

        /// <summary>
        /// 描述
        /// </summary>
        /// <value></value>
        public string Decription{get;set;}
    }
}