namespace Electronic.emporium.Api.Entity
{
    /// <summary>
    /// 评价表
    /// </summary>
    public class Appraise : BaseEntity
    {
        /// <summary>
        /// 商品id
        /// </summary>
        /// <value></value>
        public int ProductId {get;set;}
        /// <summary>
        /// 订单Id
        /// </summary>
        /// <value></value>
        public int OrderId{get;set;}
        /// <summary>
        /// 内容
        /// </summary>
        /// <value></value>
        public string Content{get;set;}
        /// <summary>
        /// 评价
        /// </summary>
        /// <value></value>
        public int AppraiseScore{get;set;}

    }
}