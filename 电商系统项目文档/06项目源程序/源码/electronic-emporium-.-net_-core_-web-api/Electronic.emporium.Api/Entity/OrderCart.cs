using System;

namespace Electronic.emporium.Api.Entity
{
    /// <summary>
    /// 购物车表
    /// </summary>
    public class OrderCart : BaseEntity
    {
        /// <summary>
        /// 用户id
        /// </summary>
        /// <value></value>
        public int UserId {get;set;}

        /// <summary>
        /// 商品Sku
        /// </summary>
        /// <value></value>
        public string Sku {get;set;}

        /// <summary>
        /// 数量
        /// </summary>
        /// <value></value>
        public int Number{get;set;}

    }
}