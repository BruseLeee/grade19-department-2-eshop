namespace Electronic.emporium.Api.Entity
{
    /// <summary>
    /// 属性值表
    /// </summary>
    public class AttributeValue : BaseEntity
    {
        /// <summary>
        /// 属性Id
        /// </summary>
        /// <value></value>
        public int AttributeId{get;set;}
        /// <summary>
        /// 名称
        /// </summary>
        /// <value></value>
        public string Name {get;set;}
    }
}