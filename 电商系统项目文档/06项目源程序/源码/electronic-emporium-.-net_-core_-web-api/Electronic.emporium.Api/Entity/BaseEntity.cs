using System;
namespace Electronic.emporium.Api.Entity
{
    /// <summary>
    /// 基类表
    /// </summary>
    public abstract class BaseEntity
    {
        /// <summary>
        /// Id
        /// </summary>
        /// <value></value>
        public int Id { get; set; }

        /// <summary>
        /// 是否启用
        /// </summary>
        /// <value></value>
        public bool IsActived { get; set; }

        /// <summary>
        /// 是否删除
        /// </summary>
        /// <value></value>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        /// <value></value>
        public DateTime CreatedTime { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        /// <value></value>
        public DateTime UpdatedTime { get; set; }


    }
}