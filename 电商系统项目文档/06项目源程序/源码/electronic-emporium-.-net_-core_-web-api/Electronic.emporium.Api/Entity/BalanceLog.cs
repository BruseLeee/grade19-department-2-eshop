

namespace Electronic.emporium.Api.Entity
{
    /// <summary>
    /// 余额
    /// </summary>
    public class BalanceLog : BaseEntity
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        /// <value></value>
        public int UserId { get; set; }

        /// <summary>
        /// 订单Id
        /// </summary>
        /// <value></value>
        public int OrderId { get; set; }

        /// <summary>
        /// 记录来源
        /// </summary>
        /// <value></value>
        public int Source { get; set; }

        /// <summary>
        /// 变动金额
        /// </summary>
        /// <value></value>
        public decimal Amount { get; set; }

    }
}