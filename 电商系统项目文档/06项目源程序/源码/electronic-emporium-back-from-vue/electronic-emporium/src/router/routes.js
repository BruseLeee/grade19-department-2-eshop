import Layout from '../components/Layout'
// 用户列表在项目运行时进行加载,提高加载速度的同时也会使项目首次加载时间延长
import userList from '../components/userController/userList'

let routes = [
    {
        path: '/',
        component: Layout, // 异步加载 (懒加载)
        redirect: { path: '/dashboard' },
        meta: {
            title: "根目录",
            icon: 'bx bx-notification',
            style: 'color:#ffffff',
            // hidden: true,
        },
        children: [
            {
                path: 'dashboard',
                name: 'dashboard',
                meta: {
                    title: "仪表盘",
                    icon: 'bx bxs-dashboard',
                    style: 'color:#ffffff'
                },
                component: () => import('../components/Dashboard/dashboard')
            },
            {
                path: 'home',
                name: 'home',
                meta: {
                    title: "首页",
                    icon: 'bx bx-window',
                    style: 'color:#ffffff',
                    hidden: true
                },
                component: () => import('../components/views/home'),
                beforeEnter(to, from, next) {
                    // console.log("首页路由独享的守卫：beforeEnter");
                    next();
                }
            },



            // {
            //     path: 'log',
            //     name: 'log',
            //     meta: {
            //         title: "日志中心",
            //         icon: 'bx bxl-blogger',
            //         style: 'color:#ffffff'
            //     },
            //     component: () => import('../components/sidebar/log')
            // },

        ],
    },

    {
        path: '/productController',
        name: 'productController',
        component: Layout, // 异步加载 (懒加载)
        meta: {
            title: "商品管理",
            icon: 'bx bxs-spreadsheet',
            style: 'color:#ffffff'
        },
        children: [
            {
                path: 'productList',
                name: 'productList',
                meta: {
                    title: "商品列表",
                    icon: 'bx bx-list-minus',
                    style: 'color:#ffffff'
                },
                component: () => import('../components/productController/productList'),
                beforeEnter(to, from, next) {
                    // console.log("用户列表路由独享的守卫：beforeEnter");
                    next();
                }
            },
            {
                path: 'addProduct',
                name: 'addProduct',
                meta: {
                    title: "添加商品",
                    icon: 'bx bxs-cart-add',
                    style: 'color:#ffffff'
                },
                component: () => import('../components/productController/addproduct'),
                beforeEnter(to, from, next) {
                    // console.log("用户列表路由独享的守卫：beforeEnter");
                    next();
                }
            },
            {
                path: 'productCategory',
                name: 'productCategory',
                meta: {
                    title: "商品类型",
                    icon: 'bx bx-certification',
                    style: 'color:#ffffff'
                },
                component: () => import('../components/productController/productCategory'),
            },
            {
                path: 'productCate',
                name: 'productCate',
                meta: {
                    title: "商品分类",
                    icon: 'bx bx-category-alt',
                    style: 'color:#ffffff'
                },
                component: () => import('../components/productController/productCate'),
                beforeEnter(to, from, next) {
                    // console.log("用户列表路由独享的守卫：beforeEnter");
                    next();
                }
            }
        ]
    },
    {
        path: '/orderController',
        name: 'orderController',
        component: Layout,
        meta: {
            title: "订单管理",
            icon: 'bx bx-list-ol',
            style: 'color:#ffffff'
        },
        children: [
            {
                path: 'orderList',
                name: 'orderList',
                meta: {
                    title: "订单列表",
                    icon: 'bx bxs-hand-down',
                    style: 'color:#ffffff'
                },
                component: () => import('../components/order/orderList'),
                beforeEnter(to, from, next) {
                    // console.log("品牌列表路由独享的守卫：beforeEnter");
                    next();
                }

            }
        ]
    },
    {
        path: '/brandList',
        name: '/brandList',
        component: Layout, // 异步加载 (懒加载)
        meta: {
            title: "品牌管理",
            icon: 'bx bx-happy-beaming',
            style: 'color:#ffffff'
        },
        children: [
            {
                path: 'brand',
                name: 'brand',
                meta: {
                    title: "品牌列表",
                    icon: 'bx bx-polygon',
                    style: 'color:#ffffff'
                },
                component: () => import('../components/brand/brandList'),
                beforeEnter(to, from, next) {
                    // console.log("品牌列表路由独享的守卫：beforeEnter");
                    next();
                }
            },

        ]
    },
    {
        path: '/provider',
        name: 'provider',
        title: "供应商管理",
        component: Layout,
        meta: {
            title: "供应商管理",
            icon: 'bx bx-sun',
            style: 'color:#ffffff'
        },
        children: [
            {
                path: 'providerList',
                name: 'providerList',
                meta: {
                    title: "供应商列表",
                    icon: 'bx bxl-tux',
                    style: 'color:#ffffff'
                },
                component: () => import("../components/provider/providerList"),
                beforeEnter(to, from, next) {
                    // console.log("供应商列表路由独享的守卫：beforeEnter");
                    next();
                }
            }
        ]
    },
    {
        path: '/userController',
        name: 'userController',
        component: Layout, // 异步加载 (懒加载)
        meta: {
            title: "用户管理",
            icon: 'bx bx-user',
            style: 'color:#ffffff'
        },
        children: [
            {
                path: 'userList',
                name: 'userList',
                meta: {
                    title: "用户列表",
                    icon: 'bx bxs-user-detail',
                    style: 'color:#ffffff'
                },
                component: userList,
                beforeEnter(to, from, next) {
                    // console.log("用户列表路由独享的守卫：beforeEnter");
                    next();
                }
            },
            {
                path: 'problemList',
                name: 'problemList',
                meta: {
                    title: "问题列表",
                    icon: 'bx bx-message-alt-error',
                    style: 'color:#ffffff'
                },
                component: () => import("../components/userController/problemList"),
                beforeEnter(to, from, next) {
                    // console.log("用户列表路由独享的守卫：beforeEnter");
                    next();
                }
            },
            {
                path: 'roleList',
                name: 'roleList',
                meta: {
                    title: "角色列表",
                    icon: 'bx bxl-ok-ru',
                    style: 'color:#ffffff'
                },
                component: () => import("../components/userController/roleList"),
                beforeEnter(to, from, next) {
                    // console.log("用户列表路由独享的守卫：beforeEnter");
                    next();
                }
            },
            {
                path: 'adminList',
                name: 'adminList',
                meta: {
                    title: "管理员列表",
                    icon: 'bx bx-user-circle',
                    style: 'color:#ffffff'
                },
                component: () => import("../components/userController/adminList"),
                beforeEnter(to, from, next) {
                    // console.log("用户列表路由独享的守卫：beforeEnter");
                    next();
                }
            },
            {
                path: 'adminRoleList',
                name: 'adminRoleList',
                meta: {
                    title: "管理员权限列表",
                    icon: 'bx bxl-digitalocean',
                    style: 'color:#ffffff'
                },
                component: () => import("../components/userController/adminRoleList"),
                beforeEnter(to, from, next) {
                    // console.log("用户列表路由独享的守卫：beforeEnter");
                    next();
                }
            },
        ]
    },
    {
        path:'/login',
        name:'login',
        meta:{
            title:"登录",
            icon:'bx bx-photo-album',
            style:'color:#ffffff',
            hidden: true
        },
        component:()=>import("../components/login/loginmainpage")
    }
    // {
    // path: '/Photo',
    //     component: Layout,
    //     meta: {
    //         title: "图片管理",
    //         icon: 'bx bx-photo-album',
    //         style: 'color:#ffffff'
    //     },
    //     children: [
    //         //{
    //         //     path: 'PhotoUser',
    //         //     name: 'PhotoUser',
    //         //     meta: {
    //         //         title: "文章图片管理",
    //         //         icon: 'bx bxl-patreon',
    //         //         style: 'color:#ffffff'
    //         //     },
    //         //     component: () => import('../components/views/photoUser')
    //         // },
    //         {
    //             path: 'carousel',
    //             name: 'carousel',
    //             meta: {
    //                 title: "轮播图管理",
    //                 icon: 'bx bx-slideshow',
    //                 style: 'color:#ffffff'
    //             },
    //             component: () => import('../components/views/carousel')
    //         },
    //     ]
    // },
    // {
    //     path: '/Article',
    //     component: Layout,
    //     meta: {
    //         title: "文章管理",
    //         icon: 'bx bx-chart',
    //         style: 'color:#ffffff'

    //     },
    //     children: [{
    //         path: 'addArticle',
    //         name: 'addArticle',
    //         meta: {
    //             title: "文章发布",
    //             icon: 'bx bx-slideshow',
    //             style: 'color:#ffffff',
    //             hidden:true
    //         },
    //         component: () => import('../components/views/addArticle')
    //     },
    //     {
    //         path: 'articleList',
    //         name: 'articleList',
    //         meta: {
    //             title: "文章列表",
    //             icon: 'bx bx-slideshow',
    //             style: 'color:#ffffff'
    //         },

    //         component: () => import('../components/views/articleList')
    //     },
    //     {
    //         path: 'articleContent',
    //         name: 'articleContent',
    //         meta: {
    //             title: "文章编辑",
    //             icon: 'bx bx-slideshow',
    //             style: 'color:#ffffff',
    //             hidden:true
    //         },
    //         component: () => import('../components/views/ArticleContent')
    //     },
    //     ]
    // },
    // {
    //     // path: '/authController',
    //     // component: Layout,
    //     // name: "authController",
    //     // meta: {
    //     //     title: "权限管理",
    //     //     icon: 'bx bxs-user'
    //     // },
    //     // children: [
    //     //     {
    //     path: '/userController',
    //     name: 'userController',
    //     component: Layout,
    //     meta: {
    //         title: "用户管理",
    //         icon: 'bx bx-user-pin',
    //         style: 'color:#ffffff'
    //     },
    //     children: [
    //         {
    //             path: 'authList',
    //             name: 'authController',
    //             meta: {
    //                 title: "权限管理",
    //                 icon: 'bx bx-lock',
    //                 style: 'color:#ffffff'
    //             },
    //             component: () => import('../components/authController/userController/authList')
    //         },
    //         {
    //             path: 'userList',
    //             name: 'userList',
    //             meta: {
    //                 title: "用户列表",
    //                 icon: 'bx bxs-user-detail',
    //                 style: 'color:#ffffff'
    //             },
    //             component: userList,
    //             beforeEnter(to, from, next) {
    //                 console.log("用户列表路由独享的守卫：beforeEnter");
    //                 next();
    //             }
    //         },
    //         {
    //             path: 'problemList',
    //             name: 'problemList',
    //             meta: {
    //                 title: "问题列表",
    //                 icon: 'bx bx-help-circle',
    //                 style: 'color:#ffffff'
    //             },
    //             component: () => import('../components/authController/userController/problemList')
    //         }
    //     ]
    //     // }
    //     // ],
    // },
    // {
    //     path: '/personalCenter',
    //     name: 'personalCenter',
    //     component: Layout,
    //     meta: {
    //         title: "个人中心",
    //         icon: 'bx bx-user',
    //         style: 'color:#ffffff',

    //     },
    //     children: [
    //         {
    //             path: 'profile',
    //             name: 'profile',
    //             meta: {
    //                 title: "个人信息",
    //                 icon: 'bx bx-id-card',
    //                 style: 'color:#ffffff'
    //             },
    //             component: () => import('../components/personalCenter/profile')
    //         },
    //         // {
    //         //     path: 'resetPwd',
    //         //     name: 'resetPwd',
    //         //     meta: {
    //         //         title: "密码更改",
    //         //         icon: 'bx bx-wrench',
    //         //         style: 'color:#ffffff'
    //         //     },
    //         //     component: () => import('../components/personalCenter/resetPwd')
    //         // },
    //     ]
    // },
    // {
    //     path: '/webSystem',
    //     component: Layout,
    //     meta: {
    //         title: "网站管理",
    //         icon: 'bx bx-wrench',
    //         style: 'color:#ffffff'
    //     },
    //     children: [{
    //         path: 'QRCode',
    //         name: 'QRCode',
    //         meta: {
    //             title: "二维码管理",
    //             icon: 'bx bx-scan',
    //             style: 'color:#ffffff'
    //         },
    //         component: () => import('../components/views/QRCode')
    //     },
    //     {
    //         path: 'webInfo',
    //         name: 'webInfo',
    //         meta: {
    //             title: "网站信息",
    //             icon: 'bx bx-like',
    //             style: 'color:#ffffff'
    //         },
    //         component: () => import('../components/views/webInfo')
    //     },
    //     ]
    //     // path: 'QRCode',
    //     // name: 'QRCode',
    //     // meta: {
    //     //     title: "二维码管理",
    //     //     icon: 'bx bx-qr-scan',
    //     //     style: 'color:#ffffff'
    //     // },
    //     // component: () => import('../components/views/QRCode')
    // },
    // {
    //     path: '/Login',
    //     title: "登錄",
    //     meta: {
    //         hidden: true
    //     },
    //     component: () => import('../components/Login/Login')
    // },

    // {
    //     path: '/Register',
    //     title: "注冊",
    //     component: () => import('../components/Register')
    // },

]


export default routes