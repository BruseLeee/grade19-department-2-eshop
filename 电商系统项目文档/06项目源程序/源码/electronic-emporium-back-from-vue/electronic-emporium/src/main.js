import Vue from 'vue'
import App from './App.vue'
import ElementUI from 'element-ui'
import router from './router'
import axios from 'axios'
import 'element-ui/lib/theme-chalk/index.css';
import { message } from '@/utils/resetMessage';
import Cookies from "js-cookie"
import VueQuillEditor from 'vue-quill-editor'
import UUID from 'vue-uuid'
import 'boxicons'


Vue.use(ElementUI)
Vue.use(VueQuillEditor)
Vue.use(UUID); 


Vue.use(ElementUI, {
  size: Cookies.get('size') || 'medium' // set element-ui default size
})
// Vue.use(ElementUI)
Vue.prototype.$message = message;
Vue.prototype.$axios = axios;

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
