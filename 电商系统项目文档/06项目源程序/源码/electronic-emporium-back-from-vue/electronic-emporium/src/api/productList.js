import request from '../utils/request'

// 获取商品
export function getProduct(params){
    return request.get("/BackProductInfo/getProduct",{params:params})
}

// 删除商品
export function deleteProduct(id){
    return request.delete(`/BackProductInfo/delProduct/${id}`)
}

// 根据商品id获取sku
export function getSku(id){
    return request.get(`/sku/getsku/${id}`)
}
// 删除sku
export function deleteSku(id){
    return request.delete(`sku/delsku/${id}`)
}