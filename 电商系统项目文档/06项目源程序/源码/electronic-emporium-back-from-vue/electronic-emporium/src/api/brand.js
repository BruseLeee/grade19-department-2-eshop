import request from '../utils/request'

// 获取所有品牌信息
export function getBrand(){
    return request.get("/BackBrandInfo/getBrandInfo")
}

// 添加品牌
export function addBrand(data){
    return request.post("/BackBrandInfo/AddBrand",data)
}

// 修改品牌信息
export function editBrand(id,data){
    return request.put(`/BackBrandInfo/UpBrand/${id}`,data)
}

// 删除品牌信息
export function delBrand(id){
    return request.delete(`/BackBrandInfo/delBrand/${id}`)
}