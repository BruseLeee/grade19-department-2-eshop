import request from '../utils/request'

// 获取用户列表
export function getUserInfo(params) {
    return request.get("/backuser/getusersinfoView",{params:params})
}

// 获取个人信息
export function getPersonalInfo(id) {
    return request.get(`/backuser/getusersinfo/${id}`)
}

// 获取问题
export function GetProblem() {
    return request.get(`/General/problem`)
}

// 添加用户
export function AddUser(data) {
    return request.post('/backuser/adduserInfo', data)
}

// 删除用户
export function isDeleted(id) {
    return request.put(`/backuser/isdeleted/${id}`)
}

// 更新用户信息
export function updateUserInfo(id, data) {
    return request.put(`backuser/upusersinfo/${id}`, data)
}

// 是否启用用户
export function isActived(id) {
    return request.put(`/backuser/isactived/${id}`)
}

//添加问题
export function addProblem(data) {
    return request.post('/backproblem/addproblem', data)
}

//修改问题
export function upProblem(data, id) {
    return request.put(`/backproblem/upproblem/${id}`, data)
}

//删除问题
export function deleteProblem(id) {
    return request.delete(`/backproblem/deleteproblem/${id}`)
}

//获取指定问题
export function getUpProblem(id) {
    return request.get(`/backproblem/getupproblem/${id}`)
}

//获取角色列表
export function getRole() {
    return request.get(`/backrole/getrole`)
}

//获取指定角色
export function getUpRole(id) {
    return request.get(`/backrole/getuprole/${id}`)
}

//修改角色列表
export function upRole(data, id) {
    return request.put(`/backrole/uprole/${id}`, data)
}

//添加角色
export function addRole(data) {
    return request.post(`/backrole/addrole`, data)
}

//删除角色
export function deleteRole(id) {
    return request.delete(`/backrole/deleterole/${id}`)
}

//查询所有管理员
export function getAdminUser() {
    return request.get(`/BackUserAdmin/getadminuser`)
}

//查询指定管理员
export function getUpAdminUser(id) {
    return request.get(`/BackUserAdmin/getupadminuser/${id}`)
}

//添加管理员
export function addAdminUser(data) {
    return request.post(`/BackUserAdmin/adminregister`, data)
}

//修改管理员用户信息
export function upAdminUser(data, id) {
    return request.put(`/BackUserAdmin/upadminuser/${id}`, data)
}

//删除管理员用户
export function deleteAdminUser(id) {
    return request.delete(`/BackUserAdmin/deleteadminuser/${id}`)
}

//查询管理员角色
export function getAdminRole() {
    return request.get(`/backadminrole/getadminrole`);
}

//查询指定管理员角色
export function getUpAdminRole(id) {
    return request.get(`/backadminrole/getupadminrole/${id}`);
}

//添加管理员权限
export function addAdminRole(data) {
    return request.post(`/BackAdminRole/addadminrole`, data)
}

//修改管理员权限
export function upAdminRole(data, id) {
    return request.put(`/BackAdminRole/upadminrole/${id}`, data);
}

//删除管理员权限
export function deleteAdminRole(id) {
    return request.delete(`/backadminrole/deleteadminrole/${id}`)
}

// 获取CHINA
export function getAddress(){
    return request.get("https://restapi.amap.com/v3/config/district?keywords=&subdistrict=3&extensions=base&key=241b11c51787c21517d2e95e8f309075")
}

// 添加用户收货地址
export function addAddress(id,data){
    return request.post(`/BackUserAddress/addUserAddress/${id}`,data)
}

// 获取用户收货地址
export function getUserAddress(id){
    return request.get(`/BackUserAddress/getUserAddress/${id}`)
}

// 用户修改收货地址
export function updateUserAddress(id,data){
    return request.put(`/BackUserAddress/upUserAddress/${id}`,data)
}

// 删除收货地址
export function deleteUserAddress(id){
    return request.delete(`/BackUserAddress/delUserAddress/${id}`)
}