import request from '../utils/request'

// 获取所有订单
export function getAllOrder(params){
    return request.get('/Order/getOrder',{params:params})
}

// 获取订单详细信息
export function getOrderDetail(id){
    return request.get(`/Order/getuserOrderinfo/${id}`)
}