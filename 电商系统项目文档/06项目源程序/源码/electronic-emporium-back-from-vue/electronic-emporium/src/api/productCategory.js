import request from '../utils/request'

// 获取类型
export function getType(){
    return request.get("/BackType/gettype")
}

// 添加类型
export function addType(data){
    return request.post("/BackType/Addstpe",data)
}

// 获取属性
export function getAttr(id){
    return request.get(`/BackType/getAttribute/${id}`)
}

// 添加属性
export function addAttr(id,data){
    return request.post(`/BackType/AddAttribute/${id}`,data)
}

// 获取属性值
export function getAttributeValue(id){
    return request.get(`/BackType/getAttributevalue/${id}`)
}

// 添加属性值
export function addAttributeValue(id,data){
    return request.post(`/BackType/AddAttributevalue/${id}`,data)
}

// 修改类型
export function updateType(id,data){
    return request.put(`/BackType/upstpe/${id}`,data)
}

// 修改属性
export function updateAttr(id,data){
    return request.put(`/BackType/upAttribute/${id}`,data)
}

// 修改属性值
export function updateAttrValue(id,data){
    return request.put(`/BackType/upAttributevalue/${id}`,data)
}

// 删除类型及一下所有的关联id
export function deleteType(id){
    return request.delete(`/BackType/delstpe/${id}`)
}

// 删除属性及一下所有属性值id
export function deleteAttr(id){
    return request.delete(`/BackType/delAttribute/${id}`)
}

// 删除属性值
export function deleteAttrValue(id){
    return request.delete(`/BackType/delAttributevalue/${id}`)
}