import request from '../utils/request'

// 获取所有商品父级分类
export function getParentCategory() {
    return request.get("/BackCategory/getParentCategory")
}

// 添加父级分类
export function addParentCategory(data) {
    return request.post("BackCategory/addParentCategory", data)
}

// 修改父类名称
export function updateParentCategoryName(id, data) {
    return request.post(`/BackCategory/upParentCategory/${id}`, data)
}

// 删除父类名称
export function deleteParentCategory(id) {
    return request.delete(`BackCategory/delParentCategory/${id}`)
}

// 获取指定父类的子类
export function getSubCategory(id) {
    return request.get(`/BackCategory/getsubCategory/${id}`)
}

// 添加父类的子分类
export function addSubCategory(id, data) {
    return request.post(`BackCategory/addsubCategory/${id}`, data)
}

// 编辑父类的子分类商品
export function editSebCategory(id, data) {
    return request.post(`/BackCategory/upsubCategory/${id}`, data)
}

// 删除父类的子分类商品
export function deleteSubCategory(id) {
    return request.delete(`/BackCategory/delsubCategory/${id}`)
}

// 是否显示父级
export function isDisplayParent(id) {
    return request.put(`/BackCategory/Parentisactived/${id}`)
}

// 是否显示子级
export function isDisplayChildren(id) {
    return request.put(`/BackCategory/subisactived/${id}`)
}