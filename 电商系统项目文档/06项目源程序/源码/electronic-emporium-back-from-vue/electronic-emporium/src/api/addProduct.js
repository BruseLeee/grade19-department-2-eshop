import request from '../utils/request'

// 获取父类分类
export function getParentCategory(){
    return request.get("/General/getParentCategory")
}

// 根据父类id获取子分类
export function getChildrenCategory(id){
    return request.get(`/General/getsubCategory/${id}`)
}

// 获取品牌
export function getBrand(){
    return request.get('/General/getBrandInfo')
}

// 获取供应商
export function getProvider(){
    return request.get('/General/getSupplierInfo')
}

// 获取类型
export function getType(){
    return request.get("/BackType/gettype")
}

// 根据类型id获取属性
export function getAttr(id){
    return request.get(`/BackType/getAttribute/${id}`)
}

// 根据属性id获取属性值
export function getAttrValue(id){
    return request.get(`/BackType/getAttributevalue/${id}`)
}

// 添加商品
export function addProduct(data){
    return request.post("/BackProductInfo/AddProduct",data)
}

// 获取属性
export function getTypeValue(id){
    return request.get(`/BackType/gettypevalue/${id}`)
}

//添加商品属性json
export function postsku(id,data){
    return request.post(`/Sku/addsku/${id}`,data)
}

// 删除sku
export function deleteSku(id){
    return request.delete(`sku/delsku/${id}`)
}