import request from '../utils/request'

// 获取供应商
export function getProvider(){
    return request.get("/BackSupplierInfo/getSupplierInfo")
}

// 添加供应商
export function addProvider(data){
    return request.post("BackSupplierInfo/AddSupplierInfo",data)
}

// 编辑供应商
export function updateProvider(id,data){
    return request.put(`/BackSupplierInfo/UpSupplierInfo/${id}`,data)
}

// 删除供应商
export function deleteProvider(id){
    return request.delete(`/BackSupplierInfo/delSupplierInfo/${id}`)
}

// 是否启用供应商
export function isActived(id){
    return request.put(`/BackSupplierInfo/supplierisactived/${id}`)
}