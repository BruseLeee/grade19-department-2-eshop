var webpack = require('webpack');

module.exports = {
    configureWebpack: {
        plugins: [
            new webpack.ProvidePlugin({
                'window.Quill': 'quill/dist/quill.js',
                'Quill': 'quill/dist/quill.js'
            }),
        ]
    },

    publicPath: process.env.NODE_ENV === "produciton" ? './' : '/',
    devServer: {
        host: '127.0.0.1',
        headers: {
            'Access-Control-Allow-Origin': "*",
        },
        overlay: {
            warnings: false,
            errors: false
        },

    },



}