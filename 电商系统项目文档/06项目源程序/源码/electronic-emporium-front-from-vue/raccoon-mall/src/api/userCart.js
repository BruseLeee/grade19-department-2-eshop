import request from "../utils/request"

// 根据用户id获取用户购物车
export function getUserCart(id) {
    return request.get(`/OrderCart/getUserOrderCart/${id}`)
}

// 加入到用户购物车
export function addUserCart(id, data) {
    return request.post(`/OrderCart/addUserOrderCart/${id}`, data)
}

// 删除用户购物车
export function deleteUserCart(id) {
    return request.delete(`/OrderCart/delUserOrderCart/${id}`)
}

// 修改购物车商品数量
export function editCartCount(id, data) {
    return request.put(`/OrderCart/upUserOrderCart/${id}`, data)
}

// 获取当前用户收货地址
export function getUserAddress(id) {
    return request.get(`/General/getaddress/${id}`)
}