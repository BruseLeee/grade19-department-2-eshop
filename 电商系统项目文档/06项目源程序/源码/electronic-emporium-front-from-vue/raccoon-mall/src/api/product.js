import request from "../utils/request"

// 获取所有商品
export function getAllProduct(){
    return request.get("/BackProductInfo/getProduct")
}

// 根据商品id获取商品详情
export function getProductDetail(id){
    return request.get(`/General/getProduct/${id}`)
}