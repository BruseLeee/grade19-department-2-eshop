/*==============================================================*/
/* DBMS name:      Microsoft SQL Server 2012                    */
/* Created on:     2021/8/19 11:37:13                           */
/*==============================================================*/


if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Appraise') and o.name = 'FK_APPRAISE_REFERENCE_PRODUCTI')
alter table Appraise
   drop constraint FK_APPRAISE_REFERENCE_PRODUCTI
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Appraise') and o.name = 'FK_APPRAISE_REFERENCE_ORDER')
alter table Appraise
   drop constraint FK_APPRAISE_REFERENCE_ORDER
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Appraise') and o.name = 'FK_APPRAISE_REFERENCE_APPRAISE')
alter table Appraise
   drop constraint FK_APPRAISE_REFERENCE_APPRAISE
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('AppraisePictureInfo') and o.name = 'FK_APPRAISE_REFERENCE_APPRAISE')
alter table AppraisePictureInfo
   drop constraint FK_APPRAISE_REFERENCE_APPRAISE
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('AttributeValue') and o.name = 'FK_ATTRIBUT_REFERENCE_ATTRIBUT')
alter table AttributeValue
   drop constraint FK_ATTRIBUT_REFERENCE_ATTRIBUT
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('BalanceLog') and o.name = 'FK_BALANCEL_REFERENCE_ORDER')
alter table BalanceLog
   drop constraint FK_BALANCEL_REFERENCE_ORDER
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('BalanceLog') and o.name = 'FK_BALANCEL_REFERENCE_USER')
alter table BalanceLog
   drop constraint FK_BALANCEL_REFERENCE_USER
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('"Order"') and o.name = 'FK_ORDER_REFERENCE_USER')
alter table "Order"
   drop constraint FK_ORDER_REFERENCE_USER
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('"Order"') and o.name = 'FK_ORDER_REFERENCE_RECEIVEA')
alter table "Order"
   drop constraint FK_ORDER_REFERENCE_RECEIVEA
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('OrderCart') and o.name = 'FK_ORDERCAR_REFERENCE_USER')
alter table OrderCart
   drop constraint FK_ORDERCAR_REFERENCE_USER
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('OrderInfo') and o.name = 'FK_ORDERINF_REFERENCE_ORDER')
alter table OrderInfo
   drop constraint FK_ORDERINF_REFERENCE_ORDER
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('PictureInfo') and o.name = 'FK_PICTUREI_REFERENCE_PRODUCTI')
alter table PictureInfo
   drop constraint FK_PICTUREI_REFERENCE_PRODUCTI
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('ProductInfo') and o.name = 'FK_PRODUCTI_REFERENCE_BRANDINF')
alter table ProductInfo
   drop constraint FK_PRODUCTI_REFERENCE_BRANDINF
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('ProductInfo') and o.name = 'FK_PRODUCTI_REFERENCE_SUPPLIER')
alter table ProductInfo
   drop constraint FK_PRODUCTI_REFERENCE_SUPPLIER
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('ProductInfo') and o.name = 'FK_PRODUCTI_REFERENCE_PARENTCA')
alter table ProductInfo
   drop constraint FK_PRODUCTI_REFERENCE_PARENTCA
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('ProductInfo') and o.name = 'FK_PRODUCTI_REFERENCE_SUBCATEG')
alter table ProductInfo
   drop constraint FK_PRODUCTI_REFERENCE_SUBCATEG
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('ReceiveAddress') and o.name = 'FK_RECEIVEA_REFERENCE_USER')
alter table ReceiveAddress
   drop constraint FK_RECEIVEA_REFERENCE_USER
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('ReplayAppraise') and o.name = 'FK_REPLAYAP_REFERENCE_APPRAISE')
alter table ReplayAppraise
   drop constraint FK_REPLAYAP_REFERENCE_APPRAISE
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Sku') and o.name = 'FK_SKU_REFERENCE_ATTRIBUT')
alter table Sku
   drop constraint FK_SKU_REFERENCE_ATTRIBUT
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Sku') and o.name = 'FK_SKU_REFERENCE_PRODUCTI')
alter table Sku
   drop constraint FK_SKU_REFERENCE_PRODUCTI
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Sku') and o.name = 'FK_SKU_REFERENCE_ATTRIBUT')
alter table Sku
   drop constraint FK_SKU_REFERENCE_ATTRIBUT
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('SubCategory') and o.name = 'FK_SUBCATEG_REFERENCE_PARENTCA')
alter table SubCategory
   drop constraint FK_SUBCATEG_REFERENCE_PARENTCA
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('"User"') and o.name = 'FK_USER_REFERENCE_PROBLEM')
alter table "User"
   drop constraint FK_USER_REFERENCE_PROBLEM
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('UserInfo') and o.name = 'FK_USERINFO_REFERENCE_USER')
alter table UserInfo
   drop constraint FK_USERINFO_REFERENCE_USER
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('管理员角色') and o.name = 'FK_管理员角色_REFERENCE_ROLE')
alter table 管理员角色
   drop constraint FK_管理员角色_REFERENCE_ROLE
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('管理员角色') and o.name = 'FK_管理员角色_REFERENCE_ADMINUSE')
alter table 管理员角色
   drop constraint FK_管理员角色_REFERENCE_ADMINUSE
go

if exists (select 1
            from  sysobjects
           where  id = object_id('AdminUser')
            and   type = 'U')
   drop table AdminUser
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Appraise')
            and   type = 'U')
   drop table Appraise
go

if exists (select 1
            from  sysobjects
           where  id = object_id('AppraisePictureInfo')
            and   type = 'U')
   drop table AppraisePictureInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('AppraiseTItle')
            and   type = 'U')
   drop table AppraiseTItle
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Attribute')
            and   type = 'U')
   drop table Attribute
go

if exists (select 1
            from  sysobjects
           where  id = object_id('AttributeValue')
            and   type = 'U')
   drop table AttributeValue
go

if exists (select 1
            from  sysobjects
           where  id = object_id('BalanceLog')
            and   type = 'U')
   drop table BalanceLog
go

if exists (select 1
            from  sysobjects
           where  id = object_id('BrandInfo')
            and   type = 'U')
   drop table BrandInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('InventoryInfo')
            and   type = 'U')
   drop table InventoryInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('"Order"')
            and   type = 'U')
   drop table "Order"
go

if exists (select 1
            from  sysobjects
           where  id = object_id('OrderCart')
            and   type = 'U')
   drop table OrderCart
go

if exists (select 1
            from  sysobjects
           where  id = object_id('OrderInfo')
            and   type = 'U')
   drop table OrderInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('ParentCategory')
            and   type = 'U')
   drop table ParentCategory
go

if exists (select 1
            from  sysobjects
           where  id = object_id('PictureInfo')
            and   type = 'U')
   drop table PictureInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Problem')
            and   type = 'U')
   drop table Problem
go

if exists (select 1
            from  sysobjects
           where  id = object_id('ProductInfo')
            and   type = 'U')
   drop table ProductInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('ReceiveAddress')
            and   type = 'U')
   drop table ReceiveAddress
go

if exists (select 1
            from  sysobjects
           where  id = object_id('ReplayAppraise')
            and   type = 'U')
   drop table ReplayAppraise
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Role')
            and   type = 'U')
   drop table Role
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Sku')
            and   type = 'U')
   drop table Sku
go

if exists (select 1
            from  sysobjects
           where  id = object_id('SubCategory')
            and   type = 'U')
   drop table SubCategory
go

if exists (select 1
            from  sysobjects
           where  id = object_id('SupplierInfo')
            and   type = 'U')
   drop table SupplierInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('"User"')
            and   type = 'U')
   drop table "User"
go

if exists (select 1
            from  sysobjects
           where  id = object_id('UserInfo')
            and   type = 'U')
   drop table UserInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('管理员角色')
            and   type = 'U')
   drop table 管理员角色
go

/*==============================================================*/
/* Table: AdminUser                                             */
/*==============================================================*/
create table AdminUser (
   Id                   int                  not null,
   Username             varchar(80)          null,
   Password             varchar(80)          null,
   Nickname             varchar(80)          null,
   Phone                varchar(80)          null,
   Gender               int                  null,
   Status               bit                  null,
   CreatedTime          datetime             null,
   IsDeleted            bit                  null,
   UpdatedTime          datetime             null,
   constraint PK_ADMINUSER primary key (Id)
)
go

/*==============================================================*/
/* Table: Appraise                                              */
/*==============================================================*/
create table Appraise (
   Id                   int                  not null,
   ProductId            int                  null,
   OrderId              int                  null,
   TitleId              int                  null,
   Content              varchar(80)          null,
   Appraise             int                  null,
   CreatedTime          datetime             null,
   IsDeleted            bit                  null,
   constraint PK_APPRAISE primary key (Id)
)
go

/*==============================================================*/
/* Table: AppraisePictureInfo                                   */
/*==============================================================*/
create table AppraisePictureInfo (
   Id                   int                  not null,
   Appraise             int                  null,
   Sorting              int                  null,
   PictureUrl           varchar(80)          null,
   IsDeleted            bit                  null,
   constraint PK_APPRAISEPICTUREINFO primary key (Id)
)
go

/*==============================================================*/
/* Table: AppraiseTItle                                         */
/*==============================================================*/
create table AppraiseTItle (
   Id                   int                  not null,
   Name                 varchar(80)          null,
   Status               bit                  null,
   IsDeleted            bit                  null,
   UpdatedTime          datetime             null,
   constraint PK_APPRAISETITLE primary key (Id)
)
go

/*==============================================================*/
/* Table: Attribute                                             */
/*==============================================================*/
create table Attribute (
   Id                   int                  not null,
   Name                 varchar(80)          null,
   Status               bit                  null,
   IsDeleted            bit                  null,
   UpdatedTime          datetime             null,
   constraint PK_ATTRIBUTE primary key (Id)
)
go

/*==============================================================*/
/* Table: AttributeValue                                        */
/*==============================================================*/
create table AttributeValue (
   AttributeId          int                  null,
   Name0                varchar(80)          null,
   Status               bit                  null,
   IsDeleted            bit                  null,
   UpdatedTime          datetime             null,
   Id                   int                  not null,
   constraint PK_ATTRIBUTEVALUE primary key (Id)
)
go

/*==============================================================*/
/* Table: BalanceLog                                            */
/*==============================================================*/
create table BalanceLog (
   Id                   int                  not null,
   UserId               int                  null,
   OrderId              int                  null,
   Source               int                  null,
   Amount               decimal              null,
   CreatedTime          datetime             null,
   IsDeleted            bit                  null,
   constraint PK_BALANCELOG primary key (Id)
)
go

/*==============================================================*/
/* Table: BrandInfo                                             */
/*==============================================================*/
create table BrandInfo (
   Id                   int                  not null,
   Name                 varchar(80)          null,
   LogoUrl              varchar(80)          null,
   Description          varchar(80)          null,
   IsRecommend          bit                  null,
   Status               bit                  null,
   IsDeleted            bit                  null,
   UpdatedTime          datetime             null,
   constraint PK_BRANDINFO primary key (Id)
)
go

/*==============================================================*/
/* Table: InventoryInfo                                         */
/*==============================================================*/
create table InventoryInfo (
   Id                   int                  not null,
   SKU                  int                  null,
   SellPrice            decimal              null,
   InventoryCount       int                  null,
   IsDeleted            bit                  null,
   constraint PK_INVENTORYINFO primary key (Id)
)
go

/*==============================================================*/
/* Table: "Order"                                               */
/*==============================================================*/
create table "Order" (
   Id                   int                  not null,
   Orderserial          int                  null,
   UserId               int                  null,
   UserName             varchar(80)          null,
   ReceiveAddress       int                  null,
   Payload              int                  null,
   OrderMoney           decimal              null,
   PlaceOrder           datetime             null,
   Payment              datetime             null,
   OrderStatus          int                  null,
   IsDeleted            bit                  null,
   constraint PK_ORDER primary key (Id)
)
go

/*==============================================================*/
/* Table: OrderCart                                             */
/*==============================================================*/
create table OrderCart (
   Id                   int                  not null,
   UserId               int                  null,
   Sku                  int                  null,
   Number               int                  null,
   Payment              datetime             null,
   OrderStatus          int                  null,
   IsDeleted            bit                  null,
   constraint PK_ORDERCART primary key (Id)
)
go

/*==============================================================*/
/* Table: OrderInfo                                             */
/*==============================================================*/
create table OrderInfo (
   Id                   int                  not null,
   OrderId              int                  null,
   SKU                  int                  null,
   Number               int                  null,
   Clish                decimal              null,
   IsDeleted            bit                  null,
   constraint PK_ORDERINFO primary key (Id)
)
go

/*==============================================================*/
/* Table: ParentCategory                                        */
/*==============================================================*/
create table ParentCategory (
   Id                   int                  not null,
   Name                 varchar(80)          null,
   Status               bit                  null,
   IsDeleted            bit                  null,
   UpdatedTime          datetime             null,
   constraint PK_PARENTCATEGORY primary key (Id)
)
go

/*==============================================================*/
/* Table: PictureInfo                                           */
/*==============================================================*/
create table PictureInfo (
   Id                   int                  not null,
   ProductId            int                  null,
   Sorting              int                  null,
   PictureUrl           varchar(80)          null,
   Status               bit                  null,
   IsDeleted            bit                  null,
   constraint PK_PICTUREINFO primary key (Id)
)
go

/*==============================================================*/
/* Table: Problem                                               */
/*==============================================================*/
create table Problem (
   Id                   int                  not null,
   Username             varchar(80)          null,
   Status               bit                  null,
   IsDeleted            bit                  null,
   constraint PK_PROBLEM primary key (Id)
)
go

/*==============================================================*/
/* Table: ProductInfo                                           */
/*==============================================================*/
create table ProductInfo (
   Id                   int                  not null,
   ParentId             int                  null,
   SubId                int                  null,
   Name                 varchar(80)          null,
   BrandId              int                  null,
   SupplierId           int                  null,
   Description          varchar(80)          null,
   CostPrice            decimal              null,
   Weight               float                null,
   Unit                 varchar(80)          null,
   IsRecommend          bit                  null,
   IsNew                bit                  null,
   Status               bit                  null,
   CreatedTime          datetime             null,
   IsDeleted            bit                  null,
   UpdatedTime          datetime             null,
   constraint PK_PRODUCTINFO primary key (Id)
)
go

/*==============================================================*/
/* Table: ReceiveAddress                                        */
/*==============================================================*/
create table ReceiveAddress (
   Id                   int                  not null,
   UserId               int                  null,
   Name                 varchar(80)          null,
   Phone                varchar(80)          null,
   PostCode             varchar(80)          null,
   Province             varchar(80)          null,
   City                 varchar(80)          null,
   Region               varchar(80)          null,
   DetailAddress        varchar(80)          null,
   DefaultStatus        bit                  null,
   IsDeleted            bit                  null,
   UpdatedTime          datetime             null,
   constraint PK_RECEIVEADDRESS primary key (Id)
)
go

/*==============================================================*/
/* Table: ReplayAppraise                                        */
/*==============================================================*/
create table ReplayAppraise (
   Id                   int                  not null,
   AppraiseId           int                  null,
   Content              varchar(80)          null,
   Appraise             int                  null,
   CreatedTime          datetime             null,
   IdDeleted            bit                  null,
   constraint PK_REPLAYAPPRAISE primary key (Id)
)
go

/*==============================================================*/
/* Table: Role                                                  */
/*==============================================================*/
create table Role (
   Id                   int                  not null,
   Name                 varchar(80)          null,
   Desctiption          varchar(80)          null,
   Status               bit                  null,
   IsDeleted            bit                  null,
   UpdatedTime          datetime             null,
   constraint PK_ROLE primary key (Id)
)
go

/*==============================================================*/
/* Table: Sku                                                   */
/*==============================================================*/
create table Sku (
   Id                   int                  not null,
   AttributeId          int                  null,
   Att_Id               int                  null,
   ProductId            int                  null,
   SKU                  int                  null,
   Status               bit                  null,
   IsDeleted            bit                  null,
   UpdatedTime          datetime             null,
   constraint PK_SKU primary key (Id)
)
go

/*==============================================================*/
/* Table: SubCategory                                           */
/*==============================================================*/
create table SubCategory (
   Id                   int                  not null,
   Name                 varchar(80)          null,
   ParentId             int                  null,
   Status               bit                  null,
   IsDeleted            bit                  null,
   UpdatedTime          datetime             null,
   constraint PK_SUBCATEGORY primary key (Id)
)
go

/*==============================================================*/
/* Table: SupplierInfo                                          */
/*==============================================================*/
create table SupplierInfo (
   Id                   int                  not null,
   Name                 varchar(80)          null,
   Type                 int                  null,
   LinkMan              varchar(80)          null,
   BankName             varchar(80)          null,
   BankAccount          varchar(80)          null,
   Address              varchar(80)          null,
   Status               bit                  null,
   IsDeleted            bit                  null,
   UpdatedTime          datetime             null,
   Phone                varchar(80)          null,
   constraint PK_SUPPLIERINFO primary key (Id)
)
go

/*==============================================================*/
/* Table: "User"                                                */
/*==============================================================*/
create table "User" (
   Id                   int                  not null,
   Username             varchar(80)          null,
   Password             varchar(80)          null,
   Problem              int                  null,
   Answer               varchar(80)          null,
   Status               bit                  null,
   CreatedTime          datetime             null,
   IsDeleted            bit                  null,
   constraint PK_USER primary key (Id)
)
go

/*==============================================================*/
/* Table: UserInfo                                              */
/*==============================================================*/
create table UserInfo (
   Id                   int                  not null,
   UserId               int                  null,
   Nickname             varchar(80)          null,
   Phone                varchar(80)          null,
   IconUrl              varchar(80)          null,
   Gender               int                  null,
   Birthday             datetime             null,
   City                 varchar(80)          null,
   Money                decimal              null,
   Joy                  varchar(80)          null,
   UpdatedTime          datetime             null,
   constraint PK_USERINFO primary key (Id)
)
go

/*==============================================================*/
/* Table: 管理员角色                                                 */
/*==============================================================*/
create table 管理员角色 (
   Id                   int                  not null,
   RoleId               int                  null,
   AdminId              int                  null,
   IsDeleted            bit                  null,
   UpdatedTime          datetime             null,
   constraint PK_管理员角色 primary key (Id)
)
go

alter table Appraise
   add constraint FK_APPRAISE_REFERENCE_PRODUCTI foreign key (ProductId)
      references ProductInfo (Id)
go

alter table Appraise
   add constraint FK_APPRAISE_REFERENCE_ORDER foreign key (OrderId)
      references "Order" (Id)
go

alter table Appraise
   add constraint FK_APPRAISE_REFERENCE_APPRAISE foreign key (TitleId)
      references AppraiseTItle (Id)
go

alter table AppraisePictureInfo
   add constraint FK_APPRAISE_REFERENCE_APPRAISE foreign key (Appraise)
      references Appraise (Id)
go

alter table AttributeValue
   add constraint FK_ATTRIBUT_REFERENCE_ATTRIBUT foreign key (AttributeId)
      references Attribute (Id)
go

alter table BalanceLog
   add constraint FK_BALANCEL_REFERENCE_ORDER foreign key (OrderId)
      references "Order" (Id)
go

alter table BalanceLog
   add constraint FK_BALANCEL_REFERENCE_USER foreign key (UserId)
      references "User" (Id)
go

alter table "Order"
   add constraint FK_ORDER_REFERENCE_USER foreign key (UserId)
      references "User" (Id)
go

alter table "Order"
   add constraint FK_ORDER_REFERENCE_RECEIVEA foreign key (ReceiveAddress)
      references ReceiveAddress (Id)
go

alter table OrderCart
   add constraint FK_ORDERCAR_REFERENCE_USER foreign key (UserId)
      references "User" (Id)
go

alter table OrderInfo
   add constraint FK_ORDERINF_REFERENCE_ORDER foreign key (OrderId)
      references "Order" (Id)
go

alter table PictureInfo
   add constraint FK_PICTUREI_REFERENCE_PRODUCTI foreign key (ProductId)
      references ProductInfo (Id)
go

alter table ProductInfo
   add constraint FK_PRODUCTI_REFERENCE_BRANDINF foreign key (BrandId)
      references BrandInfo (Id)
go

alter table ProductInfo
   add constraint FK_PRODUCTI_REFERENCE_SUPPLIER foreign key (SupplierId)
      references SupplierInfo (Id)
go

alter table ProductInfo
   add constraint FK_PRODUCTI_REFERENCE_PARENTCA foreign key (ParentId)
      references ParentCategory (Id)
go

alter table ProductInfo
   add constraint FK_PRODUCTI_REFERENCE_SUBCATEG foreign key (SubId)
      references SubCategory (Id)
go

alter table ReceiveAddress
   add constraint FK_RECEIVEA_REFERENCE_USER foreign key (UserId)
      references "User" (Id)
go

alter table ReplayAppraise
   add constraint FK_REPLAYAP_REFERENCE_APPRAISE foreign key (AppraiseId)
      references Appraise (Id)
go

alter table Sku
   add constraint FK_SKU_REFERENCE_ATTRIBUT foreign key (AttributeId)
      references Attribute (Id)
go

alter table Sku
   add constraint FK_SKU_REFERENCE_PRODUCTI foreign key (ProductId)
      references ProductInfo (Id)
go

alter table Sku
   add constraint FK_SKU_REFERENCE_ATTRIBUT foreign key (Att_Id)
      references AttributeValue (Id)
go

alter table SubCategory
   add constraint FK_SUBCATEG_REFERENCE_PARENTCA foreign key (ParentId)
      references ParentCategory (Id)
go

alter table "User"
   add constraint FK_USER_REFERENCE_PROBLEM foreign key (Problem)
      references Problem (Id)
go

alter table UserInfo
   add constraint FK_USERINFO_REFERENCE_USER foreign key (UserId)
      references "User" (Id)
go

alter table 管理员角色
   add constraint FK_管理员角色_REFERENCE_ROLE foreign key (RoleId)
      references Role (Id)
go

alter table 管理员角色
   add constraint FK_管理员角色_REFERENCE_ADMINUSE foreign key (AdminId)
      references AdminUser (Id)
go

