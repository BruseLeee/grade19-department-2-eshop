 管理员权限列表管理

# 一、任务描述

​		主要完成管理员权限管理功能，主要包括管理员权限的添加、修改、删除功能。

| 具体要求                                    |
|-----------------------------------------|
| 新增管理员权限：实现管理员权限的新增         |
| 管理员权限修改：实现对现有管理员权限进行修改 |
| 管理员权限删除：实现对现有管理员权限进行删除 |

# 二、知识点

1. Element UI各种组件的综合使用（LayUI 栅格布局、模态框组件、Table组件、Form组件等）
2. Axios
3. EF Core和LINQ的综合运用
4. 后端控制器的数据处理

# 三、重点和难点

2. 添加管理员权限，需要注意的是：需要对添加的管理员权限做重复验证。


# 四、核心代码

# 1. 管理员权限列表展示

## 1.1功能讲解  
&emsp;&emsp;点击左边管理员权限列表目录后，TabPage展示用所有管理员权限数据，实现用户信息分页功能，展示分页数据。

## 1.2主要代码

### 1.2.1前端代码
```
  <el-card class="search-card">
      <el-form :inline="true" :model="formInline" class="demo-form-inline">
        <el-input
          v-model="formInline.searchText"
          class="search"
          placeholder="根据问题名称查询..."
        ></el-input>
        &nbsp; &nbsp;
        <el-button @click="editAddUser" type="primary" size="stander"
          ><i class="el-icon-circle-plus-outline">添加管理员权限</i>
        </el-button>
      </el-form>
      <div></div>
    </el-card>
      <el-card class="form-card">
      <el-table :data="tableData" max-height="680px" class="elTable">
        <el-table-column label="管理员用户名" prop="adminName" align="center">
        </el-table-column>
        <el-table-column label="角色名称" prop="roleName" align="center">
        </el-table-column>
        <el-table-column label="操作" align="center">
          <!-- <template slot="header">
        <el-input v-model="search" size="mini" placeholder="输入关键字搜索" />
      </template> -->
          <template slot-scope="scope">
            <el-button size="mini" @click="detial(scope.$index, scope.row)"
              >编辑</el-button
            >
            <el-button
              size="mini"
              type="danger"
              @click="handleDelete(scope.$index, scope.row)"
              >删除</el-button
            >
          </template>
        </el-table-column>
      </el-table>
    </el-card>
```
### 1.2.2控制器代码
```
  [HttpPost]
        [Route("addadminrole")]
        public dynamic AdminRole(AdminRoleParams newAdminRole)
        {
            var adminname = newAdminRole.AdminName;
            var rolename = newAdminRole.RoleName;

            var admin = _Context.AdminUser.Where(x => x.Username == adminname).SingleOrDefault();
            var role = _Context.Role.Where(x => x.Name == rolename).SingleOrDefault();

            if (string.IsNullOrEmpty(adminname) && string.IsNullOrEmpty(rolename))
            {
                return DataStatus.DataError(1111, "传入的权限和管理员名称不能为空");
            }

            if (admin == null || role == null)
            {
                return DataStatus.DataError(11112, "管理员或权限不存在，请重新输入！");
            }

            var adminId = admin.Id;
            var roleId = role.Id;

            var isAdminRole = _adminRoleRepository.Table.Where(x => x.AdminId == adminId && x.RoleId == roleId).SingleOrDefault();

            if (isAdminRole != null)
            {
                return DataStatus.DataError(1113, "该管理员已有权限，请重新设置！");
            }

            var adminrole = new AdminRole
            {
                AdminId = adminId,
                RoleId = roleId
            };

            _adminRoleRepository.Insert(adminrole);

            return DataStatus.DataSuccess(1000, new { AdminName = adminname, RoleName = rolename }, "查询成功");
        }
```


# 2.添加用户信息
## 2.1功能讲解
&emsp;&emsp;实现管理员权限添加功能。

## 2.2主要代码

### 2.2.1前端代码
```
  <el-card class="search-card">
      <el-form :inline="true" :model="formInline" class="demo-form-inline">
        <el-input
          v-model="formInline.searchText"
          class="search"
          placeholder="根据问题名称查询..."
        ></el-input>
        &nbsp; &nbsp;
        <el-button @click="editAddUser" type="primary" size="stander"
          ><i class="el-icon-circle-plus-outline">添加管理员权限</i>
        </el-button>
      </el-form>
      <div></div>
    </el-card>
```
### 2.2.2控制器代码
```
  [HttpPost]
        [Route("addadminrole")]
        public dynamic AdminRole(AdminRoleParams newAdminRole)
        {
            var adminname = newAdminRole.AdminName;
            var rolename = newAdminRole.RoleName;

            var admin = _Context.AdminUser.Where(x => x.Username == adminname).SingleOrDefault();
            var role = _Context.Role.Where(x => x.Name == rolename).SingleOrDefault();

            if (string.IsNullOrEmpty(adminname) && string.IsNullOrEmpty(rolename))
            {
                return DataStatus.DataError(1111, "传入的权限和管理员名称不能为空");
            }

            if (admin == null || role == null)
            {
                return DataStatus.DataError(11112, "管理员或权限不存在，请重新输入！");
            }

            var adminId = admin.Id;
            var roleId = role.Id;

            var isAdminRole = _adminRoleRepository.Table.Where(x => x.AdminId == adminId && x.RoleId == roleId).SingleOrDefault();

            if (isAdminRole != null)
            {
                return DataStatus.DataError(1113, "该管理员已有权限，请重新设置！");
            }

            var adminrole = new AdminRole
            {
                AdminId = adminId,
                RoleId = roleId
            };

            _adminRoleRepository.Insert(adminrole);

            return DataStatus.DataSuccess(1000, new { AdminName = adminname, RoleName = rolename }, "查询成功");
        }
```




# 3.用户详情
## 3.1功能讲解
&emsp;&emsp;实现对当前管理员权限进行修改。

## 3.2主要代码

### 3.2.1前端代码
```
  <el-dialog
      :title="UserInfoTitle"
      :visible.sync="dialogFormVisible"
      :before-close="handleCancel"
    >
      <el-form :model="formData" :rules="formrules" ref="formData">
        <el-form-item label="管理员用户名" label-width="70px" prop="adminName">
          <el-select
            placeholder="请选择..."
            :disabled="readonly"
            v-model="formData.AdminName"
          >
            <el-option
              v-for="(item, index) in adminName"
              :label="item.username"
              :value="item.username"
              :key="index"
            >
              {{ item.username }}
            </el-option>
          </el-select>
        </el-form-item>
        <el-form-item label="角色名称" label-width="70px" prop="roleName">
          <el-select
            placeholder="请选择..."
            :disabled="readonly"
            v-model="formData.RoleName"
          >
            <el-option
              v-for="(item, index) in roleName"
              :label="item.name"
              :value="item.name"
              :key="index"
            >
              {{ item.name }}
            </el-option>
          </el-select>
        </el-form-item>
      </el-form>
      <div slot="footer" class="dialog-footer">
        <el-button @click="edit">编辑</el-button>
        <el-button type="primary" @click="save">保存</el-button>
      </div>
    </el-dialog>
```
### 3.2.2控制器代码
```
  [HttpPut]
        [Route("upadminrole/{id}")]
        public dynamic upAdminRole(AdminRoleParams upAdminRole, int id)
        {
            var adminrole = _adminRoleRepository.GetId(id);

            var adminname = upAdminRole.AdminName;
            var rolename = upAdminRole.RoleName;

            var admin = _Context.AdminUser.Where(x => x.Username == adminname).SingleOrDefault();
            var role = _Context.Role.Where(x => x.Name == rolename).SingleOrDefault();

            if (adminname == null && rolename == null)
            {
                return DataStatus.DataError(1111, "传入的权限和管理员名称不能为空");
            }

            if (admin == null || role == null)
            {
                return DataStatus.DataError(1112, "管理员或权限不存在，请重新输入！");
            }

            var adminId = admin.Id;
            var roleId = role.Id;

            var isAdminRole = _adminRoleRepository.Table.Where(x => x.AdminId == adminId && x.RoleId == roleId).SingleOrDefault();

            if (isAdminRole != null)
            {
                return DataStatus.DataError(1113, "该管理员已有权限，请重新设置！");
            }

            adminrole.AdminId = adminId;
            adminrole.RoleId = roleId;

            _adminRoleRepository.Update(adminrole);

            return DataStatus.DataSuccess(1000, new { AdminName = adminname, RoleName = rolename }, "权限修改成功");
        }
```




# 4.删除管理员权限
## 4.1功能讲解
&emsp;&emsp;实现删除当前管理员权限。

## 4.2主要代码

### 4.2.1前端代码
```
<el-dialog title="提示" :visible.sync="dialogVisible" width="30%">
      <span>确定删除吗</span>
      <span slot="footer" class="dialog-footer">
        <el-button @click="dialogVisible = false">取 消</el-button>
        <el-button type="primary" @click="dialogVisible = false"
          >确 定</el-button
        >
      </span>
    </el-dialog>
```
### 4.2.2控制器代码
```

        [HttpDelete]
        [Route("deleteadminrole/{id}")]
        public dynamic deleteAdminrole(int id)
        {
            var adminrole = _adminRoleRepository.GetId(id);
            _adminRoleRepository.Delete(id);
            return DataStatus.DataSuccess(1000, id, "删除成功");
        }
```