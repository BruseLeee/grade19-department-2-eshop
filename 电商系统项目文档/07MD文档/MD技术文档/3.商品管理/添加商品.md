 添加商品

# 一、任务描述

​		主要完成添加商品功能，主要包括商品的添加功能。

| 具体要求                                                     |
| ------------------------------------------------------------ |
| 新增商品：实现商品的新增                                 |

# 二、知识点

1. Element UI各种组件的综合使用（LayUI 栅格布局、模态框组件、Table组件、Form组件等）
2. Axios
3. EF Core和LINQ的综合运用
4. 后端控制器的数据处理

# 三、重点和难点

1. 商品展示，主要难点：添加商品需要进行多个表添加信息。
2. 添加商品，需要注意的是：需要对添加的商品做重复验证。
3. 收货地址接口的使，并将地址保存到数据库中


# 四、核心代码

# 1. 添加商品

## 1.1功能讲解  
&emsp;&emsp;点击左边商品添加目录后，TabPage展示填写商品信息。

## 1.2主要代码

### 1.2.1前端代码
```
  <el-card class="form-card top" v-show="form">
      <div class="inner">
        <el-form
          :label-position="labelPosition"
          label-width="100px"
          :model="addForm"
          ref="addForm"
          class="demo-ruleForm"
        >
          <div class="title">
            <h1>填写商品信息</h1>
          </div>
          <el-form-item label="商品分类" prop="parentProduct">
            <el-select v-model="addForm.parentProduct" placeholder="请选择分类">
              <el-option
                v-for="(item, index) in parentOptions"
                :key="index"
                :label="item.name"
                :value="item.id"
                @click.native="selectGet(item.id)"
              ></el-option>
            </el-select>
          </el-form-item>
          <el-form-item label="商品子分类" prop="childrenProduct">
            <el-select
              v-model="addForm.childrenProduct"
              placeholder="请选择子类"
            >
              <el-option
                v-for="(item, index) in childrenOptions"
                :key="index"
                :label="item.name"
                :value="item.id"
              ></el-option>
            </el-select>
          </el-form-item>
          <el-form-item label="商品名称" prop="name">
            <el-input v-model="addForm.name"></el-input>
          </el-form-item>
          <el-form-item label="品牌" prop="brand">
            <el-select v-model="addForm.brand" placeholder="请选择品牌">
              <el-option
                v-for="(item, index) in brandList"
                :key="index"
                :label="item.name"
                :value="item.id"
              ></el-option>
            </el-select>
          </el-form-item>
          <el-form-item label="供应商" prop="provider">
            <el-select v-model="addForm.provider" placeholder="请选择供应商">
              <el-option
                v-for="(item, index) in providerList"
                :key="index"
                :label="item.name"
                :value="item.id"
              ></el-option>
            </el-select>
          </el-form-item>
          <el-form-item label="商品图片" prop="picture" align="left">
            <el-upload
              class="upload-demo"
              ref="upload"
              :action="pathandimg"
              list-type="picture-card"
              :on-success="addPictureImageSuccess"
              :on-preview="handlePreview"
              :on-remove="handleRemove"
              :file-list="fileList"
              :auto-upload="false"
              :limit="1"
            >
              <i class="el-icon-plus"></i>
            </el-upload>
          </el-form-item>
          <el-form-item label="商品描述" prop="desc">
            <!--  富文本 -->
            <quill-editor
              class="editor"
              v-model="content"
              :options="editorOption"
              @blur="onEditorBlur($event)"
              @focus="onEditorFocus($event)"
              @change="onEditorChange($event)"
            ></quill-editor>
          </el-form-item>
          <el-form-item label="供应价格" prop="CostPrice">
            <el-input v-model="addForm.CostPrice"></el-input>
          </el-form-item>
          <el-form-item label="单位" prop="unit">
            <el-input v-model="addForm.unit"></el-input>
          </el-form-item>
          <el-form-item label="是否推荐" prop="isRecommend" align="left">
            <el-radio v-model="addForm.isRecommend" label="true">是</el-radio>
            <el-radio v-model="addForm.isRecommend" label="false">否</el-radio>
          </el-form-item>
          <el-form-item label="是否新品" prop="isNew" align="left">
            <el-radio v-model="addForm.isNew" label="true">是</el-radio>
            <el-radio v-model="addForm.isNew" label="false">否</el-radio>
          </el-form-item>
          <!-- <el-form-item label="是否启用" prop="isActive" align="left">
            <el-radio v-model="addForm.isActive" label="true">是</el-radio>
            <el-radio v-model="addForm.isActive" label="false">否</el-radio>
          </el-form-item> -->
          <el-form-item>
            <template slot-scope="scope">
              <el-button
                size="large"
                @click="goodsUp(scope.$index, scope.row)"
                type="primary"
                class="btn"
                >下一步,填写商品属性</el-button
              >
            </template>
          </el-form-item>
        </el-form>
      </div>
    </el-card>


     <el-card class="form-card top" v-show="form == false">
      <el-button @click="back">返回</el-button>
      <div class="inner">
        <el-form
          :label-position="labelPosition"
          label-width="100px"
          :model="addForm"
          ref="addForm"
          class="demo-ruleForm"
        >
          <div class="title">
            <h1>填写商品属性</h1>
          </div>

          <el-form-item label="商品类型" prop="type"> </el-form-item>
          <el-select v-model="addForm.type" placeholder="请选择类型">
            <el-option
              v-for="(item, index) in typeList"
              :key="index"
              :label="item.name"
              :value="item.id"
              @click.native="selectGetType(item.id)"
            ></el-option>
          </el-select>
          <el-card
            class="value"
            v-for="(item, indes) in allTypeValue"
            :key="indes"
            :label="item.keyValue"
            :value="item.keyId"
          >
            {{ item.keyValue }} :

            <!-- <div>
              <el-checkbox-group> </el-checkbox-group>
            </div> -->
            <el-checkbox-group
              v-model="allTypeValue[indes].selectValue"
              :max="1"
            >
              <div v-for="(items, indexs) in item.value" :key="indexs">
                <el-checkbox :label="items.id" :key="items.id"
                  >{{ items.name }}
                </el-checkbox>
              </div>
            </el-checkbox-group>
          </el-card>
          <div v-if="istype">
            价格：<el-input type="text" v-model="price" />
          </div>
          <br />

          <el-button
            size="large"
            @click="reTypegodata"
            type="primary"
            class="btn"
            v-if="istype"
            >添加属性</el-button
          >
          <!-- </el-select> -->
          
          <el-table :data="skuTable" border style="width: 100%" v-if="istype">
            <el-table-column prop="Sku" label="Sku编号" width="180">
            </el-table-column>
            <el-table-column prop="keyvalues" label="属性值" width="180">
            </el-table-column>
            <el-table-column
              prop="price"
              label="售价"
              width="180"
            ></el-table-column>
            <el-table-column>
              <template slot-scope="scope">
                <el-button
                  size="mini"
                  @click="skuDelete(scope.$index, scope.row)"
                  type="primary"
                  class="btn"
                  >删除</el-button
                >
              </template>
            </el-table-column>
          </el-table>
          <br />
          <br />
          <el-form-item>
            <template slot-scope="scope">
              <el-button
                size="large"
                @click="submitForm(scope.$index, scope.row)"
                type="primary"
                class="btn"
                >跳转至商品列表</el-button
              >
            </template>
          </el-form-item>
        </el-form>
      </div>
    </el-card>
```
### 1.2.2控制器代码
```
 [HttpPost]
        [Route("AddProduct")]
        public dynamic AddProduct(AddorUpProduct AddProduct)
        {
            var parentId = AddProduct.ParentId;
            var subId = AddProduct.SubId;
            var name = AddProduct.Name;
            var brandId = AddProduct.BrandId;
            var supplierId = AddProduct.SupplierId;
            var descript = AddProduct.Descript;
            var costPrice = AddProduct.CostPrice;
            var unit = AddProduct.Unit;
            var isRecommend = AddProduct.IsRecommend;
            var isNew = AddProduct.IsNew;

            var isParent = _Context.ParentCategory.Where(x => x.Id == parentId).SingleOrDefault();
            if (isParent == null)
            {
                return DataStatus.DataError(1111, "请检查必填字段是否都填写规范！");
            }

            var issub = _Context.SubCateGory.Where(x => x.Id == subId).SingleOrDefault();
            if (issub == null)
            {
                return DataStatus.DataError(1111, "请检查必填字段是否都填写规范！");
            }

            var isbrand = _Context.BrandInfo.Where(x => x.Id == brandId).SingleOrDefault();
            if (isbrand == null)
            {
                return DataStatus.DataError(1111, "请检查必填字段是否都填写规范！");
            }

            var issupplier = _Context.SubCateGory.Where(x => x.Id == supplierId).SingleOrDefault();
            if (issupplier == null)
            {
                return DataStatus.DataError(1111, "请检查必填字段是否都填写规范！");
            }

            if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(descript) || costPrice < 0)
            {
                return DataStatus.DataError(1111, "请检查必填字段是否都填写规范！");
            }

            var newProduct = new ProductInfo
            {
                ParentId = parentId,
                SubId = subId,
                Name = name,
                BrandId = brandId,
                SupplierId = supplierId,
                Descript = descript,
                CostPrice = costPrice,
                Unit = unit,
                IsRecommend = isRecommend,
                IsNew = isNew
            };

            _ProductInfoRepository.Insert(newProduct);
            return DataStatus.DataSuccess(1000, newProduct, "添加商品信息成功！");
        }
```